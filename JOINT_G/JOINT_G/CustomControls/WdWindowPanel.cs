using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using newfast;
using JOINT_G.Helpers;

namespace JOINT_G.CustomControls
{
    /// <summary>
    /// 描画WINの親画面。COMに渡すためのWindowIDを保持する。
    /// </summary>
    public class WdWindowPanel : HwndHost
    {
        int hostHeight, hostWidth;
        public IDrawManager Manager
        {
            get => (IDrawManager)GetValue(ManegerProperty);
            set => SetValue(ManegerProperty, value);
        }

        static readonly DependencyProperty ManegerProperty
            = DependencyProperty.Register(
                nameof(Manager),
                typeof(IDrawManager),
                typeof(WdWindowPanel),
                new PropertyMetadata(null, null)
        );

        static readonly DependencyProperty PathProperty
            = DependencyProperty.Register(
                nameof(Path),
                typeof(string),
                typeof(WdWindowPanel),
                new PropertyMetadata(null, null)
        );
        public string Path
        {
            get => (string)GetValue(PathProperty);
            set => SetValue(PathProperty, value);
        }

        /// <summary>
        /// COMに渡すためのWindowのID
        /// </summary>
        public int WinID
        {
            get => _winid;
            private set => _winid = value;
        }
        private int _winid = 0;

        public WdWindowPanel()
        {
            hostHeight = 0;
            hostWidth = 0;
        }
        public WdWindowPanel(double height, double width)
        {
            hostHeight = (int)height;
            hostWidth = (int)width;

        }
        protected override HandleRef BuildWindowCore(HandleRef hwndParent)
        {
            Manager = new DrawManager();
            var rt = Manager.WdCreateWindow(hwndParent.Handle, hostWidth, hostHeight, newfast.EV_MODE.EVENT);
            WinID = Manager.WdWindowGetID(rt);
            return new HandleRef(this, rt);

        }
        protected override void OnInitialized(EventArgs e)
        {
            try
            {
                Window win = Window.GetWindow(this);

                win.Closed += Win_Closed;
                base.OnInitialized(e);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void Win_Closed(object? sender, EventArgs e)
        {
            try
            {
                //Window win = Window.GetWindow(this);
                //win.Closed -= Win_Closed;
                Dispose();
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        protected override void DestroyWindowCore(HandleRef hwnd)
        {
            try
            {

                Manager.WdDestroyWindow(hwnd.Handle);
                this.WinID = 0;
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
    }
}

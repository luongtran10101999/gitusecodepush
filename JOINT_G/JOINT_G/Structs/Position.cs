namespace JOINT_G.Structs
{
    /// <summary>
    /// ポジションの構造体。
    /// </summary>
    public struct Position
    {
        public Position(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public int Row { get; }

        public int Column { get; }
    }
}

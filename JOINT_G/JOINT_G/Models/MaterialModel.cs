using System.ComponentModel;

namespace JOINT_G.Models
{
    /// <summary>
    /// ήf[^fB
    /// </summary>
    public class MaterialModel
    {
        [Description("No")]
        public int Id { get; set; }

        [Description("JOINT}[N")]
        public string JointMark { get; set; }

        [Description("έv}[N")]
        public string DesignMark { get; set; }

        [Description("ΐζͺ")]
        public string BeamDivisionString { get; set; }
        public int BeamDivision => BeamDivisionString == "εΐ" ? 0 : BeamDivisionString == "¬ΐ" ? 1 : BeamDivisionString == "εΐΠΏ" ? 2 : BeamDivisionString == "¬ΐΠΏ" ? 3 : -1;

        [Description("bL")]
        public string Plating { get; set; }

        [Description("‘g’")]
        public string SideUseString { get; set; }
        public int SideUse => SideUseString == "YES" ? 1 : SideUseString == "NO" ? 0 : SideUseString == "c‘€p" ? 2 : -1;

        public int? FlangeType { get; set; }

        [Description("FLG")]
        public string Flange { get; set; }

        public int? WebType { get; set; }

        [Description("WEB")]
        public string Web { get; set; }

        [Description("ή")]
        public string CentralMember { get; set; }

        [Description("[ή")]
        public string EndMember { get; set; }

        [Description("KiF")]
        public string FlangeStandard1 { get; set; }

        [Description("KiW")]
        public string WebStandard1 { get; set; }

        [Description("KiF")]
        public string FlangeStandard2 { get; set; }

        [Description("KiW")]
        public string WebStandard2 { get; set; }

        [Description("νΉϋ")]
        public string MatchMethod { get; set; }

        [Description("n`(ΐ)")]
        public string BeamHaunch { get; set; }

        [Description("n`(pθ)")]
        public string JointHaunch { get; set; }
    }
}

using FCWPF.FCCommon.FCExtentions.PropertyGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace JOINT_G.Models.Interfaces
{
    public interface IDetailScreenViewModel
    {
        public bool ValidateProperty(string propName);
        public int PropNumber { get; }
        public string Title { get; }
        public PropertyGridModelBase PropertyGridModel { get; }
        public string[] MemberInfors { get; }
        public Dictionary<string, string> FormatStrings { get; }
        public List<ErrorDetails> Errors { get; }
        public void UpdateDrawWin();
        public void SelectBolt();
        public void ClearSelectBolt();
    }
}

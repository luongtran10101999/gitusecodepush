using JOINT_G.Structs;

using System.Collections.Generic;

namespace JOINT_G.Models.Interfaces
{
    public interface IPropertyGridModel
    {
        Dictionary<string, Position> CategoryPositions { get; protected set; }

        /// <summary>
        /// 最大列の行数
        /// </summary>
        public int MaxColumnTotalRows { get; protected set; }

        /// <summary>
        /// 最大列のすべてのカテゴリ ブロックの高さ
        /// </summary>
        public double MaxColumnCategoriesHeight { get; protected set; }
    }
}

using System.Collections.Generic;

namespace JOINT_G.Models
{
    /// <summary>
    /// メイン画面のデータモデル
    /// </summary>
    internal class MainScreenModel
    {
        public int JointMarkIndex { get; set; }

        public string JointMark { get; set; }

        public int? FlangeJointType { get; set; }

        public int? WebJointType { get; set; }

        public int? FlangeSP { get; set; }

        public int? BeamDevision { get; set; }

        public int? MemberInputMethod { get; set; }

        public int? Haunch { get; set; }

        public int? GPType { get; set; }

        public int? HasPlating { get; set; }

        public int? UseSide { get; set; }

        public List<FittingModel> FittingModels { get; set; }

        public List<MaterialModel> MaterialModels { get; set; }

        public List<ElementModel> ElementModels { get; set; }
    }
}

using C1.WPF.PropertyGrid;
using FCWPF.FCCommon.FCExtentions.PropertyGrid.Editor;
using JOINT_G.Structs;

namespace JOINT_G.Models
{
    /// <summary>
    /// pθΜΪΧf[^fB
    /// </summary>
    public class DetailsModelFSP : PropertyGridModelBase
    {
        private const string _group1 = " ";
        private const string _group2 = "  ";
        public DetailsModelFSP()
        {
            CategoryPositions = new()
            {
                { _group1, new Position(0, 0) },
                { _group2, new Position(0, 1) }
            };
        }
        [Display(GroupName = _group1, Name = "Categorysb`ΦA"), Editor(typeof(FCLabelEditor))]
        public string Category1 { get; set; }

        [Display(GroupName = _group1, Name = "    zρ^Cv"), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop1 { get; set; }

        [Display(GroupName = _group1, Name = "    {ga  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop2 { get; set; }

        [Display(GroupName = _group1, Name = "    Ea   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop3 { get; set; }

        [Display(GroupName = _group1, Name = "    Ki  "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop4 { get; set; }

        [Display(GroupName = _group1, Name = "    Π€{g{ "), Editor(typeof(FCTextBoxEditor))]
        public string Prop5 { get; set; }

        [Display(GroupName = _group1, Name = "    Q[WP  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop6 { get; set; }

        [Display(GroupName = _group1, Name = "    Q[W2  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop7 { get; set; }

        [Display(GroupName = _group1, Name = "    Q[W3  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop8 { get; set; }

        [Display(GroupName = _group1, Name = "    Q[W4  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop9 { get; set; }

        [Display(GroupName = _group1, Name = "    [ «(ΰ)  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop10 { get; set; }

        [Display(GroupName = _group1, Name = "    [ «(O)  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop11 { get; set; }

        [Display(GroupName = _group1, Name = "    ρsb`  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop12 { get; set; }

        [Display(GroupName = _group1, Name = "    NA  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop13 { get; set; }

        [Display(GroupName = _group2, Name = "CategoryXvCX"), Editor(typeof(FCLabelEditor))]
        public string Category2 { get; set; }

        [Display(GroupName = _group2, Name = "    OroΒϊ  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop14 { get; set; }

        [Display(GroupName = _group2, Name = "        Π  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop15 { get; set; }

        [Display(GroupName = _group2, Name = "        ·³  "), Editor(typeof(FCLabelEditor))]
        public string Prop16 { get; set; }

        [Display(GroupName = _group2, Name = "        Ki  "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop17 { get; set; }

        [Display(GroupName = _group2, Name = "    ΰroΒϊ  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop18 { get; set; }

        [Display(GroupName = _group2, Name = "         Π  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop19 { get; set; }

        [Display(GroupName = _group2, Name = "         ·³  "), Editor(typeof(FCLabelEditor))]
        public string Prop20 { get; set; }

        [Display(GroupName = _group2, Name = "         Ki  "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop21 { get; set; }
    }
}

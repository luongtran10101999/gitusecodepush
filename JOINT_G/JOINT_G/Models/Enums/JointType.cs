namespace JOINT_G.Models.Enums
{
    public enum JointType : int
    {
        Flange = 0,

        Web = 1
    }
}

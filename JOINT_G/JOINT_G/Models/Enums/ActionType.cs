using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JOINT_G.Models.Enums
{
    public enum ActionType : int
    {
        //[ 0.����n�� 1.�r�o�i���j 2.�r�o�i��j ]

        [Description("0.Copy")]
        Copy = 0,

        [Description("1.Edit")]
        Edit = 1,

        [Description("2.Update")]
        Update = 2,

        [Description("3.Change")]
        Change = 3,

        [Description("4.Material")]
        Material = 4,
    }
}

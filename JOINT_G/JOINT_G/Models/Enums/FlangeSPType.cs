using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JOINT_G.Models.Enums
{
    public enum FlangeSPType : short
    {
        //[ 0.����n�� 1.�r�o�i���j 2.�r�o�i��j ]

        [Description("0.����n��")]
        GY = 0,

        [Description("1.�r�o�i���j")]
        SPS = 1,

        [Description("2.�r�o�i��j")]
        SPY = 2,
    }
}

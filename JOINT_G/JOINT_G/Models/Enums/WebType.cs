using System.ComponentModel;

namespace JOINT_G.Models.Enums
{
    public enum WebType
    {
        [Description("0.なし")]
        None = 0,

        [Description("1.スプライス(SP)")]
        SP = 1,

        [Description("2.ガセット(GP)")]
        GP = 2,

        [Description("3.ガセット＋スプライス(GSP)")]
        GSP = 3,

        [Description("4.T型ガセット(TGP)")]
        TGP = 4
    }
}

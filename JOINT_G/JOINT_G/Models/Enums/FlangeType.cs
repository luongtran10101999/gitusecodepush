using System.ComponentModel;

namespace JOINT_G.Models.Enums
{
    public enum FlangeType : short
    {
        [Description("0.無し")]
        None = 0,

        [Description("1.スプライス(SP)")]
        SP = 1,

        [Description("4.T型ガセット(TGP)")]
        TGP = 4,

        [Description("7.現場溶接(GY)")]
        GY = 7,

        [Description("8.PL溶接(PL)")]
        PL = 8
    }
}

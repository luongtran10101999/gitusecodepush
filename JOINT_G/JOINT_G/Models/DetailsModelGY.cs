using C1.WPF.PropertyGrid;
using FCWPF.FCCommon.FCExtentions.PropertyGrid.Editor;
using JOINT_G.Structs;
using EditorAttribute = C1.WPF.PropertyGrid.EditorAttribute;

namespace JOINT_G.Models
{
    /// <summary>
    /// 継手の詳細データモデル。
    /// </summary>
    public class DetailsModelGY : PropertyGridModelBase
    {
        private const string _group1 = " ";
        private const string _group2 = "  ";
        public DetailsModelGY()
        {
            CategoryPositions = new()
            {
                { _group1, new Position(0, 0) },
                { _group2, new Position(0, 1) },
            };
        }
        [Display(GroupName = _group1, Name = "       スカラップタイプ   "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop1 { get; set; }
        [Display(GroupName = _group1, Name = "       開先角度(上)  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop2 { get; set; }

        [Display(GroupName = _group1, Name = "       開先角度(下)   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop3 { get; set; }

        [Display(GroupName = _group1, Name = "       スカラップ半径(上)   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop4 { get; set; }

        [Display(GroupName = _group1, Name = "       スカラップ半径(下)   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop5 { get; set; }

        [Display(GroupName = _group1, Name = "       ルートギャップ   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop6 { get; set; }

        [Display(GroupName = _group1, Name = "   WEB=SP時、端部側スカラップ 上FLG側  "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop7 { get; set; }

        [Display(GroupName = _group1, Name = "                            下FLG側   "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop8 { get; set; }

        [Display(GroupName = _group1, Name = "角型スカラップ・深さ(上)   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop9 { get; set; }

        [Display(GroupName = _group1, Name = "              巾(上)   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop10 { get; set; }

        [Display(GroupName = _group1, Name = "              角部のR(上)   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop11 { get; set; }

        [Display(GroupName = _group1, Name = "角型スカラップ・深さ(下)  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop12 { get; set; }

        [Display(GroupName = _group1, Name = "             巾(下)（開先から）   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop13 { get; set; }

        [Display(GroupName = _group1, Name = "             角部のR(下)   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop14 { get; set; }

        [Display(GroupName = _group1, Name = "       適応板厚・下限   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop15 { get; set; }

        [Display(GroupName = _group1, Name = "                上限   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop16 { get; set; }

        [Display(GroupName = _group2, Name = "    梁付き段差部の水平リブ有無  "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop17 { get; set; }

        [Display(GroupName = _group2, Name = @"     ただし、段差寸法 ＜ ... の時は無し   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop18 { get; set; }

        [Display(GroupName = _group2, Name = @"         板厚基準   "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop19 { get; set; }

        [Display(GroupName = _group2, Name = @"               SizeUp  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop20 { get; set; }

        [Display(GroupName = _group2, Name = @"               板厚入力 "), Editor(typeof(FCTextBoxEditor))]
        public string Prop21 { get; set; }

        [Display(GroupName = _group2, Name = @"         規格基準  "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop22 { get; set; }

        [Display(GroupName = _group2, Name = @"                規格入力 "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop23 { get; set; }

        [Display(GroupName = _group2, Name = @"         巾基準 "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop24 { get; set; }

        [Display(GroupName = _group2, Name = @"                フカシ寸法 "), Editor(typeof(FCTextBoxEditor))]
        public string Prop25 { get; set; }

        [Display(GroupName = _group2, Name = @"                巾寸法  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop26 { get; set; }

        [Display(GroupName = _group2, Name = @"         めちがい防止　上FLG側  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop27 { get; set; }

        [Display(GroupName = _group2, Name = @"                下FLG側   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop28 { get; set; }

    }
}


using System.ComponentModel;

namespace JOINT_G.Models
{
    /// <summary>
    /// 継手マスタのデータモデル。
    /// </summary>
    public class JointMasterModel
    {
        public int Id { get; set; }

        public int No => Id + 1;

        [Description("名称No")]
        public string Name { get; set; }

        [Description("設計事務所名")]
        public string DesignOfficeName { get; set; }

        [Description("件数")]
        public int Count { get; set; }
    }
}

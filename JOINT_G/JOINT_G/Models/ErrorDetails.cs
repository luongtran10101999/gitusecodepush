using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JOINT_G.Models
{
    /// <summary>
    /// エラー詳細のデータモデル。
    /// </summary>
    public class ErrorDetails
    {
        public string ErrorMessage { get; set; }
        public int ErrorStatus { get; set; }
        public string PropName { get; set; }
    }
}

namespace JOINT_G.Models
{
    /// <summary>
    /// 継手データモデル。
    /// </summary>
    public class FittingModel
    {
        public int Index { get; set; }

        public int Number => Index;

        public string Mark { get; set; }

        public string OfficialMark { get; set; }

        public string OfficialMarkDisplay => OfficialMark ?? Mark;

        public int? FlangeType { get; set; }

        public string Flange { get; set; }

        public int? WebType { get; set; }

        public string Web { get; set; }

        public string ElementName { get; set; }

        public string FlangeStandard { get; set; }

        public string WebStandard { get; set; }
    }
}

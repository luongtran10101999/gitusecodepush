using FCWPF.FCCommon.FCExtentions.PropertyGrid;
using JOINT_G.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using C1.WPF.Core;
using JOINT_G.Models.Interfaces;
using C1.WPF.PropertyGrid;
using BrowsableAttribute = FCWPF.FCCommon.FCExtentions.PropertyGrid.BrowsableAttribute;
using System.Windows.Media;
using FCWPF.FCCommon.FCControls.Text;
using FCWPF.FCCommon.FCControls.Select;
using C1.WPF.Accordion;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using FCWPF.FCCommon.FCControls.Combination;

namespace JOINT_G.Models
{
    /// <summary>
    /// プロパティグリッドのベースモデル。
    /// </summary>
    public abstract class PropertyGridModelBase : IPropertyGridModel, INotifyPropertyChanged
    {
        private const int WrapperOffset = 3;

        public event PropertyChangedEventHandler PropertyChanged;

        [Browsable(false)]
        public Dictionary<string, Position> CategoryPositions { get; set; }

        [Browsable(false)]
        public int MaxColumnTotalRows { get; set; }

        [Browsable(false)]
        public double MaxColumnCategoriesHeight { get; set; }

        public void Init(FCPropertyGrid propertyGrid)
        {
            //FCPropertyGrid.UnifyLabelWidth(propertyGrid);
            //FCPropertyGrid.RemoveVerticalMargin(propertyGrid);
            propertyGrid.LabelWidth = propertyGrid.PropertyBoxes.Max((PropertyBox x) => x.Label.ActualWidth);
            foreach (PropertyBox propertyBox in propertyGrid.PropertyBoxes)
            {
                propertyBox.Margin = new Thickness(propertyBox.Margin.Left, 0.0, propertyBox.Margin.Right, 0.0);
            }
            GenerateGrid(propertyGrid);
            CalcConstants(propertyGrid);
        }

        public void GenerateGrid(FCPropertyGrid propertyGrid)
        {

            foreach (var catPos in CategoryPositions)
            {
                if (!propertyGrid.CategoryContainers.ContainsKey(catPos.Key))
                {
                    continue;
                }

                // Sequence: 0, 2, 4... 
                var column = catPos.Value.Column == 0 ? catPos.Value.Column : (catPos.Value.Column + 2 - (catPos.Value.Column % 2)) - 1;

                propertyGrid.CategoryContainers[catPos.Key].SetValue(Grid.RowProperty, catPos.Value.Row);
                propertyGrid.CategoryContainers[catPos.Key].SetValue(Grid.ColumnProperty, column);
                propertyGrid.CategoryContainers[catPos.Key].Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));

                propertyGrid.CategoryContainers[catPos.Key].BorderBrush = new SolidColorBrush(Color.FromRgb(196, 196, 196));
                propertyGrid.CategoryContainers[catPos.Key].BorderThickness = new Thickness(1);
                propertyGrid.CategoryContainers[catPos.Key].Margin = new Thickness(3);

                propertyGrid.CategoryContainers[catPos.Key].IsTabStop = false;
                propertyGrid.CategoryContainers[catPos.Key].Focusable = false;

                propertyGrid.CategoryContainers[catPos.Key].ExpandIconTemplate = null;
                propertyGrid.CategoryContainers[catPos.Key].Margin = new Thickness(0, 10, 0, 0);
                propertyGrid.CategoryContainers[catPos.Key].HeaderStyle = null;
                propertyGrid.CategoryContainers[catPos.Key].HeaderTemplate = null;
                (propertyGrid.CategoryContainers[catPos.Key].Content as ItemsControl).Focusable = false;
                propertyGrid.CategoryContainers[catPos.Key].HorizontalAlignment = HorizontalAlignment.Left;
            }
            var rowsCount = CategoryPositions.Values.Max(x => x.Row) + 1;
            var columnsCount = CategoryPositions.Values.Max(x => x.Column) + 1;

            var grid = new FrameworkElementFactory(typeof(Grid));


            for (int c = 0; c < columnsCount; c++)
            {
                var col = new FrameworkElementFactory(typeof(ColumnDefinition));
                col.SetValue(ColumnDefinition.WidthProperty, new GridLength(1, GridUnitType.Star));
                grid.AppendChild(col);
            }

            for (int r = 0; r < rowsCount; r++)
            {
                var row = new FrameworkElementFactory(typeof(RowDefinition));
                row.SetValue(RowDefinition.HeightProperty, GridLength.Auto);
                grid.AppendChild(row);
            }

            propertyGrid.PropertiesPanel = new ItemsPanelTemplate { VisualTree = grid };
        }

        public void CalcConstants(FCPropertyGrid propertyGrid)
        {
            var colGroup = CategoryPositions.GroupBy(x => x.Value.Column).ToDictionary(x => x.Key, x => x.Select(y => y.Key).ToArray());

            var constraints = colGroup.Select(x =>
            {
                var categories = propertyGrid.CategoryContainers.Where(z => x.Value.Contains(z.Key)).ToArray();

                return new
                {
                    CategoriesColumnHeight = categories.Sum(x => x.Value.ActualHeight),
                    TotalRows = categories.Sum(x => ((ItemsControl)x.Value.Content).Items.Count)
                };
            })
            .OrderByDescending(x => x.TotalRows)
            .First();

            MaxColumnCategoriesHeight = constraints.CategoriesColumnHeight;
            MaxColumnTotalRows = constraints.TotalRows;
        }

        public void ResizePropGrid(double wrapperHeight, FCPropertyGrid propertyGrid)
        {
            propertyGrid.BorderBrush = new SolidColorBrush(Color.FromRgb(255, 255, 255));
            var height = (wrapperHeight - MaxColumnCategoriesHeight - WrapperOffset) / MaxColumnTotalRows;
            foreach (PropertyBox propertyBox in propertyGrid.PropertyBoxes)
            {
                propertyBox.Height = 30;
                propertyBox.LabelWidth = 200;
                propertyBox.EditorWidth = 170;
                propertyBox.Width = 370;

                propertyBox.MinEditorWidth = 120;
                propertyBox.MinLabelWidth = 140;
                propertyBox.MinWidth = 260;


                propertyGrid.BorderThickness = new Thickness(2);

                if (propertyGrid.GetEditor(propertyBox.Name) is FCTextBox tb)
                {
                    tb.BorderThickness = new Thickness(1);
                    tb.Margin = new Thickness(0, 0, 5, 0);
                }
                else if (propertyGrid.GetEditor(propertyBox.Name) is FCComboBoxExpansion cb)
                {
                    cb.BorderThickness = new Thickness(1);
                    cb.Margin = new Thickness(0, 0, 5, 0);
                }
                else if (propertyGrid.GetEditor(propertyBox.Name) is FCLabel lb)
                {
                    lb.BorderThickness = new Thickness(1);
                    lb.Margin = new Thickness(0, 0, 5, 0);
                }
                else if (propertyBox.CurrentEditor is FCInputDimension id)
                {
                    propertyBox.Height = 30;
                    propertyBox.LabelWidth = 1;
                    propertyBox.EditorWidth = 300;
                    propertyBox.Width = 370;

                    propertyBox.MinEditorWidth = 1;
                    propertyBox.MinLabelWidth = 1;
                    propertyBox.MinWidth = 240;

                    propertyBox.Panel.HorizontalAlignment = HorizontalAlignment.Right;
                }


            }
            if (this.GetType() == typeof(DetailsModelTGP))
            {
                var editor1 = propertyGrid.GetEditor(nameof(DetailsModelTGP.CTValue1)) as FCComboBoxExpansion;

                editor1.BorderThickness = new Thickness(1);
                editor1.Margin = new Thickness(0, 0, 5, 0);
                editor1.Name = "CTValue1";

                var editor2 = propertyGrid.GetEditor(nameof(DetailsModelTGP.CTValue2)) as FCInputDimension;
                editor2.Name = "CTValue2";

                var editor3 = propertyGrid.GetEditor(nameof(DetailsModelTGP.CTValue3)) as FCComboBoxExpansion;
                editor3.Name = "CTValue3";
                editor3.BorderThickness = new Thickness(1);
                editor3.Margin = new Thickness(0, 0, 5, 0);

                var editor4 = propertyGrid.GetEditor(nameof(DetailsModelTGP.CTValue4)) as FCTextBox;
                editor4.Name = "CTValue4";
                editor4.BorderThickness = new Thickness(1);
                editor4.Margin = new Thickness(0, 0, 5, 0);
            }

        }

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void UpdateProperty([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

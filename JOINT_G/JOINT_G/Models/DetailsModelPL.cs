using C1.WPF.PropertyGrid;
using FCWPF.FCCommon.FCExtentions.PropertyGrid.Editor;
using JOINT_G.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JOINT_G.Models
{
    /// <summary>
    /// �p��̏ڍ׃f�[�^���f���B
    /// </summary>
    public class DetailsModelPL : PropertyGridModelBase
    {
        private const string _group1 = "     ";
        public DetailsModelPL()
        {
            CategoryPositions = new()
            {
                { _group1, new Position(0, 0) },
            };
        }

        [Display(GroupName = _group1, Name = "   �c�e�~�~���@(��)"), Editor(typeof(FCTextBoxEditor))]
        public string Prop1 { get; set; }

        [Display(GroupName = _group1, Name = "                (��)  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop2 { get; set; }

        [Display(GroupName = _group1, Name = "                (��)   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop3 { get; set; }

        [Display(GroupName = _group1, Name = "                (�E)   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop4 { get; set; }

        [Display(GroupName = _group1, Name = "                ��   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop5 { get; set; }

        [Display(GroupName = _group1, Name = "                �K�i  "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop6 { get; set; }

    }
}

using FCWPF.FCCommon.FCExtentions.PropertyGrid;
using FCWPF.FCCommon.FCExtentions.PropertyGrid.Editor;
using JOINT_G.Structs;

namespace JOINT_G.Models
{
    /// <summary>
    /// 継手の詳細データモデル。
    /// </summary>
    public class DetailsModelWSP : PropertyGridModelBase
    {
        private const string _group1 = " ";
        private const string _group2 = "  ";
        public DetailsModelWSP()
        {
            CategoryPositions = new()
            {
                { _group1, new Position(0, 0) },
                { _group2, new Position(0, 1) },
            };
        }


        [Display(GroupName = _group1, Name = "Category●ピッチ関連"), Editor(typeof(FCLabelEditor))]
        public string Category1 { get; set; }


        [Display(GroupName = _group1, Name = "　　　　配列タイプ   "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop1 { get; set; }

        [Display(GroupName = _group1, Name = "　　　　ボルト径  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop2 { get; set; }

        [Display(GroupName = _group1, Name = "　　　　孔径   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop3 { get; set; }

        [Display(GroupName = _group1, Name = "　　　　規格   "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop4 { get; set; }

        [Display(GroupName = _group1, Name = "　　　　列数  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop5 { get; set; }

        [Display(GroupName = _group1, Name = "　　　　本数(一列目)  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop6 { get; set; }

        [Display(GroupName = _group1, Name = "　　　　本数(二列目) "), Editor(typeof(FCTextBoxEditor))]
        public string Prop7 { get; set; }

        [Display(GroupName = _group1, Name = "　　　　端あき(内)   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop8 { get; set; }

        [Display(GroupName = _group1, Name = "　　　　端あき(外)   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop9 { get; set; }

        [Display(GroupName = _group1, Name = "　　　　ピッチ  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop10 { get; set; }

        [Display(GroupName = _group1, Name = "　　　　列ピッチ   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop11 { get; set; }

        [Display(GroupName = _group1, Name = "　　　　ヘリあき基準   "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop12 { get; set; }

        [Display(GroupName = _group1, Name = "  "), Editor(typeof(FCLabelEditor))]
        public string Prop13 { get; set; }

        [Display(GroupName = _group1, Name = "　　　　へりあき入力   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop14 { get; set; }

        [Display(GroupName = _group1, Name = "　　　　クリア   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop15 { get; set; }


        [Display(GroupName = _group2, Name = "Category●スプライス"), Editor(typeof(FCLabelEditor))]
        public string Category2 { get; set; }


        [Display(GroupName = _group2, Name = "　　ＳＰ板厚   "), Editor(typeof(FCTextBoxEditor))]
        public string Prop16 { get; set; }

        [Display(GroupName = _group2, Name = "　　　　規格  "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop17 { get; set; }

        [Display(GroupName = _group2, Name = "　　　　縁あき "), Editor(typeof(FCTextBoxEditor))]
        public string Prop18 { get; set; }

        [Display(GroupName = _group2, Name = "　　　　枚数  "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop19 { get; set; }

        [Display(GroupName = _group2, Name = "  "), Editor(typeof(FCLabelEditor))]
        public string Prop20 { get; set; }

        [Display(GroupName = _group2, Name = "　　　　蝶番取付有無 "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop21 { get; set; }

        [Display(GroupName = _group2, Name = "　　　　タイプ   "), Editor(typeof(FCComboBoxExpansionEditor))]
        public string Prop22 { get; set; }

        [Display(GroupName = _group2, Name = "　　　　取付寸法  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop23 { get; set; }

        [Display(GroupName = _group2, Name = "Category●ウェブ現溶 "), Editor(typeof(FCLabelEditor))]
        public string Category3 { get; set; }


        [Display(GroupName = _group2, Name = "　　ウェブ開先角度 ( =0は現溶無し )  "), Editor(typeof(FCTextBoxEditor))]
        public string Prop24 { get; set; }
    }
}

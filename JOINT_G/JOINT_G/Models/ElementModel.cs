using System.ComponentModel;

namespace JOINT_G.Models
{
    /// <summary>
    /// ÞÌf[^fB
    /// </summary>
    public class ElementModel
    {
        public int Id { get; set; }

        [Description("ÞíR[h")]
        public int? FlangeCodeId { get; set; }

        [Description("Þ")]
        public string ElementName { get; set; }

        [Description("¡@P")]
        public double? Dimension1 { get; set; }

        [Description("¡@Q")]
        public double? Dimension2 { get; set; }

        [Description("¡@R")]
        public double? Dimension3 { get; set; }

        [Description("¡@S")]
        public double? Dimension4 { get; set; }

        [Description("KiF")]
        public string FlangeStandard { get; set; }

        [Description("KiW")]
        public string WebStandard { get; set; }

        [Description("¹û")]
        public int? MatchType { get; set; }

        [Description("[fvX")]
        public float? EdgeDepth { get; set; }
    }
}

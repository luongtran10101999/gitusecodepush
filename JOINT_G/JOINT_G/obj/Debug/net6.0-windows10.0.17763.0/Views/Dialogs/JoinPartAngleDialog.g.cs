#pragma checksum "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "886A93BAB1A2608F3255AF4E4B5561A4D6EEF6D9"
//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはツールによって生成されました。
//     ランタイム バージョン:4.0.30319.42000
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。
// </auto-generated>
//------------------------------------------------------------------------------

using FCWPF.FCCommon.FCControls.Combination;
using FCWPF.FCCommon.FCControls.Controls;
using FCWPF.FCCommon.FCControls.Select;
using FCWPF.FCCommon.FCControls.Text;
using FCWPF.FCCommon.FCExtentions.Help;
using JOINT_G.Helpers;
using JOINT_G.ViewModels.Dialogs;
using JOINT_G.Views.Dialogs;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace JOINT_G.Views.Dialogs {
    
    
    /// <summary>
    /// JoinPartAngleDialog
    /// </summary>
    public partial class JoinPartAngleDialog : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 69 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal FCWPF.FCCommon.FCControls.Select.FCComboBox shapeCombobox;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal FCWPF.FCCommon.FCControls.Select.FCComboBox gradientCombobox;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal FCWPF.FCCommon.FCControls.Text.FCTextBox frequency;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal FCWPF.FCCommon.FCControls.Text.FCTextBox tan1;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal FCWPF.FCCommon.FCControls.Text.FCTextBox tan2;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal FCWPF.FCCommon.FCControls.Combination.FCCheckTextBox checkBlock;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal FCWPF.FCCommon.FCControls.Controls.FCButton buttonOk;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal FCWPF.FCCommon.FCControls.Controls.FCButton buttonCancel;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal FCWPF.FCCommon.FCExtentions.Help.FCHelpImageExtend svgImg;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.9.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/JOINT_G;component/views/dialogs/joinpartangledialog.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.9.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 20 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
            ((JOINT_G.Views.Dialogs.JoinPartAngleDialog)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            
            #line 25 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
            ((JOINT_G.Views.Dialogs.JoinPartAngleDialog)(target)).KeyDown += new System.Windows.Input.KeyEventHandler(this.OnKeyPressed);
            
            #line default
            #line hidden
            return;
            case 2:
            this.shapeCombobox = ((FCWPF.FCCommon.FCControls.Select.FCComboBox)(target));
            
            #line 69 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
            this.shapeCombobox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ShapeValue_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.gradientCombobox = ((FCWPF.FCCommon.FCControls.Select.FCComboBox)(target));
            
            #line 72 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
            this.gradientCombobox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.Gradient_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.frequency = ((FCWPF.FCCommon.FCControls.Text.FCTextBox)(target));
            
            #line 75 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
            this.frequency.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.CheckInput);
            
            #line default
            #line hidden
            return;
            case 5:
            this.tan1 = ((FCWPF.FCCommon.FCControls.Text.FCTextBox)(target));
            
            #line 78 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
            this.tan1.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.CheckInput);
            
            #line default
            #line hidden
            return;
            case 6:
            this.tan2 = ((FCWPF.FCCommon.FCControls.Text.FCTextBox)(target));
            
            #line 80 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
            this.tan2.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.CheckInput);
            
            #line default
            #line hidden
            return;
            case 7:
            this.checkBlock = ((FCWPF.FCCommon.FCControls.Combination.FCCheckTextBox)(target));
            return;
            case 8:
            this.buttonOk = ((FCWPF.FCCommon.FCControls.Controls.FCButton)(target));
            
            #line 85 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
            this.buttonOk.Click += new System.Windows.RoutedEventHandler(this.OkButtonClick);
            
            #line default
            #line hidden
            return;
            case 9:
            this.buttonCancel = ((FCWPF.FCCommon.FCControls.Controls.FCButton)(target));
            
            #line 86 "..\..\..\..\..\Views\Dialogs\JoinPartAngleDialog.xaml"
            this.buttonCancel.Click += new System.Windows.RoutedEventHandler(this.CancelButtonClick);
            
            #line default
            #line hidden
            return;
            case 10:
            this.svgImg = ((FCWPF.FCCommon.FCExtentions.Help.FCHelpImageExtend)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}


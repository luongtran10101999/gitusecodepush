using JOINT_G.Models.Enums;
using System;
using System.Collections.Generic;

namespace JOINT_G.Managers.Mappers
{
    /// <summary>
    /// WEBタイプに関するヘルパー。
    /// </summary>
    public class WebTypeMapper
    {
        private static readonly Dictionary<int, string[]> WebJointTypesMapping = new()
        {
            {-1, new[] { "0.無し", "1.スプライス(SP)", "2.ガセット(GP)", "3.ガセット＋スプライス(GSP)" } },
            {(int)FlangeType.None, new [] { "1.スプライス(SP)", "2.ガセット(GP)", "3.ガセット＋スプライス(GSP)" } },
            {(int)FlangeType.SP, new [] { "0.無し", "1.スプライス(SP)" } },
            {(int)FlangeType.TGP, new[] { "0.無し" } },
            {(int)FlangeType.GY, new [] { "1.スプライス(SP)", "2.ガセット(GP)", "3.ガセット＋スプライス(GSP)" } },
            {(int)FlangeType.PL, new[] { "0.無し" } },
        };
        private static readonly Dictionary<int, string[]> HaunchsMapper = new()
        {
            {-1, new[] { "0.無し" } },
            { 0, new [] { "0.無し", "2.タイプ２" } },
            { 1, new[] { "0.無し" , "1.タイプ１",  "2.タイプ２"  } },
        };
        public Dictionary<int, string> GetWebJointTypes(int flangeJointType, bool isEnabled)
        {
            int flange = isEnabled ? flangeJointType : -1;
            var result = new Dictionary<int, string>();
            foreach (var webType in WebJointTypesMapping[flange])
            {
                result.Add(int.Parse(webType.Split(".")[0]), webType);
            }
            return result;
        }
        public Dictionary<int, string> GetHaunchs(int memberInputMethod, bool isEnabled)
        {
            int bzinp = isEnabled ? memberInputMethod : -1;
            var result = new Dictionary<int, string>();
            foreach (var haunch in HaunchsMapper[bzinp])
            {
                result.Add(int.Parse(haunch.Split(".")[0]), haunch);
            }
            return result;
        }
    }
}

using System.Collections.Generic;

namespace JOINT_G.Helpers
{
    /// <summary>
    /// Dictionaryのヘルパー。
    /// </summary>
    public static class DictionaryHelper
    {
        public static int IndexOf(this Dictionary<int, string> source ,  int key)
        {
            int index = 0;
            foreach (var item in source)
            {
                if(item.Key == key)  return index;
                index++;
            }
            return -1;
        }
    }
}

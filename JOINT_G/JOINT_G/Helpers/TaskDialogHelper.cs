using FastCrew.Application.UI;

using System.Windows.Forms;

namespace JOINT_G.Helpers
{
    /// <summary>
    /// TaskDialog を呼び出す共通の静的クラス
    /// </summary>
    internal static class TaskDialogHelper
    {
        private const string ErrorCaption = "エラー";

        private static readonly TaskDialogButtonCollection OkCollection = new() { new("OK") };
        private static readonly TaskDialogButtonCollection EnterConfirmCollection = new() { new("確認 Enter") };
        private static readonly TaskDialogButtonCollection OkCancelCollection = new() { new("強行"), new("キャンセル") };
        private static readonly TaskDialogButtonCollection YesNoCollection = new() { new("いいえ"), new("はい") };
        public static bool ShowWarningConfirmation(string text, string caption)
        {
            var pressedBtn = FcTaskDialog.Show(caption ?? string.Empty, text, TaskDialogIcon.Warning, OkCancelCollection);
            return pressedBtn == OkCancelCollection[0];
        }
        public static bool ShowWarningYesNoConfirmation(string text, string caption)
        {
            var pressedBtn = FcTaskDialog.Show(caption ?? string.Empty, text, TaskDialogIcon.Warning, YesNoCollection);
            return pressedBtn == YesNoCollection[1];
        }
        public static void ShowWarning(string text, string caption)
        {
            FcTaskDialog.Show(caption ?? string.Empty, text, TaskDialogIcon.Warning, OkCollection);
        }

        public static void ShowErrorConfirmation(string text, string caption = null)
        {
            var pressedBtn = FcTaskDialog.Show(caption ?? ErrorCaption, text, TaskDialogIcon.Error, EnterConfirmCollection);
            if (pressedBtn != EnterConfirmCollection[0])
            {
                ShowErrorConfirmation(text, caption);
            }

        }

        public static void ShowError(string text, string caption = null)
        {
            FcTaskDialog.Show(caption ?? ErrorCaption, text, TaskDialogIcon.Error, OkCollection);
        }

        public static void ShowInfo(string text, string caption = null)
        {
            FcTaskDialog.Show(caption ?? string.Empty, text, TaskDialogIcon.Information, OkCollection);
        }
    }
}

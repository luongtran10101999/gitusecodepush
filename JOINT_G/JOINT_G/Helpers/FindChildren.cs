using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace JOINT_G.Helpers
{
    /// <summary>
    /// コレクションの中の要素を検索するためのヘルパー。
    /// </summary>
    public static class FindChildren
    {
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject parent)
        where T : DependencyObject
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                var childType = child as T;
                if (childType != null)
                {
                    yield return (T)child;
                }

                foreach (var other in FindVisualChildren<T>(child))
                {
                    yield return other;
                }
            }
        }

        public static T GetFirstChildOfType<T>(DependencyObject dependencyObject) where T : DependencyObject
        {
            if (dependencyObject == null)
            {
                return null;
            }

            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(dependencyObject); i++)
            {
                var child = VisualTreeHelper.GetChild(dependencyObject, i);
                var result = (child as T) ?? GetFirstChildOfType<T>(child);

                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        public static T FindChildrenByName<T>(this DependencyObject dependencyObject, string childName) where T : DependencyObject
        {
            if (dependencyObject == null)
            {
                return null;
            }
            // success case
            if (dependencyObject is T && ((FrameworkElement)dependencyObject).Name == childName)
                return dependencyObject as T;

            var count = VisualTreeHelper.GetChildrenCount(dependencyObject);
            for (var i = 0; i < count; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(dependencyObject, i);
                // recursively drill down the tree
                T obj = FindChildrenByName<T>(child, childName);

                if (obj != null)
                    return obj;
            }
            return null;
        }

        public static T FindChildrenCommandParamater<T>(this DependencyObject dependencyObject, Key? key) where T : DependencyObject
        {
            if (dependencyObject == null)
            {
                return null;
            }
            // success case
            if (dependencyObject is T && (dependencyObject as Button).CommandParameter != null && (dependencyObject as Button).CommandParameter.Equals(key.Value))
                return dependencyObject as T;

            var count = VisualTreeHelper.GetChildrenCount(dependencyObject);
            for (var i = 0; i < count; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(dependencyObject, i);
                // recursively drill down the tree
                T obj = FindChildrenCommandParamater<T>(child, key);

                if (obj != null)
                    return obj;
            }
            return null;
        }
        public static T FindChildrenCommandParamater<T>(this DependencyObject dependencyObject, string commandParamater) where T : DependencyObject
        {
            if (dependencyObject == null)
            {
                return null;
            }
            // success case
            if (dependencyObject is T && (dependencyObject as Button).CommandParameter != null && (dependencyObject as Button).CommandParameter.Equals(commandParamater))
                return dependencyObject as T;

            var count = VisualTreeHelper.GetChildrenCount(dependencyObject);
            for (var i = 0; i < count; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(dependencyObject, i);
                // recursively drill down the tree
                T obj = FindChildrenCommandParamater<T>(child, commandParamater);

                if (obj != null)
                    return obj;
            }
            return null;
        }

        public static IEnumerable<T> FindVisualChildrens<T>(this DependencyObject parent) where T : DependencyObject
        {
            if (parent == null)
                throw new ArgumentNullException(nameof(parent));

            var queue = new Queue<DependencyObject>(new[] { parent });

            while (queue.Any())
            {
                var reference = queue.Dequeue();
                var count = VisualTreeHelper.GetChildrenCount(reference);

                for (var i = 0; i < count; i++)
                {
                    var child = VisualTreeHelper.GetChild(reference, i);
                    if (child is T children)
                        yield return children;

                    queue.Enqueue(child);
                }
            }
        }

        public static T FindVisualChildrenFirst<T>(DependencyObject parent)
        where T : DependencyObject
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                var childType = child as T;
                if (childType != null)
                {
                    return (T)child;
                }

                foreach (var other in FindVisualChildren<T>(child))
                {
                    return other;
                }
            }
            return null;
        }
        public static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            //get parent item
            if (child is null) return null;
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return FindParent<T>(parentObject);
        }
    }
}

using Serilog;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace JOINT_G.Helpers
{
    /// <summary>
    /// 例外出力のためのヘルパー。
    /// </summary>
    public static class ExceptionHelper
    {
        public static void HandleException(this Exception ex)
        {
            try
            {
                var st = new StackTrace(ex, true);
                var fileName = st.GetFrames()
                              .Select(frame => new
                              {
                                  FileName = frame.GetFileName(),
                              }).First();


                var frame = st.GetFrame(0);

                var line = frame.GetFileLineNumber();

                var methodName = ex.TargetSite != null ? ex.TargetSite.Name : "";
                var className = (ex.TargetSite != null && ex.TargetSite.DeclaringType != null) ? ex.TargetSite.DeclaringType.Name : "";

                Log.Error($"{Path.GetFileName(fileName.FileName)} {className}:{methodName}:{line} ; Message : {ex.Message} ; StackTrace : \n {ex.ToString()} \n");
            }
            catch (Exception ex2)
            {

            }
        }
    }
}

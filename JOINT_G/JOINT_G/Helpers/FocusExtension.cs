using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace JOINT_G.Helpers
{
    /// <summary>
    /// フォーカス制御のヘルパー。
    /// </summary>
    public static class FocusExtension
    {
        public static readonly DependencyProperty IsFocusedProperty =
            DependencyProperty.RegisterAttached("IsFocused", typeof(bool?), typeof(FocusExtension), new FrameworkPropertyMetadata(IsFocusedChanged) { BindsTwoWayByDefault = true });

        public static bool? GetIsFocused(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            return (bool?)element.GetValue(IsFocusedProperty);
        }

        public static void SetIsFocused(DependencyObject element, bool? value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            element.SetValue(IsFocusedProperty, value);
        }

        private static void IsFocusedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var fe = (FrameworkElement)d;

            if (e.OldValue == null)
            {
                fe.GotFocus += FrameworkElement_GotFocus;
                fe.LostFocus += FrameworkElement_LostFocus;
            }

            if (!fe.IsVisible)
            {
                fe.IsVisibleChanged += new DependencyPropertyChangedEventHandler(fe_IsVisibleChanged);
            }

            if (e.NewValue != null && (bool)e.NewValue)
            {
                fe.Focus();
            }
        }

        private static void fe_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var fe = (FrameworkElement)sender;
            if (fe.IsVisible && (bool)fe.GetValue(IsFocusedProperty))
            {
                fe.IsVisibleChanged -= fe_IsVisibleChanged;
                fe.Focus();
            }
        }

        private static void FrameworkElement_GotFocus(object sender, RoutedEventArgs e)
        {
            ((FrameworkElement)sender).SetValue(IsFocusedProperty, true);
            e.Handled = true;
            SetSelectedAll(sender);
        }

        private static void FrameworkElement_LostFocus(object sender, RoutedEventArgs e)
        {
            ((FrameworkElement)sender).SetValue(IsFocusedProperty, false);
        }

        private static void SetSelectedAll(object element)
        {
            if (element is TextBox)
            {
                TextBox control = (TextBox)element;
                control.SelectAll();
            }

            if (element is PasswordBox)
            {
                PasswordBox control = (PasswordBox)element;
                control.SelectAll();
            }
        }

        private static void SetSelection(PasswordBox passwordBox, int start, int length)
        {
            passwordBox.GetType().GetMethod("Select", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(passwordBox, new object[] { start, length });
        }

        public static void SetFocus<T>(string elementName, DependencyObject parent) where T : DependencyObject
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(new Action(delegate ()
            {
                var el = (FindChildren.FindChildrenByName<T>(parent, elementName) as FrameworkElement);
                if (el != null)
                {
                    el.Focus();
                }
            }));
        }

        public static void SelectAll<T>(string elementName, DependencyObject parent) where T : DependencyObject
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(new Action(delegate ()
            {
                var el = (FindChildren.FindChildrenByName<T>(parent, elementName) as FrameworkElement);
                if (el != null)
                {
                    el.Focus();
                }
            }));
        }
    }
}

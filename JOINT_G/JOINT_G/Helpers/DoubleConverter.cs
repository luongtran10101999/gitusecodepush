using FCWPF.FCCommon.FCControls.Text;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace JOINT_G.Helpers
{
    /// <summary>
    /// 数値変換のためのヘルパー。
    /// </summary>
    [ValueConversion(typeof(double), typeof(string))]
    public class DoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double doubleType = (double)value;

            return FCTextBoxCommon.Convert(doubleType.ToString(), parameter as String);

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = value as string;

            double resultDouble;
            if (double.TryParse(strValue, out resultDouble))
            {
                return resultDouble;
            }
            return DependencyProperty.UnsetValue;
        }
    }
}

using System.Globalization;

namespace JOINT_G.Helpers
{
    /// <summary>
    /// エンコーディングの問題を防ぐために、CultureInfo を変更します。
    /// </summary>
    internal static class CultureHelper
    {
        private const string JapanCultureName = "ja-JP";

        public static void SetJapaneseUiCulture()
        {
            if (CultureInfo.CurrentUICulture.Name != JapanCultureName)
            {
                var japanCulture = CultureInfo.GetCultureInfo(JapanCultureName);

                CultureInfo.DefaultThreadCurrentCulture = japanCulture;
                CultureInfo.DefaultThreadCurrentUICulture = japanCulture;
                CultureInfo.CurrentCulture = japanCulture;
                CultureInfo.CurrentUICulture = japanCulture;
            }
        }
    }
}

using FCWPF.FCCommon.FCControls.Combination;
using FCWPF.FCCommon.FCControls.Select;
using FCWPF.FCCommon.FCControls.Text;
using FCWPF.FCCommon.FCExtentions.PropertyGrid;
using FCWPF.FCCommon.FCExtentions.PropertyGrid.Editor;
using FCWPF.FCCommon.FCExtFunction;
using FCWPF.FCCommon.FCFunction;
using JOINT_G.Models.Interfaces;
using JOINT_G.Repository;
using JOINT_G.ViewModels.DetailsScreen;
using newfast;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace JOINT_G.Helpers
{
    /// <summary>
    /// 詳細画面のヘルパークラス。いろいろ便利な関数を実装。
    /// </summary>
    public static class DetailScreenHelper
    {
        private static IDetailScreenViewModel _viewModel;
        private static FCPropertyGrid _propGrid;
        public static bool HandelCombobox(string propName,
                                          FCComboBox fCComboBox = null)
        {
            try
            {
                var cbb = fCComboBox ?? _propGrid.GetEditor(propName) as FCComboBox;
                if (cbb == null)
                    return true;

                if (string.IsNullOrEmpty(cbb.Text))
                {
                    return false;
                }
                if (cbb.ItemsSource is List<FCComboBoxClass> itemSrc)
                {
                    if (itemSrc.Any(x => x.Key.Any(y => char.IsLetter(y))))
                    {
                        if ((cbb.ItemsSource as List<FCComboBoxClass>).Any(x => x.Both == cbb.Text))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    }
                    var textBox = cbb.GetTextBox();
                    if (itemSrc.Any(x => x.Both == textBox.Text))
                    {
                        return true;
                    }

                    var indexOf = itemSrc.Any(x => x.Key.Equals(cbb.Text));
                    var temp = cbb.Text;
                    if (!indexOf)
                    {
                        return false;
                    }
                    else
                    {
                        cbb.SelectedIndex = -1;
                        cbb.SelectedIndex = Convert.ToInt32(temp);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return true;
            }
        }
        public static bool ValidateTextBox(string propName,
                                          FCTextBox fCTextBox = null)
        {
            try
            {
                var textBoxObject = fCTextBox ?? _propGrid.GetEditor(propName) as FCTextBox;
                if (textBoxObject == null)
                    return true;
                if (string.IsNullOrEmpty(textBoxObject.Text)) return false;
                if (textBoxObject.FormatString.Contains("f"))
                {

                    double i;
                    bool isDouble = double.TryParse(textBoxObject.Text, out i);
                    if (!isDouble)
                    {
                        textBoxObject.SelectAll();
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    int x;
                    if (!int.TryParse(textBoxObject.Text, out x))
                    {
                        textBoxObject.SelectAll();
                        return false;
                    }
                    else return true;
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return false;
            }
        }

        public static void LoadEventAndSetBinding(FCPropertyGrid propGrid,
                                                  IDetailScreenViewModel viewModel,
                                                  EventHandler eventComboxboxInitialized,
                                                  EventHandler eventTextBoxInitialized,
                                                  EventHandler eventFCInputDimensionInitialized = null,
                                                  Func<string, bool> ignoreCallback = null)
        {
            try
            {
                _viewModel = viewModel;
                _propGrid = propGrid;
                Type myType = viewModel.GetType();
                IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());

                for (int i = 1; i <= viewModel.PropNumber; i++)
                {
                    var editor = propGrid.GetEditor($"Prop{i}") as FrameworkElement;
                    if (editor == null) continue;
                    editor.Name = $"Prop{i}";

                    var propertyBox = propGrid.GetPropertyBox(editor.Name);
                    if (propertyBox != null)
                        propertyBox.Name = $"Prop{i}";


                    if (editor is FCComboBox)
                    {
                        var combobox = editor as FCComboBoxExpansion;

                        combobox.FormatString = viewModel.FormatStrings[$"Prop{i}"];

                        if (combobox != null)
                        {
                            try
                            {
                                combobox.TextInputMode = FCTextBoxCommon.INPUTMODE.HALF;

                                if (props.FirstOrDefault(x => x.Name == combobox.Name + "Item").GetValue(null) is Dictionary<int, string> itemSrc)
                                {
                                    FCControlCommon.SetCandidate(combobox, itemSrc.ToDictionary(x => x.Key.ToString(), x => x.Value), ".", nameof(FCComboBoxClass.Both));
                                }
                                else if (props.FirstOrDefault(x => x.Name == combobox.Name + "Item").GetValue(null) is Dictionary<string, string> itemSrcString)
                                {
                                    FCControlCommon.SetCandidate(combobox, itemSrcString, "", nameof(FCComboBoxClass.Both));

                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Error(viewModel.GetType().Name + "Error Not Found Combobox Item : " + combobox.Name);
                            }
                            combobox.SetBinding(FCComboBox.SelectedValueProperty, new Binding($"SelectedValue{combobox.Name}") { Source = viewModel });



                            combobox.Initialized += eventComboxboxInitialized;
                        }

                    }
                    else if (editor is FCTextBox textbox)
                    {
                        textbox.Initialized += eventTextBoxInitialized;

                        textbox.FormatString = viewModel.FormatStrings[$"Prop{i}"];
                    }
                }

                if (_viewModel.GetType() == typeof(DetailsScreenTGPViewModel))
                {
                    for (var i = 1; i <= 4; i++)
                    {
                        var editor = propGrid.GetEditor($"CTValue{i}") as FrameworkElement;
                        if (editor == null) continue;
                        editor.Name = $"CTValue{i}";

                        var propertyBox = propGrid.GetPropertyBox(editor.Name);
                        if (propertyBox != null)
                            propertyBox.Name = $"CTValue{i}";

                        if (editor is FCTextBox textbox)
                        {
                            textbox.Initialized += eventTextBoxInitialized;

                            textbox.FormatString = "%6.1f";
                        }
                        else if (editor is FCComboBoxExpansion cb)
                        {
                            cb.FormatString = "%2d";

                            if (editor.Name == "CTValue1")
                            {
                                var itemSrc = DetailsScreenTGPViewModel.JW_TX_GTBZComboboxItem;
                                FCControlCommon.SetCandidate(cb, itemSrc.ToDictionary(x => x.Key.ToString(), x => x.Value), ".", nameof(FCComboBoxClass.Both));
                                cb.SetBinding(FCComboBox.SelectedValueProperty, new Binding("SeletedValue_JW_TX_GTBZ") { Source = viewModel });
                            }
                            else
                            {
                                var itemSrc = DetailsScreenTGPViewModel.JW_TX_GTKKComboboxItem;
                                FCControlCommon.SetCandidate(cb, itemSrc.ToDictionary(x => x.Key.ToString(), x => x.Value), ".", nameof(FCComboBoxClass.Both));
                                cb.SetBinding(FCComboBox.SelectedValueProperty, new Binding("SeletedValue_JW_TX_GTKK") { Source = viewModel });
                            }
                            cb.Initialized += eventComboxboxInitialized;
                        }
                        else if (editor is FCInputDimension id)
                        {
                            id.Initialized += eventFCInputDimensionInitialized;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        public static void SetBindingEnabled()
        {
            try
            {
                Type myType = _viewModel.GetType();
                IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());

                for (int i = 1; i <= _viewModel.PropNumber; i++)
                {
                    var editor = _propGrid.GetEditor("Prop" + i.ToString()) as FrameworkElement;
                    if (editor == null) continue;

                    var propertyBox = _propGrid.GetPropertyBox(editor.Name);
                    if (propertyBox == null) continue;

                    // Set Binding Enable and Set Event IsEnabledChanged
                    if (props.Any(prop => prop.Name == "Prop" + i.ToString() + "Enabled"))
                    {
                        propertyBox.SetBinding(UIElement.IsEnabledProperty, new Binding("Prop" + i.ToString() + "Enabled") { Source = _viewModel });

                        propertyBox.Label.IsEnabledChanged += (sender, e) =>
                        {
                            FrameworkElement frameworkElement = (sender as FrameworkElement);
                            FCControlCommon.EnableControl(frameworkElement, frameworkElement.IsEnabled);

                            if (frameworkElement.Name == "SepecialLabel")
                            {

                                if (!(sender as FrameworkElement).IsEnabled)
                                {
                                    return;
                                }
                                (frameworkElement as TextBlock).Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                            }
                        };
                        propertyBox.Label.SetBinding(UIElement.IsEnabledProperty, new Binding("Prop" + i.ToString() + "Enabled") { Source = _viewModel });
                        FCControlCommon.EnableControl(propertyBox.Label, propertyBox.Label.IsEnabled);
                    }

                    // Set Color for Label is category
                    var categoryPropertyBox = _propGrid.GetPropertyBox("Category" + i.ToString());
                    if (categoryPropertyBox != null)
                    {
                        var text = categoryPropertyBox.Label.Text;
                        categoryPropertyBox.Label.Text = text.Replace("Category", "");
                        categoryPropertyBox.Label.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                        if (props.Any(prop => prop.Name == "Category" + i.ToString() + "Enabled"))
                        {
                            categoryPropertyBox.Label.IsEnabledChanged += (sender, e) =>
                            {
                                FCControlCommon.EnableControl((sender as FrameworkElement), (sender as FrameworkElement).IsEnabled);
                            };
                            categoryPropertyBox.Label.SetBinding(UIElement.IsEnabledProperty, new Binding("Category" + i.ToString() + "Enabled") { Source = _viewModel });
                        }

                        continue;
                    }


                    if (editor is FCLabel labelEditor)
                    {
                        if (_viewModel.GetType() != typeof(DetailsScreenFSPViewModel))
                        {

                            labelEditor.Visibility = Visibility.Hidden;
                            propertyBox.Label.SetBinding(TextBlock.TextProperty, new Binding($"PropertyGridModel.Prop{i}") { Source = _viewModel, });

                            if (!props.Any(prop => prop.Name == $"Prop{i}Enabled"))
                            {
                                propertyBox.Label.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                            }
                            else
                            {

                                propertyBox.Label.Name = "SepecialLabel";
                                var isEnabled = props.FirstOrDefault(x => x.Name == $"Prop{i}Enabled");

                                if (isEnabled == null)
                                {
                                    propertyBox.Label.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                                }
                                else
                                {

                                    if (Convert.ToBoolean(isEnabled.GetValue(_viewModel)))
                                    {
                                        propertyBox.Label.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                                    }
                                }

                            }
                        }
                        else if (_viewModel.GetType() == typeof(DetailsScreenFSPViewModel) && i == 20)
                        {
                            if (!props.Any(prop => prop.Name == $"Prop{i}Enabled"))
                            {
                                propertyBox.Label.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                            }
                            else
                            {

                                propertyBox.Label.Name = "SepecialLabel";
                                var isEnabled = props.FirstOrDefault(x => x.Name == $"Prop{i}Enabled");

                                if (isEnabled == null)
                                {
                                    propertyBox.Label.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                                }
                                else
                                {

                                    if (Convert.ToBoolean(isEnabled.GetValue(_viewModel)))
                                    {
                                        propertyBox.Label.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                                        labelEditor.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                                        labelEditor.IsEnabledChanged += (sender, args) =>
                                        {
                                            FCLabel label = (sender as FCLabel);

                                            label.Foreground = (label.IsEnabled) ? Brushes.Black : Brushes.LightGray;
                                            if (!label.IsEnabled)
                                            {
                                                return;
                                            }
                                            label.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                                        };
                                        labelEditor.SetBinding(FrameworkElement.IsEnabledProperty, new Binding("IsEnabled") { Source = propertyBox.Label });
                                    }
                                }

                            }
                        }
                        else
                        {
                            propertyBox.Label.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                            labelEditor.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        public static Dictionary<int, string> GetStandards()
        {

            var standardMembers = DataRepository.GetStandardMembers();
            if (standardMembers == null)
                return new Dictionary<int, string>();
            var data = new Dictionary<int, string>();
            for (int i = 0; i < standardMembers.Length; i++)
            {
                standardMembers[i] = standardMembers[i];
                data.Add(i, standardMembers[i]);
            }
            return data;
        }
        public static string GetTitle(DetailType detailType)
        {
            return DataRepository.GetTitle(detailType) ?? "";
        }
        public static string[] GetMemberInfor(DetailType detailType)
        {
            try
            {
                return DataRepository.GetMemberInfor(detailType)?.ToArray() ?? new string[3];
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return new string[3];
            }
        }

        public static void RefreshFCPropertyGrid()
        {
            if (_viewModel.Errors.Count == 0 && _propGrid != null)
            {
                DataLinkAll(_propGrid);
                FCPropertyGrid.InitFormatData(_propGrid);
            }
        }

        public static void DataLinkAll(FCPropertyGrid fCPropertyGrid)
        {
            int cnt = fCPropertyGrid.PropertyBoxes.Count;

            for (int i = 0; i < cnt; i++)
            {
                string memberName = fCPropertyGrid.PropertyBoxes[i].PropertyAttribute.MemberName;

                IFCEditor editor = fCPropertyGrid.GetEditor(memberName);

                if (editor is FCComboBox)
                {
                    continue;
                }
                else if (editor is Control ctrl)
                {
                    FCExtControlCommon.DataLink(fCPropertyGrid.PropertyBoxes[i].PropertyAttribute, ctrl);
                }
            }
        }

        public static bool CanCheckProp(string propName)
        {
            Type myType = _viewModel.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());

            // If Check All Prop , pass field Have IsEnable = False 
            if (props.Any(x => x.Name == propName + "Enabled")
                 && Convert.ToBoolean(props.FirstOrDefault(x => x.Name == propName + "Enabled").GetValue(_viewModel)) == false
               )
            {
                return false;
            }

            if (_propGrid.GetEditor(propName) is FCLabel)
            {
                return false;
            }
            return true;
        }

        public static bool CanUpdateCombobox([CallerMemberName] string propName = null)
        {
            propName = propName.Replace("SelectedValue", "");
            if (_propGrid is null)
                return true;
            if (_propGrid.GetEditor(propName) is FCComboBox cb)
            {
                return HandelCombobox(propName, cb);
            }
            return true;
        }


    }
}

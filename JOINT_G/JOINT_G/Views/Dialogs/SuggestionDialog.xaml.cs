﻿using JOINT_G.Helpers;
using JOINT_G.ViewModels.Dialogs;
using System;
using System.Windows;
using System.Windows.Input;

namespace JOINT_G.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for SuggestionDialog.xaml
    /// </summary>
    public partial class SuggestionDialog : Window
    {
        private readonly double _left;
        private readonly double _top;

        public SuggestionDialog(double left, double top, SuggestionDialogViewModel vm)
        {
            InitializeComponent();
            DataContext = vm;
            listDimension.Focus();
            _left = left;
            _top = top;
        }

        private void CommandAction(Action<SuggestionDialogViewModel> callback, bool dialogAttack)
        {
            try
            {
                if (!(DataContext is SuggestionDialogViewModel model))
                {
                    return;
                }
                if (dialogAttack)
                {
                    callback(model);
                    Hide();
                }
                else
                {
                    callback(model);
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
        }

        private void ListDimension_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                CommandAction(model => model.SetDimension(), true);
            }
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {
            CommandAction(model => model.SetDimension(), true);
        }

        private void ListDimension_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            CommandAction(model => model.SetDimension(), true);
        }

        private void ListDimension_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            CommandAction(model => model.SetDimension(), true);
        }

        private void SuggestionDialog_Load(object sender, RoutedEventArgs e)
        {
            this.Left = _left + 345;
            this.Top = _top + 615;
        }
    }
}

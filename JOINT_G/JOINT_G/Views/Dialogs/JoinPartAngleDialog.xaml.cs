﻿using FCWPF.FCCommon.FCControls.Select;
using FCWPF.FCCommon.FCControls.Text;
using FCWPF.FCCommon.FCFunction;
using JOINT_G.Extensions;
using JOINT_G.ViewModels.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using DoubleConverter = JOINT_G.Helpers.DoubleConverter;

namespace JOINT_G.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for JoinPartAngleDialog.xaml
    /// </summary>
    public partial class JoinPartAngleDialog : Window, INotifyPropertyChanged
    {
        private bool _isValidate = false;
        private bool isOkClick = false;

        public event PropertyChangedEventHandler PropertyChanged;

        public JoinPartAngleDialog()
        {
            InitializeComponent();

            ViewModel = new JoinPartAngleDialogViewModel();

            DataContext = ViewModel;
            checkBlock.Loaded += (s, e) =>
            {
                checkBlock.TopElement = shapeCombobox;
                checkBlock.DelegateOKProcs.Add(ProcessOk);
            };

            tan1.AutoMove = true;
            tan2.AutoMove = true;
            frequency.AutoMove = true;
            shapeCombobox.AutoMove = true;
            gradientCombobox.AutoMove = true;

            tan1.SetBinding(FCTextBox.TextProperty,
              new Binding("Tan1")
              {
                  Source = ViewModel,
                  Converter = new DoubleConverter(),
                  ConverterParameter = tan1.FormatString,
                  Mode = BindingMode.OneWay
              }
                                    );

            tan2.SetBinding(FCTextBox.TextProperty,
              new Binding("Tan2")
              {
                  Source = ViewModel,
                  Converter = new DoubleConverter(),
                  ConverterParameter = tan2.FormatString,
                  Mode = BindingMode.OneWay
              }
                                    );

            frequency.SetBinding(FCTextBox.TextProperty,
              new Binding("Frequency")
              {
                  Source = ViewModel,
                  Converter = new DoubleConverter(),
                  ConverterParameter = frequency.FormatString,
                  Mode = BindingMode.OneWay
              }
                                    );
            FormatTextbox();
        }

        internal JoinPartAngleDialogViewModel ViewModel { get; set; }

        private void ProcessOk()
        {
            Topmost = false;
            isOkClick = true;
            List<object> textboxToValidate = new List<object>(new[] { frequency, tan1, tan2 });
            _isValidate = true;
            foreach (object objectToValidate in textboxToValidate)
            {
                CheckInput(objectToValidate, null);
            }
            if (_isValidate)
            {
                Topmost = true;
                ViewModel.Update();
                DialogResult = true;
            }
            else
            {
                isOkClick = false;
            }
        }

        private void ProcessCancel()
        {
            DialogResult = false;
        }

        private void OkButtonClick(object sender, RoutedEventArgs e) => ProcessOk();

        private void CancelButtonClick(object sender, RoutedEventArgs e) => ProcessCancel();

        private void OnKeyPressed(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.F2 when ViewModel.OkButtonEnabled:
                    buttonCancel.Focus();
                    ProcessOk();
                    return;

                case Key.F1:
                    ProcessCancel();
                    return;
            }
        }

        private void CheckInput(object sender = null, KeyEventArgs e = null)
        {
            if (Keyboard.IsKeyDown(Key.Enter) || Keyboard.IsKeyDown(Key.F9) || isOkClick)
            {
                var textBox = sender as FCTextBox;
                if (textBox == null)
                    return;
                double value;
                int isValid = 0;
                if (!double.TryParse(textBox.Text, out value))
                    isValid = -1;

                if (isValid == 0)
                {
                    if (textBox.Name == "frequency")
                    {
                        if (textBox.IsEnabled && (value < 0.1 || value > 98.0))
                            isValid = -1;
                        else
                            ViewModel.Frequency = Convert.ToDouble(textBox.Text);
                    }
                    else if (textBox.Name == "tan1")
                    {
                        if (textBox.IsEnabled && value < 0.1)
                            isValid = -1;
                        else
                            ViewModel.Tan1 = Convert.ToDouble(textBox.Text);
                    }
                    else if (textBox.Name == "tan2")
                    {
                        if (textBox.IsEnabled && value < 0.1)
                            isValid = -1;
                        else
                            ViewModel.Tan2 = Convert.ToDouble(textBox.Text);
                    }
                }
                if (isValid < 0)
                {
                    _isValidate = false;
                    this.ShowWarning("入力値が不正です。", "入力エラー");
                    if (!(e is null))
                        e.Handled = true;
                    FCControlCommon.FocusSelect(textBox);
                }
                else if (textBox.Name == "tan2" && textBox.IsEnabled)
                {
                    ViewModel.Tan2Do();
                    frequency.Text = ViewModel.Frequency.ToString();
                    FormatTextbox();
                }
                else
                {
                    FormatTextbox();
                }
            }
        }

        private void FormatTextbox()
        {

            tan1.Text = FCTextBoxCommon.Convert(tan1.Text, tan1.FormatString);
            tan2.Text = FCTextBoxCommon.Convert(tan2.Text, tan2.FormatString);
            frequency.Text = FCTextBoxCommon.Convert(frequency.Text, frequency.FormatString);

        }

        private void ShapeValue_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var combobox = sender as FCComboBox;

            if (Convert.ToInt32(combobox.SelectedValue) == 1)
            {
                ViewModel.Tan2Enabled = true;
                ViewModel.Tan1Enabled = true;
                ViewModel.FrequencyEnabled = true;
                ViewModel.GradientInputMethodEnabled = true;
                var temp = ViewModel.GradientInputMethodValue;
                gradientCombobox.SelectedValue = temp == 0 ? 1 : 0;
                gradientCombobox.SelectedValue = temp;
            }
            else
            {
                ViewModel.Tan2Enabled = false;
                ViewModel.Tan1Enabled = false;
                ViewModel.FrequencyEnabled = false;
                ViewModel.GradientInputMethodEnabled = false;
                gradientCombobox.SelectedValue = ViewModel.GradientInputMethodValue;
                tan1.Text = "0";
                tan2.Text = "0";
                frequency.Text = "0";
                FormatTextbox();
            }
        }

        private void Gradient_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            var combobox = sender as FCComboBox;
            if (Convert.ToInt32(combobox.SelectedValue) == 1 && ViewModel.ShapeValue == 1)
            {
                ViewModel.FrequencyEnabled = false;
                ViewModel.Tan1Enabled = true;
                ViewModel.Tan2Enabled = true;
            }
            else if (ViewModel.ShapeValue == 1 && Convert.ToInt32(combobox.SelectedValue) == 0)
            {
                ViewModel.FrequencyEnabled = true;
                ViewModel.Tan1Enabled = false;
                ViewModel.Tan2Enabled = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GridLength labelColumnWidth = new GridLength(1, GridUnitType.Star);
            GridLength textboxColumnWidth = new GridLength(28, GridUnitType.Pixel);
            checkBlock.CdLbl.Width = labelColumnWidth;
            checkBlock.CdTxt.Width = textboxColumnWidth;

            FCControlCommon.FocusSelect(shapeCombobox);
        }
    }
}

﻿using FCWPF.FCCommon.FCControls.Select;
using FCWPF.FCCommon.FCFunction;
using JOINT_G.Structs;
using JOINT_G.ViewModels.Dialogs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JOINT_G.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for DisplayMethodDialog.xaml
    /// </summary>
    public partial class DisplayMethodDialog : Window
    {
        private DisplayMethodDialogViewModel ViewModel { get; set; }

        public DisplayMethodDialog()
        {
            InitializeComponent();
            checkBlock.TopElement = calcMethodValue;
            checkBlock.DelegateOKProcs.Add(ProcessOk);
            ViewModel = new DisplayMethodDialogViewModel();
            DataContext = ViewModel;
        }
        private void LoadedWindowCommand(object sender, EventArgs e)
        {
            ViewModel.LoadedWindowCommandExecuted(sender, e);
        }

        private void CalcMethodValue_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var combobox = sender as FCComboBox;
            if (Convert.ToInt32(combobox.SelectedValue) == 1)
            {
                ViewModel.OutputMethodComboboxEnabled = true;
                ViewModel.PositionComboboxEnabled = false;
                position.SelectedIndex = ViewModel.PositionValue ?? 0;
                ViewModel.OutputMethodValue = 0;
            }
            else
            {
                ViewModel.OutputMethodComboboxEnabled = false;
                outputMethod.SelectedIndex = ViewModel.OutputMethodValue ?? 0;
                ViewModel.PositionComboboxEnabled = true;
                ViewModel.PositionValue = 0;
            }
        }
        private void ProcessOk()
        {
            ViewModel.UpdateFilter();
            DialogResult = true;
        }

        private void ProcessCancel()
        {
            DialogResult = false;
        }
        private void OkButtonClick(object sender, RoutedEventArgs e) => ProcessOk();

        private void CancelButtonClick(object sender, RoutedEventArgs e) => ProcessCancel();

        private void OnKeyPressed(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.F2 when ViewModel.OkButtonEnabled:
                    ProcessOk();
                    return;

                case Key.F1:
                    ProcessCancel();
                    return;
            }
        }

        private void DisplayMethodDialogWin_Loaded(object sender, RoutedEventArgs e)
        {
            GridLength labelColumnWidth = new GridLength(1, GridUnitType.Star);
            GridLength textboxColumnWidth = new GridLength(28, GridUnitType.Pixel);
            checkBlock.CdLbl.Width = labelColumnWidth;
            checkBlock.CdTxt.Width = textboxColumnWidth;

            FCControlCommon.FocusSelect(calcMethodValue);
        }
    }
}

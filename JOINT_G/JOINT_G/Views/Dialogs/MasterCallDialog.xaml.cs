﻿using JOINT_G.Extensions;
using JOINT_G.ViewModels.Dialogs;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace JOINT_G.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for MasterCallDialog.xaml
    /// </summary>
    public partial class MasterCallDialog : Window
    {
        private List<string> _jointList;
        internal MasterCallDialogViewModel ViewModel { get; }

        public MasterCallDialog(List<string> jointList)
        {
            InitializeComponent();

            ViewModel = new MasterCallDialogViewModel();
            _jointList = jointList;
            checkBlock.TopElement = newMarkName;
            checkBlock.DelegateOKProcs.Add(ProcessOk);
            DataContext = ViewModel;
        }
        private void ProcessOk()
        {
            if (checkValidMark())
            {
                ViewModel.CreateJointFromMeisho();
                DialogResult = true;
                this.Close();
            }
            else
            {
                newMarkName.Focus();
                newMarkName.SelectAll();
            }
        }
        private bool checkValidMark()
        {
            bool isValid = true;
            if (newMarkName.Text is null || newMarkName.Text?.Length <= 0)
            {
                this.ShowWarning("JOINTマークを入力して下さい。", "エラー");
                isValid = false;
            }
            else if (_jointList.Contains(newMarkName.Text))
            {
                this.ShowWarning("入力された新マークは既に使われています。", "エラー");
                isValid = false;
            }
            else if (ViewModel.SelectedJointMaster == null || ViewModel.SelectedMark == null)
            {
                this.ShowWarning("新マークを入力してください。", "エラー");
                isValid = false;
            }
            return isValid;
        }

        private void ProcessCancel()
        {
            DialogResult = false;
        }

        private void OkButtonClick(object sender, RoutedEventArgs e) => ProcessOk();

        private void CancelButtonClick(object sender, RoutedEventArgs e) => ProcessCancel();

        private void OnKeyPressed(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.F2 when ViewModel.OkButtonEnabled:
                    ProcessOk();
                    return;

                case Key.F1:
                    ProcessCancel();
                    return;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GridLength labelColumnWidth = new GridLength(1, GridUnitType.Star);
            GridLength textboxColumnWidth = new GridLength(28, GridUnitType.Pixel);
            checkBlock.CdLbl.Width = labelColumnWidth;
            checkBlock.CdTxt.Width = textboxColumnWidth;
        }
    }
}

﻿using JOINT_G.Extensions;
using JOINT_G.ViewModels.Dialogs;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace JOINT_G.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for CopyDialog.xaml
    /// </summary>
    public partial class CopyDialog : Window
    {
        public CopyDialogViewModel ViewModel { get; set; }
        private List<string> _jointList = new List<string>();
        private string _type;
        public CopyDialog(string oldMarkName, List<string> jointList, string type)
        {
            ViewModel = new CopyDialogViewModel(oldMarkName, type);
            DataContext = ViewModel;
            InitializeComponent();
            checkBlock.TopElement = newMarkName;
            checkBlock.DelegateCancelProcs.Add(() => CheckCanceled());
            checkBlock.DelegateOKProcs.Add(() => CheckPassed());
            _jointList = jointList;
            _type = type;
        }
        private void CheckCanceled()
        {
            ViewModel.CheckChanged(false);
        }

        private void CheckPassed()
        {
            ViewModel.CheckChanged(true);
            ProcessOk();
        }
        private void ProcessOk()
        {
            Topmost = false;
            if (checkValidMark())
            {
                DialogResult = true;
                this.Close();
            }
            else
            {
                Topmost = true;
                newMarkName.Focus();
                newMarkName.SelectAll();
            }
        }

        private void ProcessCancel()
        {
            DialogResult = false;
            this.Close();
        }

        private void OkButtonClick(object sender, RoutedEventArgs e) => ProcessOk();

        private void CancelButtonClick(object sender, RoutedEventArgs e) => ProcessCancel();

        private void OnKeyPressed(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.F2 when ViewModel.OkButtonEnabled:
                    ProcessOk();
                    return;

                case Key.F1:
                    ProcessCancel();
                    return;
            }
        }

        private void LoadedWindowCommand(object sender, EventArgs e)
        {
            ViewModel.LoadedWindowCommandExecuted(sender, e);
        }
        private void Window_Activated(object sender, EventArgs e)
        {
            //Topmost = true;
        }

        private void Window_Deactived(object sender, EventArgs e)
        {
            if (Owner == null || Owner.IsActive)
                return;
            bool hasActiveWindow = false;
            foreach (Window ownedWindow in Owner.OwnedWindows)
            {
                if (ownedWindow.IsActive)
                    hasActiveWindow = true;
            }

            if (!hasActiveWindow)
                Topmost = false;
        }

        private bool checkValidMark()
        {
            bool isValid = true;
            if (newMarkName.Text.Length <= 0)
            {
                this.ShowWarning("JOINTマークを入力して下さい。", "エラー");
                isValid = false;
            }
            else if (_jointList.Contains(newMarkName.Text))
            {
                switch (_type)
                {
                    case "copy":
                    case "change":
                        this.ShowWarning($"入力された新マークは既に使われています。", "エラー");
                        isValid = false;
                        break;
                    case "edit":
                        bool select = this.ShowWarningConfirmation($" JOINTマーク「{newMarkName.Text}」は既に登録されています。", "入力確認");
                        isValid = select;
                        break;
                }
            }
            return isValid;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GridLength labelColumnWidth = new GridLength(1, GridUnitType.Star);
            GridLength textboxColumnWidth = new GridLength(28, GridUnitType.Pixel);
            checkBlock.CdLbl.Width = labelColumnWidth;
            checkBlock.CdTxt.Width = textboxColumnWidth;
        }
    }
}

﻿using FCWPF.FCCommon.FCControls.Select;
using FCWPF.FCCommon.FCControls.Text;
using FCWPF.FCCommon.FCFunction;
using JOINT_G.Helpers;
using JOINT_G.Models;
using JOINT_G.ViewModels.DetailsScreen;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using JOINT_G.Models.Interfaces;
using JOINT_G.Models.Enums;
using JOINT_G.Extensions;
using FCWPF.FCCommon.FCExtentions.PropertyGrid;
using FCWPF.FCCommon.FCControls.Combination;

namespace JOINT_G.Views.DetailsScreen
{
    /// <summary>
    /// Interaction logic for DetailsScreenGP.xaml
    /// </summary>
    public partial class DetailsScreenJOINT_G_04 : Window
    {
        // WSP , GP , GSP 

        private bool _isBackToDt03 = false;
        public bool IsBackToDt03 { get => _isBackToDt03; }
        private List<string> _comboboxLoadedList = new List<string>();
        private bool _validate;
        private WebType _webType;

        public DetailsScreenJOINT_G_04(WebType webType, bool haveDt03)
        {
            Application.Current.MainWindow = this;
            InitializeComponent();
            switch (webType)
            {
                case WebType.SP:
                    ViewModel = new DetailsScreenWSPViewModel(new DetailsModelWSP(), DrawArea2);
                    break;
                case WebType.GP:
                    ViewModel = new DetailsScreenGPViewModel(new DetailsModelGP(), DrawArea2);
                    break;
                case WebType.GSP:
                    ViewModel = new DetailsScreenGSPViewModel(new DetailsModelGSP(), DrawArea2);
                    break;
            }
            DataContext = ViewModel;
            _webType = webType;

            var fcTextBoxHeight = 0.0;
            if (ViewModel.MemberInfors != null)
            {
                foreach (var i in ViewModel.MemberInfors)
                {
                    if (!String.IsNullOrEmpty(i))
                    {
                        FCLabel fCTextBox = new FCLabel();
                        fCTextBox.Text = i;
                        fCTextBox.Padding = new Thickness(5);
                        compomentInforStackPanel.Children.Add(fCTextBox);
                        fcTextBoxHeight += 25;
                    }
                }

                compomentInforStackPanel.Height = fcTextBoxHeight + 5;
            }

            propGrid.AutoMove = true;
            if (!haveDt03)
            {
                buttonF3.IsEnabled = false;
            }
            checkBlock.Loaded += (sender, e) =>
            {
                (sender as FCCheckTextBox).TextBox.Padding = new Thickness(0, 4, 0, 0);
            };

            propGrid.NextControl = checkBlock;
        }

        public IDetailScreenViewModel ViewModel { get; set; }

        private void ResizePropGrid()
        {
            ViewModel.PropertyGridModel.ResizePropGrid(propGridWrapper.ActualHeight, propGrid);
        }

        private void ComboboxInitialized(object s, EventArgs e)
        {
            var combobox = (s as FCComboBoxExpansion);

            combobox.LostFocus += (s, e) =>
            {
                help.Content = "";
                help.Background = Brushes.White;
            };

            combobox.GotFocus += (s, e) =>
            {
                if (s is ComboBox combobox)
                {

                    string content = null;
                    if (combobox.ItemsSource is List<FCComboBoxClass> itemSrc && itemSrc.Count <= 10)
                    {
                        content = "[  ";
                        foreach (var item in itemSrc)
                        {
                            content = content + item.Both + "  ";
                        }
                        content = content + "]";
                    }
                    help.Content = content;
                    help.Background = Brushes.LightYellow;
                }
            };

            combobox.GotFocus += LoadGuidImage;

            combobox.Loaded += (s, e) =>
            {

                var combobox = s as FCComboBoxExpansion;
                if (_comboboxLoadedList.Contains(combobox.Name))
                    return;

                combobox.GetTextBox().Name = (s as FrameworkElement).Name;

                combobox.GetTextBox().PreviewLostKeyboardFocus += (s, e) =>
                {

                    if (s is TextBox textBox)
                    {
                        if (
                             !Keyboard.IsKeyDown(Key.Enter)
                             && !Keyboard.IsKeyDown(Key.F9)
                           )
                        {
                            _validate = ViewModel.ValidateProperty(textBox.Name);
                            DetailScreenHelper.RefreshFCPropertyGrid();

                        }

                        if (!_validate
                            && Keyboard.IsKeyDown(Key.Enter)
                         )
                        {
                            var result = ShowDialog();

                            if (!result)
                            {
                                e.Handled = true;
                                FCControlCommon.FocusSelect(textBox);
                            }
                        }
                    }

                };
                combobox.GetTextBox().PreviewKeyDown += (s, e) =>
                {

                    if (s is TextBox textBox)
                    {
                        if (e.Key == Key.Enter || e.Key == Key.F9)
                        {
                            _validate = ViewModel.ValidateProperty(textBox.Name);
                            DetailScreenHelper.RefreshFCPropertyGrid();
                        }
                    }
                };

                _comboboxLoadedList.Add(combobox.Name);
            };

            combobox.DropDownClosed += (s, e) =>
            {


                if (!Keyboard.IsKeyDown(Key.Enter))
                {
                    Window w = Window.GetWindow(this);

                    IInputElement element = FocusManager.GetFocusedElement(w);

                    if (
                        null != element
                        && true == combobox.IsEditable
                        && element is TextBox tb
                    )
                    {
                        _validate = ViewModel.ValidateProperty((s as FrameworkElement).Name);
                        DetailScreenHelper.RefreshFCPropertyGrid();
                        FCControlCommon.MoveFocus(tb, Key.Enter);
                    }
                    else
                    {
                        _validate = ViewModel.ValidateProperty((s as FrameworkElement).Name);
                        DetailScreenHelper.RefreshFCPropertyGrid();
                        FCControlCommon.MoveFocus((s as Control), Key.Enter);
                    }
                }
            };
        }

        private void TextboxInitialized(object s, EventArgs e)
        {

            if (s is TextBox textbox)
            {
                textbox.PreviewLostKeyboardFocus += TextBoxPreviewLostKeyboardFocus;
                textbox.KeyUp += TextBoxKeyUp;
                textbox.PreviewKeyDown += (s, e) =>
                {
                    var textBox = (s as TextBox);
                    if (e.Key == Key.Enter || e.Key == Key.F9 || e.Key == Key.Down || e.Key == Key.Up)
                    {
                        _validate = ViewModel.ValidateProperty(textBox.Name);
                        DetailScreenHelper.RefreshFCPropertyGrid();
                    }
                };
                textbox.GotFocus += LoadGuidImage;
            }
        }
        private void TextBoxKeyUp(object s, KeyEventArgs e)
        {
            if (e.Key == Key.Down || e.Key == Key.Up)
            {
                if (e.Key == Key.Down)
                {
                    propGrid.FCPropertyGridKeyDownCommonProc(Key.Enter);
                }
                else
                {
                    propGrid.FCPropertyGridKeyDownCommonProc(Key.F9);
                }
            }
        }
        private void TextBoxPreviewLostKeyboardFocus(object s, KeyboardFocusChangedEventArgs e)
        {

            if (s is TextBox textbox)
            {
                // Lose focus but not Press Key
                if (
                    !Keyboard.IsKeyDown(Key.Down)
                    && !Keyboard.IsKeyDown(Key.Enter)
                    && !Keyboard.IsKeyDown(Key.F9)
                    && !Keyboard.IsKeyDown(Key.Up))
                {
                    _validate = ViewModel.ValidateProperty(textbox.Name);
                    DetailScreenHelper.RefreshFCPropertyGrid();
                }

                if (!_validate && Keyboard.IsKeyDown(Key.Enter))
                {
                    var result = ShowDialog();

                    if (!result)
                    {
                        e.Handled = true;
                        FCControlCommon.FocusSelect(textbox);
                    }
                }
            }
        }


        private void PropGridLoaded(object sender, RoutedEventArgs e)
        {
            propGrid.SelectedObject = ViewModel.PropertyGridModel;
            DetailScreenHelper.LoadEventAndSetBinding(propGrid,
                                                      ViewModel,
                                                      new EventHandler(ComboboxInitialized),
                                                      new EventHandler(TextboxInitialized)
                                                      );
        }
        private void PropBoxesLoaded(object sender, EventArgs e)
        {

            ViewModel.PropertyGridModel.Init(propGrid);
            DetailScreenHelper.SetBindingEnabled();
            FCPropertyGrid.InitFormatData(propGrid);
            ResizePropGrid();
            (propGrid.GetEditor("Prop1") as FrameworkElement).Focus();
            checkBlock.DelegateOKProcs.Add(ProcessOk);


            checkBlock.TopElement = propGrid.GetEditor("Prop1") as FrameworkElement;

            propGrid.NextControl = checkBlock;

            propGrid.PrevControl = propGrid.GetEditor("Prop1") as Control;
        }


        private void buttonF5_Click(object sender = null, RoutedEventArgs e = null)
        {
            try
            {
                var option = this.ShowWarningConfirmation("削除したボルトを全て取消しますか？", "ボルト削除全取消");
                if (option)
                    ViewModel.ClearSelectBolt();
            }
            catch (Exception ex)
            {

                ex.HandleException();
            }
        }
        private void buttonF4_Click(object sender = null, RoutedEventArgs e = null)
        {
            try
            {
                Disable(false);
                ViewModel.SelectBolt();
                var str = "";

                BindingExpression bindingExpression = BindingOperations.GetBindingExpression(LineInfo, TextBlock.TextProperty);
                if (bindingExpression != null)
                {
                    PropertyInfo? property = bindingExpression.DataItem.GetType().GetProperty(bindingExpression.ParentBinding.Path.Path);
                    if (property != null)
                    {
                        property.SetValue(bindingExpression.DataItem, str, null);
                        Disable(true);
                    }

                }
            }
            catch (Exception ex)
            {

                ex.HandleException();
            }
        }
        private void buttonF3_Click(object sender, RoutedEventArgs e)
        {
            _isBackToDt03 = true;
            DialogResult = false;
            this.Close();
        }
        private void FCButtonOk_Click(object sender, RoutedEventArgs e)
        {
            ProcessOk();
        }
        private void FCButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }
        private void OnKeyPressed(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.F5 when buttonF5.IsEnabled:
                    buttonF5_Click();
                    break;
                case Key.F4:
                    buttonF4_Click();
                    break;
                case Key.F3 when buttonF3.IsEnabled:
                    buttonF3_Click(null, null);
                    return;
                case Key.F2 when buttonF5.IsEnabled:
                    buttonOk.Focus();
                    FCButtonOk_Click(sender, e);
                    break;

                case Key.F1 when buttonF5.IsEnabled:
                    buttoncancel.Focus();
                    FCButtonCancel_Click(sender, e);
                    break;
            }
        }

        private void ProcessOk()
        {
            //this.IsEnabled = false;
            var isValidate = ViewModel.ValidateProperty("ALL");

            if (isValidate)
            {
                DialogResult = true;
                this.Close();
            }
            else
            {
                for (var j = 0; j < ViewModel.Errors.Count; j++)
                {
                    var option = ShowDialog(j);

                    if (!option)
                    {
                        (propGrid.GetEditor(ViewModel.Errors[j].PropName) as FrameworkElement).Focus();
                        return;
                    }
                }
                DialogResult = true;
                this.Close();
            }
        }
        private void Disable(bool enable)
        {
            propGrid.IsEnabled = enable;
            buttoncancel.IsEnabled = enable;
            buttonF3.IsEnabled = enable;
            buttonF4.IsEnabled = enable;
            buttonF5.IsEnabled = enable;
            buttonOk.IsEnabled = enable;
            checkBlock.IsEnabled = enable;


            if (enable == true)
                FCControlCommon.FocusSelect(checkBlock);
        }



        private bool ShowDialogWarning(ErrorDetails errorDetails)
        {
            try
            {
                bool option = false;
                Application.Current.Dispatcher.Invoke(() =>
                {
                    option = this.ShowWarningConfirmation(errorDetails.ErrorMessage, "警告");
                });
                return option;
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return false;
            }
        }
        private void ShowDialogError(ErrorDetails errorDetails)
        {
            try
            {
                Application.Current.Dispatcher.Invoke(() => this.ShowWarning(errorDetails.ErrorMessage, "エラー"));
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        private bool ShowDialog(int index = 0)
        {
            //this.IsEnabled = false;
            var errroDetail = ViewModel.Errors.Count == 0 ? null : ViewModel.Errors[index];

            if (errroDetail == null)
            {
                //this.IsEnabled = true;
                return false;
            }
            else if (errroDetail.ErrorStatus == -1)
            {
                ShowDialogError(errroDetail);
                //this.IsEnabled = true;
                return false;
            }
            else
            {
                var option = ShowDialogWarning(errroDetail);
                if (!option)
                {
                    //this.IsEnabled = true;
                    return false;
                }
                else
                {
                    //this.IsEnabled = true;
                    return true;
                }
            }
        }

        #region WdPanel
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            ViewModel.UpdateDrawWin();
            ViewModel.ClearSelectBolt();
        }

        public void UpdateDrawWin()
        {
            ViewModel.UpdateDrawWin();
        }

        #endregion

        private void LoadGuidImage(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement element)
                {
                    String imageSource = System.AppDomain.CurrentDomain.BaseDirectory + @"svg\file_name";
                    var index = Convert.ToInt32(element.Name.Replace("Prop", ""));
                    switch (_webType)
                    {
                        case WebType.GP:
                            switch (element.Name)
                            {
                                case "Prop19":
                                    imageSource = imageSource.Replace("file_name", "GD_JR22_GPL_KJTP.svg");
                                    break;
                                case "Prop26":
                                    imageSource = imageSource.Replace("file_name", "GD_JR22_WEBGEN_WEBKAI.svg");
                                    break;
                                default:
                                    imageSource = imageSource.Replace("file_name", "GD_JR22_GPL.svg");
                                    break;
                            }
                            break;
                        case WebType.GSP:
                            if (index <= 11)
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR22_WPIT.svg");
                            }
                            else if (index <= 26)
                            {
                                if (index == 21 || index == 22)
                                {
                                    imageSource = imageSource.Replace("file_name", "GD_JR11_CHOBAN.svg");
                                }
                                else
                                    imageSource = imageSource.Replace("file_name", "GD_JR22_WFUCHI.svg");
                            }
                            else if (index == 30)
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR22_WEBGEN_WEBKAI.svg");
                            }
                            else
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR22_WEBGEN_URAITA.svg");
                            }
                            break;
                        case WebType.SP:
                            if (index <= 11)
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR22_WPIT.svg");
                            }
                            else if (index <= 19)
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR22_WFUCHI.svg");
                            }
                            else if (index <= 22)
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR11_CHOBAN.svg");
                            }
                            else if (index == 24)
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR22_WEBGEN_WEBKAI.svg");
                            }
                            else
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR11_CHOBAN.svg");
                            }
                            break;
                    }

                    svgImgBottom.Load(new Uri(imageSource));
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GridLength labelColumnWidth = new GridLength(1, GridUnitType.Star);
            GridLength textboxColumnWidth = new GridLength(28, GridUnitType.Pixel);
            checkBlock.CdLbl.Width = labelColumnWidth;
            checkBlock.CdTxt.Width = textboxColumnWidth;
        }
    }
}

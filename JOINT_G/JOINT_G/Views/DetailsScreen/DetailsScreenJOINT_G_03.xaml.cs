﻿using FCWPF.FCCommon.FCControls.Combination;
using FCWPF.FCCommon.FCControls.Select;
using FCWPF.FCCommon.FCControls.Text;
using FCWPF.FCCommon.FCExtentions.PropertyGrid;
using FCWPF.FCCommon.FCFunction;
using JOINT_G.Extensions;
using JOINT_G.Helpers;
using JOINT_G.Models;
using JOINT_G.Models.Enums;
using JOINT_G.Models.Interfaces;
using JOINT_G.ViewModels.DetailsScreen;
using Serilog;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace JOINT_G.Views.DetailsScreen
{
    /// <summary>
    /// Interaction logic for DetailsScreen.xaml
    /// </summary>
    public partial class DetailsScreenJOINT_G_03 : Window
    {
        // FSP , TGP , GY , PL

        private List<string> _comboboxLoadedList = new List<string>();
        private bool _isFCInputDimensionLoaded = false;
        private bool _validate;
        private FlangeType _flangeType;

        public DetailsScreenJOINT_G_03(FlangeType flangeType)
        {
            Log.Logger.Information("DetailsScreenJOINT_G_03.xaml.cs DetailsScreenJOINT_G_03:DetailsScreenJOINT_G_03 , Message :  Open DetailsScreenJOINT_G_03 window");
            Application.Current.MainWindow = this;
            InitializeComponent();

            _flangeType = flangeType;

            switch (flangeType)
            {
                case FlangeType.SP:
                    ViewModel = new DetailsScreenFSPViewModel(new DetailsModelFSP(), DrawArea1);
                    break;
                case FlangeType.PL:
                    ViewModel = new DetailsScreenPLViewModel(new DetailsModelPL(), DrawArea1);
                    break;
                case FlangeType.TGP:
                    ViewModel = new DetailsScreenTGPViewModel(new DetailsModelTGP(), DrawArea1);
                    break;
                case FlangeType.GY:
                    ViewModel = new DetailsScreenGYViewModel(new DetailsModelGY(), DrawArea1);
                    break;
            }
            DataContext = ViewModel;


            grid.RowDefinitions.Clear();
            grid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(5, GridUnitType.Pixel) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(5, GridUnitType.Pixel) });
            grid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });

            Grid.SetRow(compomentInfor, 0);
            Grid.SetRow(gridPropertyGrid, 2);
            Grid.SetRow(dockPanelPropertyGrid, 4);



            var fcTextBoxHeight = 0.0;
            foreach (var i in ViewModel.MemberInfors)
            {
                FCLabel fCTextBox = new FCLabel();
                if (!String.IsNullOrEmpty(i))
                {
                    fCTextBox.Text = i;
                    fCTextBox.Padding = new Thickness(5, 4, 5, 5);
                    compomentInforStackPanel.Children.Add(fCTextBox);
                    fcTextBoxHeight += 25;
                }
            }

            compomentInforStackPanel.Height = fcTextBoxHeight;

            propGrid.AutoMove = true;

            checkBlock.DelegateOKProcs.Add(ProcessOk);

            if (_flangeType == FlangeType.TGP)
                checkBlock.TopElement = propGrid.GetEditor("CTValue1") as FrameworkElement;
            else checkBlock.TopElement = propGrid.GetEditor("Prop1") as FrameworkElement;


            checkBlock.Loaded += (sender, e) =>
            {
                (sender as FCCheckTextBox).TextBox.Padding = new Thickness(0, 4, 0, 0);
            };


        }

        public IDetailScreenViewModel ViewModel { get; set; }



        private void ResizePropGrid()
        {
            ViewModel.PropertyGridModel.ResizePropGrid(propGridWrapper.ActualHeight, propGrid);
        }
        private void ComboboxInitialized(object s, EventArgs e)
        {
            var combobox = (s as FCComboBoxExpansion);

            combobox.LostFocus += (s, e) =>
            {
                help.Content = "";
                help.Background = Brushes.White;
            };
            combobox.GotFocus += (s, e) =>
            {
                if (s is ComboBox combobox)
                {

                    string content = "";
                    if (combobox.ItemsSource is List<FCComboBoxClass> itemSrc && itemSrc.Count <= 10)
                    {
                        content = "[  ";
                        foreach (var item in itemSrc)
                        {
                            content = content + item.Both + "  ";
                        }
                        content = content + "]";
                    }
                    help.Content = content;
                    help.Background = Brushes.LightYellow;
                }
            };
            combobox.GotFocus += LoadGuidImage;
            combobox.Loaded += (s, e) =>
            {

                var combobox = s as FCComboBoxExpansion;
                if (_comboboxLoadedList.Contains(combobox.Name))
                    return;

                combobox.GetTextBox().Name = (s as FrameworkElement).Name;

                combobox.GetTextBox().PreviewLostKeyboardFocus += (s, e) =>
                {

                    if (s is TextBox textBox)
                    {
                        if (
                             !Keyboard.IsKeyDown(Key.Enter)
                             && !Keyboard.IsKeyDown(Key.F9)
                           )
                        {
                            _validate = ViewModel.ValidateProperty(textBox.Name);
                            DetailScreenHelper.RefreshFCPropertyGrid();
                        }

                        if (!_validate
                            && Keyboard.IsKeyDown(Key.Enter)
                         )
                        {
                            var result = ShowDialog();

                            if (!result)
                            {
                                e.Handled = true;
                                FCControlCommon.FocusSelect(textBox);
                            }
                        }
                    }

                };
                combobox.GetTextBox().PreviewKeyDown += (s, e) =>
                {

                    if (s is TextBox textBox)
                    {
                        if (e.Key == Key.Enter || e.Key == Key.F9)
                        {
                            _validate = ViewModel.ValidateProperty(textBox.Name);
                            DetailScreenHelper.RefreshFCPropertyGrid();
                        }
                    }
                };

                _comboboxLoadedList.Add(combobox.Name);
            };


            combobox.DropDownClosed += (s, e) =>
            {


                if (!Keyboard.IsKeyDown(Key.Enter))
                {
                    Window w = Window.GetWindow(this);

                    IInputElement element = FocusManager.GetFocusedElement(w);

                    if (
                        null != element
                        && true == combobox.IsEditable
                        && element is TextBox tb
                    )
                    {
                        _validate = ViewModel.ValidateProperty((s as FrameworkElement).Name);
                        DetailScreenHelper.RefreshFCPropertyGrid();
                        FCControlCommon.MoveFocus(tb, Key.Enter);
                    }
                    else
                    {
                        _validate = ViewModel.ValidateProperty((s as FrameworkElement).Name);
                        DetailScreenHelper.RefreshFCPropertyGrid();
                        FCControlCommon.MoveFocus((s as Control), Key.Enter);
                    }
                }
            };
        }

        private void TextBoxPreviewLostKeyboardFocus(object s, KeyboardFocusChangedEventArgs e)
        {

            if (s is TextBox textbox)
            {
                // Lose focus but not Press Key
                if (
                    !Keyboard.IsKeyDown(Key.Down)
                    && !Keyboard.IsKeyDown(Key.Enter)
                    && !Keyboard.IsKeyDown(Key.F9)
                    && !Keyboard.IsKeyDown(Key.Up))
                {
                    _validate = ViewModel.ValidateProperty(textbox.Name);
                    DetailScreenHelper.RefreshFCPropertyGrid();
                }

                if (!_validate && Keyboard.IsKeyDown(Key.Enter))
                {
                    var result = ShowDialog();

                    if (!result)
                    {
                        e.Handled = true;
                        FCControlCommon.FocusSelect(textbox);
                    }
                }
            }
        }
        private void TextboxInitialized(object s, EventArgs e)
        {

            if (s is TextBox textbox)
            {
                textbox.PreviewLostKeyboardFocus += TextBoxPreviewLostKeyboardFocus;
                textbox.KeyUp += TextBoxKeyUp;
                textbox.PreviewKeyDown += (s, e) =>
                  {
                      var textBox = (s as TextBox);
                      if (e.Key == Key.Enter || e.Key == Key.F9 || e.Key == Key.Down || e.Key == Key.Up)
                      {
                          _validate = ViewModel.ValidateProperty(textBox.Name);
                          DetailScreenHelper.RefreshFCPropertyGrid();
                      }
                  };
                textbox.GotFocus += LoadGuidImage;
            }
        }
        private void TextBoxKeyUp(object s, KeyEventArgs e)
        {
            if (e.Key == Key.Down || e.Key == Key.Up)
            {
                if (e.Key == Key.Down)
                {
                    propGrid.FCPropertyGridKeyDownCommonProc(Key.Enter);
                }
                else
                {
                    propGrid.FCPropertyGridKeyDownCommonProc(Key.F9);
                }
            }
        }



        private void InputDimensionInitialized(object s, EventArgs e)
        {

            var id = s as FCInputDimension;

            id.Loaded += (s, e) =>
            {
                if (_isFCInputDimensionLoaded)
                {
                    return;
                }
                _isFCInputDimensionLoaded = true;


                var listTextBox = FindChildren.FindVisualChildrens<FCTextBox>(id);

                int i = 1;
                foreach (var item in listTextBox)
                {
                    item.Name = $"Text{i}Dimension";
                    item.SetBinding(FCTextBox.TextProperty, new Binding(item.Name)
                    {
                        Source = ViewModel,
                        Converter = new DoubleConverter(),
                        ConverterParameter = "%6.1f",
                        Mode = BindingMode.TwoWay
                    });
                    item.GotFocus += LoadGuidImage;
                    item.LostFocus += (sender, e) =>
                    {
                        (ViewModel as DetailsScreenTGPViewModel).ValidateCompomentCt(item.Name, propGrid);
                        DetailScreenHelper.RefreshFCPropertyGrid();
                    };
                    item.PreviewKeyDown += (sender, e) =>
                    {
                        var viewModel = ViewModel as DetailsScreenTGPViewModel;
                        if (e.Key == Key.Enter && !(ViewModel as DetailsScreenTGPViewModel).ValidateCompomentCt(item.Name, propGrid))
                        {
                            var result = ShowDialog();
                            if (!result)
                            {
                                var inputDimension = propGrid.GetEditor("CTValue2") as FCInputDimension;
                                FCControlCommon.FocusSelect(inputDimension.Txt1);
                            }
                            DetailScreenHelper.RefreshFCPropertyGrid();

                        }

                    };
                    i++;
                }
            };
        }

        private void PropGridLoaded(object sender, RoutedEventArgs e)
        {
            propGrid.SelectedObject = ViewModel.PropertyGridModel;
            DetailScreenHelper.LoadEventAndSetBinding(propGrid,
                                                      ViewModel,
                                                      new EventHandler(ComboboxInitialized),
                                                      new EventHandler(TextboxInitialized),
                                                      new EventHandler(InputDimensionInitialized)
                                                      );
        }
        private void PropBoxesLoaded(object sender, EventArgs e)
        {
            ViewModel.PropertyGridModel.Init(propGrid);
            DetailScreenHelper.SetBindingEnabled();
            FCPropertyGrid.InitFormatData(propGrid);
            ResizePropGrid();

            // Init Focus
            if (_flangeType != FlangeType.TGP)
                (propGrid.GetEditor("Prop1") as FrameworkElement).Focus();
            else (propGrid.GetEditor("CTValue1") as FrameworkElement).Focus();

            propGrid.NextControl = checkBlock;

            if (_flangeType == FlangeType.TGP)
            {
                propGrid.PrevControl = (propGrid.GetEditor("CTValue1") as Control);
            }
            else
            {
                propGrid.PrevControl = propGrid.GetEditor("Prop1") as Control;
            }

        }

        private void FCButtonOk_Click(object sender, RoutedEventArgs e)
        {
            ProcessOk();
        }
        private void FCButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }

        private void ProcessOk()
        {
            //this.IsEnabled = false;
            bool isValidate;
            if (_flangeType == FlangeType.TGP)
            {
                isValidate = (ViewModel as DetailsScreenTGPViewModel).ValidateCompomentCt("ALL", propGrid);
                if (isValidate)
                {
                    isValidate = ViewModel.ValidateProperty("ALL");
                }
                else
                {
                    for (var j = 0; j < ViewModel.Errors.Count; j++)
                    {
                        var option = ShowDialog(j);
                        if (!option)
                        {
                            var inputDimension = propGrid.GetEditor("CTValue2") as FCInputDimension;
                            FCControlCommon.FocusSelect(inputDimension.Txt1);
                            return;
                        }
                    }
                    DialogResult = true;
                    this.Close();
                }
            }
            else
                isValidate = ViewModel.ValidateProperty("ALL");


            if (isValidate)
            {
                DialogResult = true;
                this.Close();
            }
            else
            {
                for (var j = 0; j < ViewModel.Errors.Count; j++)
                {
                    var option = ShowDialog(j);

                    if (!option)
                    {
                        (propGrid.GetEditor(ViewModel.Errors[j].PropName) as FrameworkElement).Focus();
                        return;
                    }
                }
                DialogResult = true;
                this.Close();
            }
        }

        private void OnKeyPressed(object sender, KeyEventArgs e)
        {

            switch (e.Key)
            {
                case Key.F2:
                    buttonOk.Focus();
                    ProcessOk();
                    break;

                case Key.F1:
                    buttoncancel.Focus();
                    DialogResult = false;
                    this.Close();
                    break;
            }
        }


        #region WdPanel
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            ViewModel.UpdateDrawWin();
            ViewModel.ClearSelectBolt();
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ViewModel.SelectBolt();
            var str = "";

            BindingExpression bindingExpression = BindingOperations.GetBindingExpression(LineInfo, TextBlock.TextProperty);
            if (bindingExpression != null)
            {
                PropertyInfo? property = bindingExpression.DataItem.GetType().GetProperty(bindingExpression.ParentBinding.Path.Path);
                if (property != null)
                    property.SetValue(bindingExpression.DataItem, str, null);
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ViewModel.ClearSelectBolt();
        }
        #endregion

        private bool ShowDialogWarning(ErrorDetails errorDetails)
        {
            try
            {
                bool option = false;
                Application.Current.Dispatcher.Invoke(() =>
                {
                    option = this.ShowWarningConfirmation(errorDetails.ErrorMessage, "警告");
                });
                return option;
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return false;
            }
        }
        private void ShowDialogError(ErrorDetails errorDetails)
        {
            try
            {
                Application.Current.Dispatcher.Invoke(() => this.ShowWarning(errorDetails.ErrorMessage, "エラー"));
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        public bool ShowDialog(int index = 0)
        {
            //this.IsEnabled = false;
            var errroDetail = ViewModel.Errors.Count == 0 ? null : ViewModel.Errors[index];

            if (errroDetail == null)
            {
                //this.IsEnabled = true;
                return false;
            }
            else if (errroDetail.ErrorStatus == -1)
            {
                ShowDialogError(errroDetail);
                //this.IsEnabled = true;
                return false;
            }
            else
            {
                var option = ShowDialogWarning(errroDetail);
                if (!option)
                {
                    //this.IsEnabled = true;
                    return false;
                }
                else
                {
                    //this.IsEnabled = true;
                    return true;
                }
            }
        }

        public void UpdateDrawWin()
        {
            ViewModel.UpdateDrawWin();
        }
        private void LoadGuidImage(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement element)
                {
                    String imageSource = System.AppDomain.CurrentDomain.BaseDirectory + @"svg\file_name";

                    int.TryParse(element.Name.Replace("Prop", ""), out int index);
                    switch (_flangeType)
                    {
                        case FlangeType.SP:
                            if (index >= 1 && index <= 7)
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR22_FGAGE.svg");
                            }
                            else if (index >= 10)
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR22_FPIT.svg");
                            }
                            break;
                        case FlangeType.PL:
                            imageSource = imageSource.Replace("file_name", "GD_JR22_PLYO.svg");
                            break;
                        case FlangeType.TGP:
                            if (index == 0)
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR22_CTBZ2.svg");
                            }
                            if (index == 19)
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR22_TGP_KJTP.svg");
                            }
                            else
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR22_TGP.svg");
                            }
                            break;
                        case FlangeType.GY:
                            if (index <= 16 && index >= 2)
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR22_GENYO.svg");
                            }
                            else if (index > 16)
                            {
                                imageSource = imageSource.Replace("file_name", "GD_JR22_GENYO2.svg");
                            }
                            break;
                    }

                    svgImgBottom.Load(new Uri(imageSource));
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GridLength labelColumnWidth = new GridLength(1, GridUnitType.Star);
            GridLength textboxColumnWidth = new GridLength(28, GridUnitType.Pixel);
            checkBlock.CdLbl.Width = labelColumnWidth;
            checkBlock.CdTxt.Width = textboxColumnWidth;
        }
    }
}

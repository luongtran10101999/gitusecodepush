﻿using FCWPF.FCCommon.FCControls.Combination;
using FCWPF.FCCommon.FCControls.Select;
using FCWPF.FCCommon.FCControls.Text;
using JOINT_G.Extensions;
using JOINT_G.Helpers;
using JOINT_G.Models;
using JOINT_G.ViewModels.MainScreen;
using JOINT_G.Views.Dialogs;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;
using System.Windows.Threading;
using Binding = System.Windows.Data.Binding;
using System.Text;
using JOINT_G.Repository;
using System.Diagnostics;

namespace JOINT_G.Views.MainScreen
{
    /// <summary>
    /// Interaction logic for MainScreenWindow.xaml
    /// </summary>
    public partial class MainScreenWindow : Window
    {
        //private bool _isRowSelected;

        private MainScreenWindowViewModel ViewModel { get; }
        List<FCInputDimension> dms;
        List<FCComboBox> mrkTyps;
        List<FCComboBoxExpansion> grades;
        List<FCComboBox> flgStds;
        List<FCComboBox> webStds;
        List<FCComboBox> matchTypes;
        private List<FCTextBox> _tbxDms1;
        private List<FCTextBox> _tbxDms2;
        private List<FCTextBox> _tbxDms3;
        private List<FCTextBox> _tbxDms4;
        private List<FCTextBox> _tbxTdep;
        private List<FCTextBox> _tbxWidth;
        private List<FCTextBox> _tbxHeight;
        private double _screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
        private double _screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
        //private List<string> lstBtn = new List<string>() { "cancelBtn", "okBtn", "copy_btn", "delete_btn", "edit_mark_btn", "setup_official_mark_btn", "master_call_btn", "part_angle_btn", "mem_delete_btn", "mem_add_btn" };
        public MainScreenWindow()
        {
            //_isRowSelected = false;
            Log.Logger.Information("MainScreenWindow.xaml.cs MainScreenWindow:MainScreenWindow , Message :  Open main window");
            InitializeComponent();
            ViewModel = new MainScreenWindowViewModel();
            DataContext = ViewModel;
            checkBlock.DelegateCancelProcs.Add(CheckCanceled);
            checkBlock.DelegateOKProcs.Add(CheckPassed);
            if (_screenWidth <= 1300)
            {
                this.Width = _screenWidth;
                this.Height = _screenHeight - 40;
            }


        }
        private void LoadedWindowCommand(object sender, EventArgs e)
        {
            try
            {
                ViewModel.LoadedWindowCommandExecuted(sender, e);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void MainScreenWin_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {


                joint_list.UnselectAll();
                jointMark.SelectedIndex = -1;
                WidthHeader.Width = 0;
                HeightHeader.Width = 0;
                GridLength labelColumnWidth = new GridLength(1, GridUnitType.Star);
                GridLength textboxColumnWidth = new GridLength(28, GridUnitType.Pixel);
                checkBlock.CdLbl.Width = labelColumnWidth;
                checkBlock.CdTxt.Width = textboxColumnWidth;

                dms = FindChildren.FindVisualChildrens<FCInputDimension>(elementsDock).ToList();
                ViewModel.Dms = dms;
                mrkTyps = FindChildren.FindVisualChildrens<FCComboBox>(gridMrkTyp).ToList();
                for (int i = 0; i < dms.Count; i++)
                {
                    dms[i].Txt1.SetBinding(TextBox.TextProperty, new Binding("Dimension1") { Source = ViewModel.ElementViewModels.ElementAt(i) });
                    dms[i].Txt2.SetBinding(TextBox.TextProperty, new Binding("Dimension2") { Source = ViewModel.ElementViewModels.ElementAt(i) });
                    dms[i].Txt3.SetBinding(TextBox.TextProperty, new Binding("Dimension3") { Source = ViewModel.ElementViewModels.ElementAt(i) });
                    dms[i].Txt4.SetBinding(TextBox.TextProperty, new Binding("Dimension4") { Source = ViewModel.ElementViewModels.ElementAt(i) });
                    dms[i].Txt1.SetBinding(TextBox.TagProperty, new Binding("Id") { Source = ViewModel.ElementViewModels.ElementAt(i) });
                    dms[i].Txt2.SetBinding(TextBox.TagProperty, new Binding("Id") { Source = ViewModel.ElementViewModels.ElementAt(i) });
                    dms[i].Txt3.SetBinding(TextBox.TagProperty, new Binding("Id") { Source = ViewModel.ElementViewModels.ElementAt(i) });
                    dms[i].Txt4.SetBinding(TextBox.TagProperty, new Binding("Id") { Source = ViewModel.ElementViewModels.ElementAt(i) });
                    var startIndex = ViewModel.ElementViewModels[i].ElementNameIndex;
                    dms[i].Txt1.TabIndex = ++startIndex;
                    dms[i].Txt2.TabIndex = ++startIndex;
                    dms[i].Txt3.TabIndex = ++startIndex;
                    dms[i].Txt4.TabIndex = ++startIndex;
                }
                _tbxDms1 = FindChildren.FindVisualChildrens<FCTextBox>(elementsDock).Where(x => x.Name == "txt1").ToList();
                _tbxDms2 = FindChildren.FindVisualChildrens<FCTextBox>(elementsDock).Where(x => x.Name == "txt2").ToList();
                _tbxDms3 = FindChildren.FindVisualChildrens<FCTextBox>(elementsDock).Where(x => x.Name == "txt3").ToList();
                _tbxDms4 = FindChildren.FindVisualChildrens<FCTextBox>(elementsDock).Where(x => x.Name == "txt4").ToList();

                _tbxTdep = FindChildren.FindVisualChildrens<FCTextBox>(elementsDock).Where(x => x.Name == "tdep").ToList();
                _tbxWidth = FindChildren.FindVisualChildrens<FCTextBox>(elementsDock).Where(x => x.Name == "width").ToList();
                _tbxHeight = FindChildren.FindVisualChildrens<FCTextBox>(elementsDock).Where(x => x.Name == "height").ToList();
                ViewModel.TbxtDept = _tbxTdep;
                ViewModel.TbxWidth = _tbxWidth;
                ViewModel.TbxHeight = _tbxHeight;

                List<FCComboBox> dmbs = FindChildren.FindVisualChildrens<FCComboBox>(elementsDock).ToList();
                var dmbs2 = FindChildren.FindVisualChildrens<FCComboBoxExpansion>(elementsDock).ToList();
                grades = dmbs2.Where(x => x.Name == "gradeCode").ToList();

                foreach (var item in grades)
                {
                    item.KeyValuePairs = new Dictionary<string, string>();
                    item.KeyValuePairs2 = new Dictionary<string, string>();
                    item.KeyValuePairs3 = new Dictionary<string, object>();
                    // Got Focus then Handel Load Guid Svg
                    item.GotFocus += MrkTypGotFocus;
                }



                flgStds = dmbs.Where(x => x.Name == "flgStd").ToList();
                webStds = dmbs.Where(x => x.Name == "webStd").ToList();
                matchTypes = dmbs.Where(x => x.Name == "matchType").ToList();
                for (int i = 0; i < 3; i++)
                {
                    _tbxDms1[i].Loaded += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        TxtMemberFormatString(textbox);
                    };
                    _tbxDms2[i].Loaded += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        TxtMemberFormatString(textbox);
                    };
                    _tbxDms3[i].Loaded += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        TxtMemberFormatString(textbox);
                    };
                    _tbxDms4[i].Loaded += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        TxtMemberFormatString(textbox);
                    };
                }
                foreach (var tbx in _tbxDms1)
                {
                    // Got Focus then Handel Load Guid Svg
                    tbx.Name = "dimension";
                    tbx.GotFocus += MrkTypGotFocus;

                    tbx.GotFocus += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        textbox.SelectAll();
                    };
                    tbx.LostFocus += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        TxtMemberFormatString(textbox);

                        var tag = int.Parse(textbox.Tag.ToString()!);
                        if (_tbxDms3[tag - 1].IsEnabled)
                        {
                            double.TryParse(textbox.Text, out var input);
                            ViewModel.UpdateTempElement();
                            HandleSetSuggestion(tag - 1, grades[tag - 1]);
                        }
                    };
                    tbx.PreviewKeyDown += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        double input;
                        double.TryParse(textbox.Text, out input);
                        var tag = int.Parse(textbox.Tag.ToString()!);
                        ViewModel.UpdateTempElement();
                        if (e.Key == Key.Enter)
                        {
                            if (input == null || input <= 0)
                            {
                                e.Handled = true;
                                Application.Current.Dispatcher.Invoke(() => this.ShowWarning("入力値が不正です。", "入力エラー"));
                                textbox.SelectAll();
                            }
                        }
                    };
                }
                foreach (var tbx in _tbxDms2)
                {
                    tbx.GotFocus += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        textbox.SelectAll();
                        var tag = int.Parse(textbox.Tag.ToString()!);
                        if (_tbxDms3[tag - 1].IsEnabled)
                        {
                            HandleSetSuggestion(tag - 1, grades[tag - 1]);
                        }
                    };
                    tbx.LostFocus += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        TxtMemberFormatString(textbox);
                    };
                    tbx.PreviewKeyDown += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        double input;
                        double.TryParse(textbox.Text, out input);
                        var tag = int.Parse(textbox.Tag.ToString()!);
                        ViewModel.UpdateTempElement();
                        if (e.Key == Key.Enter)
                        {
                            if (input <= 0)
                            {
                                e.Handled = true;
                                Application.Current.Dispatcher.Invoke(() => this.ShowWarning("入力値が不正です。", "入力エラー"));
                                textbox.SelectAll();
                            }
                            else
                            {
                                if (!_tbxDms3[tag - 1].IsEnabled || _tbxDms3[tag - 1].Visibility != Visibility.Visible)
                                {
                                    var validate = ViewModel.ValidateJointInput();
                                    if (validate.Item1[tag - 1] != null && validate.Item1[tag - 1].Length > 0)
                                    {
                                        bool result = showMessageValidate(validate.Item1[tag - 1], validate.Item2[tag - 1]);
                                        if (!result)
                                        {
                                            _tbxDms1[tag - 1].Focus();
                                            _tbxDms1[tag - 1].SelectAll();
                                            e.Handled = true;
                                        }
                                    }
                                }
                                //else if (ViewModel.DimensionSuggest != null)
                                //{
                                //    flgStds[tag - 1].Focus();
                                //    flgStds[tag - 1].GetTextBox().SelectAll();
                                //    e.Handled = true;
                                //}
                            }
                        }
                    };
                }
                foreach (var tbx in _tbxDms3)
                {
                    tbx.GotFocus += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        textbox.SelectAll();
                    };
                    tbx.LostFocus += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        TxtMemberFormatString(textbox);
                    };
                    tbx.PreviewKeyDown += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        double input;
                        double.TryParse(textbox.Text, out input);
                        var tag = int.Parse(textbox.Tag.ToString()!);
                        ViewModel.UpdateTempElement();
                        if (e.Key == Key.Enter)
                        {
                            if (input <= 0)
                            {
                                Application.Current.Dispatcher.Invoke(() => this.ShowWarning("入力値が不正です。", "入力エラー"));
                                e.Handled = true;
                                textbox.SelectAll();
                            }
                            else
                            {
                                if (!_tbxDms4[tag - 1].IsEnabled || _tbxDms4[tag - 1].Visibility != Visibility.Visible)
                                {
                                    var validate = ViewModel.ValidateJointInput();
                                    if (validate.Item1[tag - 1] != null && validate.Item1[tag - 1].Length > 0)
                                    {
                                        bool result = showMessageValidate(validate.Item1[tag - 1], validate.Item2[tag - 1]);
                                        if (!result)
                                        {
                                            _tbxDms1[tag - 1].Focus();
                                            _tbxDms1[tag - 1].SelectAll();
                                            e.Handled = true;
                                        }
                                    }
                                }
                            }
                        }
                    };
                }
                foreach (var tbx in _tbxDms4)
                {
                    tbx.Loaded += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        TxtMemberFormatString(textbox);
                    };
                    tbx.GotFocus += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        textbox.SelectAll();
                    };
                    tbx.LostFocus += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        TxtMemberFormatString(textbox);
                    };
                    tbx.PreviewKeyDown += (sender, e) =>
                    {
                        var textbox = sender as FCTextBox;
                        double input;
                        double.TryParse(textbox.Text, out input);
                        var tag = int.Parse(textbox.Tag.ToString()!);
                        ViewModel.UpdateTempElement();
                        if (e.Key == Key.Enter)
                        {
                            if (input <= 0)
                            {
                                Application.Current.Dispatcher.Invoke(() => this.ShowWarning("入力値が不正です。", "入力エラー"));
                                e.Handled = true;
                                textbox.SelectAll();
                            }
                            else
                            {
                                var validate = ViewModel.ValidateJointInput();
                                if (validate.Item1[tag - 1] != null && validate.Item1[tag - 1].Length > 0)
                                {
                                    bool result = showMessageValidate(validate.Item1[tag - 1], validate.Item2[tag - 1]);
                                    if (!result)
                                    {
                                        _tbxDms1[tag - 1].Focus();
                                        _tbxDms1[tag - 1].SelectAll();
                                        e.Handled = true;
                                    }
                                }
                            }
                        }
                    };
                }
                TextBox tbxJmk = jointMark.GetTextBox();
                tbxJmk.Name = "jmk";
                tbxJmk.MaxLength = 6;
                ViewModel.TbxJointMark = tbxJmk;
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void TxtMemberFormatString(FCTextBox textbox)
        {
            try
            {
                if (textbox != null)
                {
                    textbox.Text = FCTextBoxCommon.Convert(textbox.Text, "%6.1f");
                }
            }
            catch (Exception ex)
            {
                Log.Error($"{textbox.Name} Error Convert : {textbox.Name} {textbox.FormatString} {textbox.Text}");
            }
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.Key)
                {
                    case Key.F1:
                        okBtn.Focus();
                        CloseBtnCommand();
                        break;
                    case Key.F2:
                        okBtn.Focus();
                        OkBtnCommand();
                        break;
                    case Key.F3:
                        if (ViewModel.CopyButtonEnabled)
                            CopyCommand();
                        break;
                    case Key.F4:
                        if (ViewModel.DeleteButtonEnabled)
                            DeleteCommand();
                        break;
                    case Key.F5:
                        if (ViewModel.EditMarkButtonEnabled)
                            ChangeCommand();
                        break;
                    case Key.F6:
                        OfficialMarkSetupCommand();
                        break;
                    case Key.F7:
                        BeamJointMasterCallCommand();
                        break;
                    case Key.F8:
                        if (dataTabControl.SelectedIndex == 1)
                        {
                            DisplayMethodCommand();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        private void OkBtnCommand(object sender = null, RoutedEventArgs e = null)
        {
            try
            {
                bool canGoNext = true;
                var tbxJmk = jointMark.GetTextBox();
                //Validate JointMark Field
                TextBox invalidControl = null;
                if (tbxJmk.Text.Length == 0)
                {
                    JointMarkClear();
                    canGoNext = false;
                    invalidControl = jointMark.GetTextBox();
                    goto End;
                }
                //Validate Other MarkType Fields
                var otherMrkTyps = mrkTyps.Where(x => x.Name != "jointMark");
                foreach (var cmb in otherMrkTyps)
                {
                    if (!ValidateInput(cmb))
                    {
                        canGoNext = false;
                        invalidControl = cmb.GetTextBox();
                        goto End;
                    }
                }
                //Validate Member Fields
                //Validate Grade
                if (grades[0].SelectedIndex < 0
                    || int.Parse((grades[0].SelectedItem as FCComboBoxClass).Key) == 0
                    || !ValidateInputGrade(grades[0]))
                {
                    canGoNext = false;
                    invalidControl = grades[0].GetTextBox();
                    goto End;
                }
                for (int i = 1; i < ViewModel.showedMember; i++)
                {
                    if (!ValidateInputGrade(grades[i]))
                    {
                        invalidControl = grades[i].GetTextBox();
                        canGoNext = false;
                        goto End;
                    }
                }
                for (int i = 0; i < ViewModel.showedMember; i++)
                {
                    string numOfDimension;
                    grades[i].KeyValuePairs2.TryGetValue(grades[i].SelectedValue.ToString(), out numOfDimension);
                    int numOfShowedDms = int.Parse(numOfDimension);
                    if (numOfShowedDms >= 1 && double.Parse(_tbxDms1[i].Text) <= 0)
                    {
                        invalidControl = _tbxDms1[i];
                        canGoNext = false;
                        goto End;
                    }
                    else if (numOfShowedDms >= 2 && double.Parse(_tbxDms2[i].Text) <= 0)
                    {
                        invalidControl = _tbxDms2[i];
                        canGoNext = false;
                        goto End;
                    }
                    else if (numOfShowedDms >= 3 && double.Parse(_tbxDms3[i].Text) <= 0)
                    {
                        invalidControl = _tbxDms3[i];
                        canGoNext = false;
                        goto End;
                    }
                    else if (numOfShowedDms == 4 && double.Parse(_tbxDms4[i].Text) <= 0)
                    {
                        invalidControl = _tbxDms4[i];
                        canGoNext = false;
                        goto End;
                    }
                    if (!ValidateInputGrade(grades[i]))
                    {
                        invalidControl = grades[i].GetTextBox();
                        canGoNext = false;
                        goto End;
                    }
                    if (!ValidateInput(flgStds[i]))
                    {
                        invalidControl = flgStds[i].GetTextBox();
                        canGoNext = false;
                        goto End;
                    }
                    if (!ValidateInput(webStds[i]))
                    {
                        invalidControl = webStds[i].GetTextBox();
                        canGoNext = false;
                        goto End;
                    }
                    else if (ViewModel.IsEndDepthVisiable() && _tbxTdep[i].IsEnabled && double.Parse(_tbxTdep[i].Text) <= 0)
                    {
                        invalidControl = _tbxTdep[i];
                        canGoNext = false;
                        goto End;
                    }
                    else if (_tbxWidth[i].IsEnabled && double.Parse(_tbxWidth[i].Text) <= 0)
                    {
                        invalidControl = _tbxWidth[i];
                        canGoNext = false;
                        goto End;
                    }
                    else if (_tbxHeight[i].IsEnabled && double.Parse(_tbxHeight[i].Text) <= 0)
                    {
                        invalidControl = _tbxHeight[i];
                        canGoNext = false;
                        goto End;
                    }
                }
                var validate = ViewModel.ValidateJointInput();
                for (int i = 0; i < 30; i++)
                {
                    if (validate.Item1[i] != null && validate.Item1[i].Length > 0)
                    {
                        bool result = showMessageValidate(validate.Item1[i], validate.Item2[i]);
                        if (!result)
                        {
                            canGoNext = false;
                            grades.ElementAt(i).Focus();
                            return;
                        }
                        else
                        {
                            canGoNext = true;
                        }
                    }
                }
            End:
                if (canGoNext)
                {
                    if (ViewModel.OkBtnCommandExecuted())
                    {
                        JointMarkClear(true);
                        this.UpdateLayout();
                        jointMark.GetTextBox().Focus();
                    }
                }
                else
                {
                    Application.Current.Dispatcher.Invoke(() => this.ShowWarning("入力値が不正です。", "入力エラー"));
                    invalidControl.Focus();
                    invalidControl.SelectAll();
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        private bool showMessageValidate(string msg, int status)
        {
            bool result = false;
            if (status == -1)
            {
                Application.Current.Dispatcher.Invoke(() => this.ShowWarning(msg, "エラー"));
                result = false;
            }
            else if (status < 0)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    result = this.ShowWarningConfirmation(msg, "入力エラー");
                });
            }
            return result;
        }
        private void CloseBtnCommand(object sender = null, RoutedEventArgs e = null)
        {
            var result = false;
            Application.Current.Dispatcher.Invoke(() =>
            {
                result = this.ShowWarningConfirmation("梁JOINT取り付けが未設定の場合は、次にJOINT取り付けを行ってください。", "処理確認");

            });
            if (result)
                ViewModel.CloseBtnCommandExecuted();
        }

        private void CheckCanceled()
        {
            ViewModel.CheckChanged(false);
            ftyp.Focus();
        }

        private void CheckPassed()
        {
            ViewModel.CheckChanged(true);
            OkBtnCommand();
        }
        private void TabSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dataTabControl != null && dataTabControl.SelectedIndex == 0)
            {
                buttonsGridJoint.Visibility = Visibility.Visible;
                buttonsGridMember.Visibility = Visibility.Hidden;
            }
            else
            {
                buttonsGridJoint.Visibility = Visibility.Hidden;
                buttonsGridMember.Visibility = Visibility.Visible;
            }
        }

        #region JointList functions
        private void JointRowSelected(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var itemsCount = (sender as DataGrid).SelectedItems?.Count;
                ViewModel.HandleJointRowSelected(itemsCount ?? 0);
                String imageSource = System.AppDomain.CurrentDomain.BaseDirectory + @"svg\GD_JR22_STTYPE.svg";
                svgImgBottom.Load(new Uri(imageSource));
                if (itemsCount > 0)
                {
                    RemoveEvent();
                    int index = (sender as DataGrid).SelectedIndex;
                    ViewModel.JointMarkIndex = index;
                    ViewModel.JointMarks.TryGetValue(index, out string changeJmk);
                    JointMarkChange(changeJmk);
                    jointMark.GetTextBox().Focus();
                    jointMark.GetTextBox().SelectAll();
                    AddEvent();
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void FormatDimension(int index)
        {
            _tbxDms1[index].Text = FCTextBoxCommon.Convert(_tbxDms1[index].Text, "%6.1f");
            _tbxDms2[index].Text = FCTextBoxCommon.Convert(_tbxDms2[index].Text, "%6.1f");
            _tbxDms3[index].Text = FCTextBoxCommon.Convert(_tbxDms3[index].Text, "%6.1f");
            _tbxDms4[index].Text = FCTextBoxCommon.Convert(_tbxDms4[index].Text, "%6.1f");
            _tbxWidth[index].Text = FCTextBoxCommon.Convert(_tbxWidth[index].Text, "%6.1f");
            _tbxHeight[index].Text = FCTextBoxCommon.Convert(_tbxHeight[index].Text, "%6.1f");
            if (ViewModel.IsEndDepthVisiable())
                _tbxTdep[index].Text = FCTextBoxCommon.Convert(_tbxTdep[index].Text, "%6.1f");

            var idx = grades[index].SelectedValue != null ? (int)grades[index].SelectedValue : -1;
            if (idx == 404)
            {
                _tbxWidth[index].Width = 70;
                _tbxHeight[index].Width = 70;
            }
            else if (idx == 310 || idx == 311)
            {
                _tbxWidth[index].Width = 70;
                _tbxHeight[index].Width = 0;
            }
            else
            {
                _tbxWidth[index].Width = 0;
                _tbxHeight[index].Width = 0;
            }
        }

        private Dictionary<string, List<string>> GetSunpou(int sunpousuu, string zaishu, List<string> l)
        {
            Dictionary<string, List<string>> rtn = new Dictionary<string, List<string>>();

            int cnt = l.Count;
            string delimiter = "x";

            for (int i = 0; i < cnt; i++)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(zaishu);
                sb.Append("-");

                string[] arSunpou = l[i].Split(delimiter);

                for (int j = 0; j < arSunpou.Length; j++)
                {
                    if (0 != j)
                    {
                        sb.Append(delimiter);
                    }

                    sb.Append(arSunpou[j]);
                }

                if (0 < sunpousuu)
                {
                    if (!rtn.ContainsKey(sb.ToString()))
                    {
                        rtn.Add(sb.ToString(), arSunpou.ToList<string>());
                    }
                }
            }

            return rtn;
        }

        private void CopyCommand(object sender = null, RoutedEventArgs e = null)
        {
            FocusManager.SetFocusedElement(this, null);
            Keyboard.ClearFocus();
            try
            {
                if (joint_list.SelectedIndex >= 0)
                {
                    string jmk = joint_list.SelectedItems?.Cast<FittingModel>().FirstOrDefault().Mark;
                    bool isSuccess = ViewModel.CopyCommandExecuted(jmk);
                    if (isSuccess)
                    {
                        var request = new TraversalRequest(FocusNavigationDirection.Next);
                        request.Wrapped = true;
                        jointMark.MoveFocus(request);
                    }
                }
                else
                {
                    jointMark.GetTextBox().Focus();
                    jointMark.GetTextBox().SelectAll();
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void DeleteCommand(object sender = null, RoutedEventArgs e = null)
        {
            try
            {
                if (joint_list.SelectedIndex >= 0)
                {

                    var result = this.ShowWarningDeleteConfirmation("選択した継手データを削除しますか？", "削除");
                    if (result)
                    {
                        string jmk = joint_list.SelectedItems?.Cast<FittingModel>().FirstOrDefault().Mark;
                        bool isSuccess = ViewModel.DeleteCommandExecuted(jmk);
                        if (isSuccess)
                        {
                            JointMarkClear();
                            ViewModel.UpdateJointListData();
                            ViewModel.UpdateMemberListData();
                            jointMark.GetTextBox().Focus();
                        }
                    }
                }
                else
                {
                    jointMark.GetTextBox().Focus();
                    jointMark.GetTextBox().SelectAll();
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        private void ChangeCommand(object sender = null, RoutedEventArgs e = null)
        {
            FocusManager.SetFocusedElement(this, null);
            Keyboard.ClearFocus();
            try
            {
                if (joint_list.SelectedIndex >= 0)
                {
                    string jmk = joint_list.SelectedItems?.Cast<FittingModel>().FirstOrDefault().Mark;
                    bool isSuccess = ViewModel.ChangeCommandExecuted(jmk);
                    if (isSuccess)
                    {
                        var request = new TraversalRequest(FocusNavigationDirection.Next);
                        request.Wrapped = true;
                        jointMark.MoveFocus(request);
                    }
                }
                else
                {
                    jointMark.GetTextBox().Focus();
                    jointMark.GetTextBox().SelectAll();
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        private void OfficialMarkSetupCommand(object sender = null, RoutedEventArgs e = null)
        {
            FocusManager.SetFocusedElement(this, null);
            Keyboard.ClearFocus();
            ViewModel.OfficialMarkSetupCommandExecuted();
        }
        private void BeamJointMasterCallCommand(object sender = null, RoutedEventArgs e = null)
        {
            FocusManager.SetFocusedElement(this, null);
            Keyboard.ClearFocus();
            try
            {
                var dialog = new MasterCallDialog(ViewModel.JointMarks.Select(x => x.Value).ToList());
                Application.Current.MainWindow = dialog;
                var res = dialog.ShowDialog();
                Application.Current.MainWindow = this;
                string newMark = dialog.ViewModel.NewMarkName;
                if (res is true)
                {
                    SelectMaterCallMark();
                    ViewModel.BeamJointMasterCallCommandExecuted(newMark);
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        #endregion

        #region MemberList functions
        private void DisplayMethodCommand(object sender = null, RoutedEventArgs e = null)
        {
            FocusManager.SetFocusedElement(this, null);
            Keyboard.ClearFocus();
            ViewModel.DisplayMethodCommandExecuted();
        }
        private void ComboBoxPreviewKeyDown(object sender, KeyEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                //do action
            }));
        }
        private void ElementsRowSelected(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var itemsCount = member_list.SelectedItems?.Count;
                if (itemsCount > 0)
                {
                    string materialMark = "";
                    var tbxJmk = jointMark.GetTextBox();
                    var memberModel = member_list.SelectedItems?.Cast<MaterialModel>().FirstOrDefault();
                    if (memberModel is not null)
                    {
                        materialMark = memberModel.JointMark.Length <= 0 ? tbxJmk.Text : memberModel.JointMark;
                        if (materialMark.Length <= 0)
                        {
                            FocusManager.SetFocusedElement(this, null);
                            Keyboard.ClearFocus();
                            var dialog = new CopyDialog(materialMark, ViewModel.JointMarks.Select(x => x.Value).ToList(), "edit");
                            Application.Current.MainWindow = dialog;
                            var result = dialog.ShowDialog();
                            Application.Current.MainWindow = this;
                            if (result == true)
                            {
                                materialMark = dialog.ViewModel.NewMarkName;
                            }
                            else
                            {
                                member_list.UnselectAll();
                                materialMark = "";
                            }
                        }
                        tbxJmk.Text = materialMark;
                        tbxJmk.Focus();
                        tbxJmk.SelectAll();
                        if (materialMark.Length > 0)
                        {
                            RemoveEvent();
                            SelectMaterialMark(member_list.SelectedIndex + 1, materialMark);
                            var request = new TraversalRequest(FocusNavigationDirection.Next);
                            request.Wrapped = true;
                            tbxJmk.MoveFocus(request);
                            AddEvent();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        private void MarkGridLostFocus(object sender, RoutedEventArgs e)
        {
            notificationLabel.Content = string.Empty;
        }

        private void MarkGridGotFocus(object sender, RoutedEventArgs e)
        {
            CastInfomationToLabel(e.GetComboBox());
        }
        #endregion

        #region MarkTypeInput functions
        private void PartAngleCommand(object sender, RoutedEventArgs e)
        {
            DependencyObject scope = FocusManager.GetFocusScope(e.OriginalSource as DependencyObject);
            FocusManager.SetFocusedElement(scope, this);
            ViewModel.PartAngleCommandExecuted();
        }
        private void ComboboxJointMarkSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //if (_isRowSelected)
                //{
                //    return;
                //}

                var index = (e.OriginalSource as FCComboBox).SelectedIndex;
                if (index >= 0)
                {
                    joint_list.ScrollIntoView(joint_list.Items[index]);
                    joint_list.SelectedIndex = (sender as ComboBox).SelectedIndex;
                    (e.OriginalSource as FCComboBox).GetTextBox().SelectAll();
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void JointMarkClear(bool isSaveJoint = false)
        {
            // only clear when a joint is selected
            if (DataRepository.GetMrkTyp()._jmk.Length == 0 && !isSaveJoint)
                return;

            // clear all data in main screen
            joint_list.UnselectAll();
            member_list.UnselectAll();
            ViewModel.UnselectJoint();
            ViewModel.ResetJointMarkType();
            ViewModel.ResetJointMember();
            WidthHeader.Width = 0;
            HeightHeader.Width = 0;
            for (int i = 0; i < ViewModel.showedMember; i++)
            {
                _tbxWidth[i].Width = 0;
                _tbxHeight[i].Width = 0;
            }
        }

        private bool IsJtMrkExisting(string mrk)
        {
            return ViewModel.JointMarks.Where(x => x.Value == mrk).Count() > 0;
        }

        private bool IsMrkChanged(string mrk)
        {
            return DataRepository.GetMrkTyp()._jmk != mrk;
        }

        private void SelectJointByMrk(string mrk)
        {
            var jts = ViewModel.JointMarks.Where(x => x.Value == mrk).ToList();
            if (jts.Count() > 0)
                joint_list.SelectedIndex = jts[0].Key;
        }

        private void JointMarkChange(string mrk)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            // only process if mark is actually changed
            if (!IsMrkChanged(mrk))
                return;

            // in case, joint mark is not existing, then we create new one
            if (IsJtMrkExisting(mrk))
            {
                // select joint
                DataRepository.SelectJoint(mrk);
                SelectJointByMrk(mrk);
                ViewModel.UpdateMarkTypeOnJointMark();
            }
            else
            {
                // update tmp mark type
                var mrkTyp = DataRepository.GetMrkTyp();
                mrkTyp._jmk = mrk;
                DataRepository.UpdateTmpMrkTyp(mrkTyp);
                UpdateDrawWin();
                ViewModel.UpdateMarkTypeOnJointMark();
            }

            // Load members
            ViewModel.UpdateListElement();

            // load grade codes
            UpdateGradeCodesAll();

            // update drawWIN
            UpdateDrawWin();
            sw.Stop();
            Debug.WriteLine($"select joint: {sw.ElapsedMilliseconds}");
        }

        public void UpdateDrawWin()
        {
            DrawArea.Manager.DrawWin(DrawArea.WinID);
        }

        private void SelectMaterialMark(int index, string mrk)
        {
            // Select Joint Member Data
            DataRepository.SelectJointMemberDat(index, mrk);
            // load mark type data
            ViewModel.UpdateMarkTypeOnJointMark();

            // Load members
            ViewModel.UpdateListElement();

            // load grade codes
            UpdateGradeCodesAll();
        }
        private void SelectMaterCallMark()
        {
            // load mark type data
            ViewModel.UpdateMarkTypeOnJointMark();

            // Load members
            ViewModel.UpdateListElement();

            // load grade codes
            UpdateGradeCodesAll();

            // update drawWIN
            UpdateDrawWin();
        }

        private void JgMrkKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var mrk = jointMark.GetTextBox().Text.Trim();
                if (0 == mrk.Length)
                {
                    // unselect joint
                    e.Handled = true;
                    Application.Current.Dispatcher.Invoke(() => this.ShowWarning("入力値が不正です。", "入力エラー"));
                    JointMarkClear();
                    jointMark.GetTextBox().Focus();

                }
                else
                {
                    JointMarkChange(mrk);
                }
            }
        }

        private void JgMrkLostFocus(object sender, RoutedEventArgs e)
        {
            var mrk = jointMark.GetTextBox().Text.Trim();
            if (0 == mrk.Length)
            {
                JointMarkClear();
            }
            else
            {
                JointMarkChange(mrk);
            }
        }

        public void AddEvent()
        {
            jointMark.GetTextBox().LostFocus += JgMrkLostFocus;
            jointMark.GetTextBox().PreviewKeyDown += JgMrkKeyDown;
        }

        public void RemoveEvent()
        {
            jointMark.GetTextBox().LostFocus -= JgMrkLostFocus;
            jointMark.GetTextBox().PreviewKeyDown -= JgMrkKeyDown;
        }

        private void handleMemberInputMethodChanged()
        {
            if (bzinp.SelectedIndex == 1)
            {
                grades[2].Visibility = Visibility.Hidden;
                matchTypes[2].IsEnabled = true;
                matchTypes[2].SetValue(Grid.ColumnProperty, 2);
            }
            else
            {
                grades[2].Visibility = Visibility.Visible;
                matchTypes[2].SetValue(Grid.ColumnProperty, 10);
            }
        }
        #endregion

        #region MemberInput functions
        private void OnAddNewMember(object sender, RoutedEventArgs e)
        {
            try
            {
                ViewModel.AddNewMember();
                elementsScrollViewer.ScrollToBottom();

                //call function generate data
                UpdateGradeCodes(ViewModel.showedMember - 1);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void OnDeleteMemberClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var memberId = int.Parse(((Button)sender).Tag.ToString()!);
                ViewModel.DeleteMember(memberId - 1);
                ViewModel.UpdateTempElement();
                UpdateGradeCodesAll();
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void ElementsScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            try
            {
                // Sync Input Elements Data scrolls
                elementsHeaderScrollViewer.Margin = elementsScrollViewer.ComputedVerticalScrollBarVisibility == Visibility.Visible
                    ? new Thickness(0, 0, SystemParameters.VerticalScrollBarWidth * 2, 0)
                    : new Thickness(0, 0, 0, 0);

                elementsHeaderScrollViewer.ScrollToHorizontalOffset(elementsScrollViewer.HorizontalOffset);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }


        //private void Cmb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (jointMark.SelectedIndex >= 0 || jointMark.GetTextBox().Text.Length > 0)
        //        ViewModel.UpdateTempElement();

        //}
        private void MemberCombobox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;
                var cb = sender as FCComboBox;
                if (!ValidateInput(cb))
                {
                    e.Handled = true;
                    cb.GetTextBox().Focus();
                    cb.GetTextBox().SelectAll();
                }
                else
                {
                    ViewModel.UpdateTempElement();
                    //TraversalRequest request = new TraversalRequest(FocusNavigationDirection.Next);
                    //request.Wrapped = true;
                    //cb.MoveFocus(request);
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void MemberTextbox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;
                var tbx = sender as TextBox;
                if (double.Parse(tbx.Text) == 0)
                {
                    Application.Current.Dispatcher.Invoke(() => this.ShowWarning("入力値が不正です。", "入力エラー"));
                    e.Handled = true;
                    tbx.Focus();
                    tbx.SelectAll();
                }
                else
                {
                    ViewModel.UpdateTempElement();
                }
                //TraversalRequest request = new TraversalRequest(FocusNavigationDirection.Next);
                //request.Wrapped = true;
                //tbx.MoveFocus(request);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        private void EdgeDepth_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox tbx = e.OriginalSource as TextBox;
            Regex rgx = new Regex("[^0-9.-]+");
            e.Handled = rgx.IsMatch(tbx.Text);
            string value = Regex.Replace(tbx.Text, "[A-Za-z ]", "");
            //double parsedValue = double.Parse(value);
            tbx.Text = value;

        }
        #endregion

        private void ComboboxMrkTyp_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var cb = sender as FCComboBox;
                if (!ValidateInput(cb))
                {
                    e.Handled = true;
                }

                TraversalRequest request = new TraversalRequest(FocusNavigationDirection.Next);
                request.Wrapped = true;
                cb.MoveFocus(request);

            }

        }
        private void ComboboxGrade_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                var cmb = sender as FCComboBoxExpansion;
                if (cmb.SelectedItem == null) return;
                int index = (cmb.TabIndex - 121) / 10;
                var keyValue = (FCComboBoxClass)cmb.SelectedItem;
                int.TryParse(keyValue.Key, out var idx);
                var selectedVal = new KeyValuePair<int, string>(idx, keyValue.Val).Key;

                if (e.Key == Key.Enter)
                {
                    if (ValidateInputGrade(cmb))
                    {
                        if (index == 0 && selectedVal <= 0)
                        {
                            e.Handled = true;
                            Application.Current.Dispatcher.Invoke(() => this.ShowWarning("入力値が不正です。", "入力エラー"));
                            cmb.GetTextBox().SelectAll();
                        }
                        else
                        {
                            ValidateGradeCode(cmb, index);
                        }
                    }
                    else
                    {
                        e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }

        }
        private bool ValidateInput(FCComboBox cmb)
        {
            try
            {
                if (cmb.ItemsSource is Dictionary<int, string> itemSrc)
                {
                    if (itemSrc.Values.Any(x => x == cmb.Text))
                    {
                        return true;
                    }
                    if (string.IsNullOrEmpty(cmb.Text))
                    {
                        cmb.GetTextBox().SelectAll();
                        return false;
                    }
                    int input;
                    bool isNumric = int.TryParse(cmb.Text, out input);

                    if (!isNumric)
                    {
                        cmb.GetTextBox().SelectAll();
                        return false;
                    }

                    var indexOf = (cmb.ItemsSource as Dictionary<int, string>).IndexOf(input);
                    if (indexOf == -1)
                    {
                        cmb.GetTextBox().SelectAll();
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        private bool ValidateInputGrade(FCComboBox cmb)
        {
            try
            {
                if (cmb.ItemsSource is List<FCComboBoxClass> gradeItems)
                {
                    if (string.IsNullOrEmpty(cmb.Text))
                    {
                        cmb.GetTextBox().SelectAll();
                        return false;
                    }
                    if (gradeItems.Any(x => x.Both == cmb.Text))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        private void MemberTextbox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tbx = e.OriginalSource as TextBox;
            tbx.Text = FCTextBoxCommon.Convert(tbx.Text, "%6.1f");
        }

        private void GridSplitter_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MainGrid.ColumnDefinitions[0].Width = new GridLength(2, GridUnitType.Star);
            MainGrid.ColumnDefinitions[2].Width = new GridLength(1, GridUnitType.Star);
        }

        private void GridSplitter1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PanelGrid.RowDefinitions[0].Height = new GridLength(1, GridUnitType.Star);
            PanelGrid.RowDefinitions[2].Height = new GridLength(1, GridUnitType.Star);
        }

        private void gradeCode_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                // TODO: need use index, instead of tabindex
                var cmb = e.GetComboBox();
                int index = (cmb.TabIndex - 121) / 10;

                var itemSrc = DataRepository.GetGradeCodes(index).Values;
                string content = "[  ";
                foreach (var item in itemSrc)
                {
                    content = content + item + "  ";
                }
                content = content + "]";
                SetInfomationToLabel(content);

                ShowWidthAndHeightLabel();
                if (cmb.SelectedValue != null)
                {
                    int key = (int)cmb.SelectedValue;
                    ViewModel.HandleMemberControlEnabled(key, index);
                }
                //Selection Changed Event
                cmb.SelectionChanged += GradeCode_SelectionChanged;
            }
            catch (Exception ex)
            {
                notificationLabel.Content = string.Empty;
                ex.HandleException();
            }
        }

        private void ShowWidthAndHeightLabel()
        {
            WidthHeader.Width = 0;
            HeightHeader.Width = 0;
            List<int?> grade = ViewModel.ElementViewModels.Select(x => x.GradeCodeId).ToList();
            if (grade.Contains(404))
            {
                WidthHeader.Width = 70;
                HeightHeader.Width = 70;
            }
            else if (grade.Contains(311) || grade.Contains(310))
            {
                WidthHeader.Width = 70;
            }
        }

        private void GradeCode_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (e.AddedItems == null || e.AddedItems.Count == 0) return;
                var cmb = e.GetComboBox() as FCComboBoxExpansion;
                int index = (cmb.TabIndex - 121) / 10;
                int key = (int)cmb.SelectedValue;

                ViewModel.HandleMemberControlEnabled(key, index);
                FormatDimension(index);
                ShowWidthAndHeightLabel();
                if (key >= 0) ViewModel.UpdateTempElement();
                string numOfDimension;
                cmb.KeyValuePairs2.TryGetValue(cmb.SelectedValue.ToString(), out numOfDimension);
                int numOfShowedDms = int.Parse(numOfDimension);
                if (numOfShowedDms >= 3)
                    HandleSetSuggestion(index, cmb);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void HandleSetSuggestion(int index, FCComboBoxExpansion cmb)
        {
            var l = new List<string>();

            if (ViewModel.HandleSelectSuggestion(index))
            {
                var dict3 = new Dictionary<string, object>();
                //change keyvaluepair3
                foreach (var item in ViewModel.DimensionSuggest)
                {
                    var str = item.Value.Split('-')[1].ToString();
                    if (!l.Contains(str))
                    {
                        l.Add(str);
                    }
                }
                if (cmb.KeyValuePairs2.Count == 0) return;
                for (var j = 0; j < cmb.KeyValuePairs.Count; j++)
                {
                    var kvp = cmb.KeyValuePairs.ElementAt(j);
                    var zaishu = kvp.Value;
                    var sunpousuu = int.Parse(cmb.KeyValuePairs2[kvp.Key]);

                    dict3.Add(kvp.Key, GetSunpou(sunpousuu, zaishu, l));
                }

                cmb.KeyValuePairs3 = dict3;
            }
        }

        private void MemberCombobox_GotFocus(object sender, RoutedEventArgs e)
        {
            CastInfomationToLabel(e.GetComboBox());
        }

        private void CastInfomationToLabel(ComboBox cmb)
        {
            if (cmb is null)
            {
                notificationLabel.Content = string.Empty;
                return;
            }

            try
            {
                var itemSrc = cmb.Items.OfType<string>().Any() ? cmb.Items.Cast<string>() : cmb.Items.Cast<dynamic>().Select(x => x.Value as string);

                string content = "[  ";
                foreach (var item in itemSrc)
                {
                    content = content + item + "  ";
                }
                content = content + "]";

                SetInfomationToLabel(content);
            }
            catch (Exception ex)
            {
                notificationLabel.Content = string.Empty;
                ex.HandleException();
            }
        }

        private void SetInfomationToLabel(string text)
        {
            notificationLabel.Content = text.Length < 140 ? text : $"{text.Substring(0, 140)} ...";

        }
        private void gradeCode_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                notificationLabel.Content = "";
                var cmb = sender as FCComboBoxExpansion;
                int index = (cmb.TabIndex - 121) / 10;
                cmb.SelectionChanged -= GradeCode_SelectionChanged;
                ValidateGradeCode(cmb, index);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void joint_list_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down || e.Key == Key.Up && e.IsRepeat)
            {
                e.Handled = true;
            }
        }

        private void UpdateGradeCodes(int memberInd)
        {
            //handle add keyvaluepair for comboboxexpension
            //寸法数
            Dictionary<string, string> dic2 = new Dictionary<string, string>
                {
                    { "0", "0" },
                    { "1", "2" },
                    { "2", "4" },
                    { "3", "4" },
                    { "50", "4" },
                    { "51", "4" },
                    { "52", "4" },
                    { "53", "0" },
                    { "90", "4" },
                    { "4", "3" },
                    { "5", "4" },
                    { "9", "4" },
                    { "10", "4" },
                    { "11", "3" },
                    { "16", "3" },
                    { "19", "2" },
                    { "23", "3" },
                    { "94", "4" },
                    { "204", "3" },
                    { "205", "4" },
                    { "210", "4" },
                    { "211", "3" },
                    { "310", "4" },
                    { "311", "3" },
                    { "404", "3" },
                    { "999", "1" }
                };

            //KeyValuePairs
            //KeyValuePairs1
            var gradeCodes = DataRepository.GetGradeCodes(memberInd);
            var idx = ViewModel.ElementViewModels[memberInd].GradeCodeId;
            bool idx_existing = false;

            var dict1 = new Dictionary<string, string>();
            var dict2 = new Dictionary<string, string>();
            foreach (var subDic0 in gradeCodes)
            {
                var listStr = subDic0.Value.Split('.');
                var value = "";
                for (int j = 1; j < listStr.Length; j++)
                {
                    value += listStr[j];
                    if (subDic0.Key == idx)
                        idx_existing = true;
                }
                dict1.Add(subDic0.Key.ToString(), value);
                dic2.TryGetValue(subDic0.Key.ToString(), out var dict2Value);
                dict2.Add(subDic0.Key.ToString(), dict2Value);
            }
            grades[memberInd].KeyValuePairs = dict1;
            grades[memberInd].KeyValuePairs2 = dict2;
            grades[memberInd].KeyValuePairs3 = new Dictionary<string, object>();

            if (idx_existing)
            {
                grades[memberInd].SelectedValue = idx;
            }
            else
            {
                grades[memberInd].SelectedIndex = 0;
            }
            dms[memberInd].ComboBoxExpansion = grades[memberInd];
            Log.Logger.Information("MainScreenWindow.xaml.cs MainScreenWindow:UpdateGradeCodes , Message :  Done UpdateGradeCodes");
        }

        private void jointMark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var cmb = (FCComboBox)sender;

                if (cmb.Text.Trim().Equals(""))
                {
                    e.Handled = true;
                    cmb.GetTextBox().SelectAll();
                    cmb.Focus();
                }
            }
        }

        private void MrkTypInpLostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                FCComboBox cmb = sender as FCComboBox;
                if (cmb.SelectedItem != null)
                {
                    ValidateMrkTypInpDat();
                    if (cmb.Name == "hanch" || cmb.Name == "bzinp")
                    {
                        ViewModel.LimitNewMembers();
                        if (cmb.Name == "bzinp")
                            handleMemberInputMethodChanged();
                        if (cmb.Name == "hanch" && ViewModel.IsEndDepthVisiable())
                        {
                            for (int i = 0; i < ViewModel.showedMember; i++)
                            {
                                _tbxTdep[i].IsEnabled = true;
                                _tbxTdep[i].Text = FCTextBoxCommon.Convert(_tbxTdep[i].Text, "%6.1f");
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void MrkTypInpKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter)
                return;

            try
            {
                FCComboBox cmb = sender as FCComboBox;
                if (ValidateInput(cmb) && cmb.SelectedItem != null)
                {
                    ValidateMrkTypInpDat();
                    if (cmb.Name == "hanch" || cmb.Name == "bzinp")
                    {
                        ViewModel.LimitNewMembers();
                        if (cmb.Name == "bzinp")
                            handleMemberInputMethodChanged();
                    }
                }
                else
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void UpdateGradeCodesAll()
        {
            for (int i = 0; i < ViewModel.showedMember; i++)
            {
                UpdateGradeCodes(i);
                FormatDimension(i);
            }
            ShowWidthAndHeightLabel();
        }

        private void ValidateMrkTypInpDat()
        {
            var cMrkTyp = DataRepository.GetMrkTyp();

            // update mrk type
            ViewModel.UpdTmpMrkTyp();

            var uMrkTyp = DataRepository.GetMrkTyp();

            // only update member if joint mark or flg_type or web_type has changed
            if (cMrkTyp._jmk != uMrkTyp._jmk ||
                cMrkTyp._ftyp != uMrkTyp._ftyp ||
                cMrkTyp._wtyp != uMrkTyp._wtyp)
            {
                ViewModel.UpdateListElement();
                UpdateGradeCodesAll();
            }
        }

        private void ValidateGradeCode(FCComboBoxExpansion cmb, int index)
        {
            if (cmb == null || cmb.SelectedItem == null) return;
            int key = (int)cmb.SelectedValue;
            if (key >= 0)
            {
                ViewModel.HandleMemberControlEnabled(key, index);
                FormatDimension(index);
                ViewModel.UpdateTempElement();
            }
            if (_tbxDms3[index].IsEnabled)
            {
                var l = new List<string>();
                HandleSetSuggestion(index, grades[index]);
            }
        }

        private void MemberInput_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                notificationLabel.Content = "";
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        private void MrkTypGotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement element)
                {
                    String imageSource = System.AppDomain.CurrentDomain.BaseDirectory + @"svg\file_name";
                    switch (element.Name)
                    {
                        case "ftyp":
                            imageSource = imageSource.Replace("file_name", "GD_JR22_JTYPE.svg");
                            break;
                        case "wtyp":
                            imageSource = imageSource.Replace("file_name", "GD_JR22_JTYPE.svg");
                            break;
                        case "bzinp":
                            imageSource = imageSource.Replace("file_name", "GD_JR22_BZINP.svg");
                            break;
                        case "hanch":
                            imageSource = imageSource.Replace("file_name", "GD_JR22_HANCH3.svg");
                            break;
                        case "gradeCode":
                        case "dimension":
                            imageSource = imageSource.Replace("file_name", "GD_JR22_BZINP.svg");
                            break;
                        case "tdep":
                            imageSource = imageSource.Replace("file_name", "GD_JR22_HANCH3.svg");
                            break;
                    }
                    svgImgBottom.Load(new Uri(imageSource));
                }
            }
            catch (Exception ex)
            {

            }
        }

    }
}

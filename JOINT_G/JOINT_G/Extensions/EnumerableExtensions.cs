using System;
using System.Collections.Generic;
using System.Linq;

namespace JOINT_G.Extensions
{
    /// <summary>
    /// string配列のExtensionクラス。
    /// </summary>
    internal static class EnumerableExtensions
    {
        public static Func<string, bool> NullOrWhiteSpace => str => !string.IsNullOrWhiteSpace(str);

        public static Func<string, string> Trim => str => str.Trim();

        public static IEnumerable<string> NotEmpty(this IEnumerable<string> strings)
        {
            if (strings == null)
            {
                throw new ArgumentNullException(nameof(strings));
            }

            return strings.Where(NullOrWhiteSpace)!;
        }

        public static IEnumerable<string> Trimmed(this IEnumerable<string> strings)
        {
            if (strings == null)
            {
                throw new ArgumentNullException(nameof(strings));
            }

            return strings.Select(Trim);
        }

        public static IEnumerable<string> NotEmptyTrimmed(this IEnumerable<string> strings)
        {
            return strings.NotEmpty().Trimmed();
        }

        public static string Join(this IEnumerable<string> strings, string separator)
        {
            if (strings == null)
            {
                throw new ArgumentNullException(nameof(strings));
            }

            return string.Join(separator, strings);
        }
    }
}

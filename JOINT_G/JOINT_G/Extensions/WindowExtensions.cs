using FastWpfApp.Helpers;
using JOINT_G.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace JOINT_G.Extensions
{
    /// <summary>
    /// WindowのExtensionクラス。フォーカス移動の制御、ダイアログ表示などを行う。
    /// </summary>
    internal static class WindowExtensions
    {
        internal static void MoveFocus(this Window wnd, Control ctrl, Key key)
        {
            MoveFocus(wnd, ctrl, key == Key.Enter ? FocusNavigationDirection.Next : FocusNavigationDirection.Previous);
        }

        internal static void MoveFocus(this Window wnd, Control ctrl, FocusNavigationDirection dir)
        {
            var requestNext = new TraversalRequest(dir);
            var keyboardFocused = Keyboard.FocusedElement as UIElement;
            if (keyboardFocused != null)
            {
                if (ctrl is not null && dir == FocusNavigationDirection.Previous)
                {
                    ctrl.MoveFocus(requestNext);
                }
                else
                {
                    keyboardFocused.MoveFocus(requestNext);
                }
            }
        }

        internal static IEnumerable<T> FindVisualChilds<T>(this DependencyObject depObj) where T : DependencyObject
        {
            if (depObj == null)
            {
                yield return (T)Enumerable.Empty<T>();
            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                DependencyObject ithChild = VisualTreeHelper.GetChild(depObj, i);
                if (ithChild == null)
                {
                    continue;
                }

                if (ithChild is T t)
                {
                    yield return t;
                }

                foreach (T childOfChild in FindVisualChilds<T>(ithChild))
                {
                    yield return childOfChild;
                }
            }
        }

        public static void ShowInfo(this Window wnd, string text, string caption = null)
        {
            TaskDialogHelper.ShowInfo(text, caption);
        }

        public static void ShowError(this Window wnd, string text, string caption = null)
        {
            TaskDialogHelper.ShowError(text, caption);
        }

        public static void ShowErrorConfirmation(this Window wnd, string text, string caption = null)
        {
            TaskDialogHelper.ShowErrorConfirmation(text, caption);
        }

        public static void ShowWarning(this Window wnd, string text, string caption)
        {
            TaskDialogHelper.ShowWarning(text, caption);
        }

        public static bool ShowWarningConfirmation(this Window wnd, string text, string caption)
        {
            return TaskDialogHelper.ShowWarningConfirmation(text, caption);
        }
        public static bool ShowWarningDeleteConfirmation(this Window wnd, string text, string caption)
        {
            return TaskDialogHelper.ShowWarningYesNoConfirmation(text, caption);
        }
        public static void RemoveIcon(this Window wnd)
        {
            IconHelper.RemoveIcon(wnd);
        }
    }
}

using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;

namespace JOINT_G.Extensions
{
    /// <summary>
    /// ComboboxのExtension。
    /// </summary>
    internal static class ComboBoxExtensions
    {
        internal static void UpdateText(this ComboBox cmb)
        {
            // TODO: Find better solutions. This trigger validation 2 times
            var index = cmb.SelectedIndex;
            cmb.SelectedIndex = -1;
            cmb.SelectedIndex = index;
        }
        /// <summary>
        /// KeyEventArgs から Combobox を取得するための拡張機能.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static ComboBox GetComboBox(this KeyEventArgs e)
        {
            return GetComboBox(e.Source, e.OriginalSource);
        }

        /// <summary>
        /// RoutedEventArgs から Combobox を取得するための拡張機能.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static ComboBox GetComboBox(this RoutedEventArgs e)
        {
            return GetComboBox(e.Source, e.OriginalSource);
        }

        private static ComboBox GetComboBox(object source, object originalSource)
        {
            ComboBox cmb = null;
            if (source is ComboBox)
            {
                cmb = (ComboBox)source;
            }
            else if ((originalSource as Control)?.TemplatedParent is ComboBox)
            {
                cmb = (ComboBox)((Control)originalSource).TemplatedParent;
            }

            return cmb;
        }
    }
}

using Serilog.Sinks.File.Header;
using System;

namespace JOINT_G.Configuration.Serilog
{
    /// <summary>
    /// ログ出力の設定に必須な宣言。
    /// </summary>
    public class SerilogHooks
    {
        public static HeaderWriter Header => new HeaderWriter($"{Environment.NewLine}######################[BEGIN LOGGING AT {DateTime.Now:yyyy-MM-dd HH:mm:ss.fff}]############################", true);
    }
}

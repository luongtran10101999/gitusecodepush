using Serilog;
using newfast;
using System.Reflection;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using JOINT_G.Models;
using JOINT_G.Extensions;
using System.Runtime.InteropServices;
using JOINT_G.Helpers;

namespace JOINT_G.Repository
{
    public static class DataRepository
    {

        public static JghedMaster jig = new JghedMaster();
        private static Dictionary<string, int> flgDict = new Dictionary<string, int>(){
            {"None", 0},
            {"SP", 1},
            {"TGP", 4},
            {"GY", 7},
            {"PL", 8},
        };
        private static Dictionary<string, int> webDict = new Dictionary<string, int>(){
            {"None", 0},
            {"SP", 1},
            {"GP", 2},
            {"GSP", 3},
            {"TDP", 4},
        };

        public static void Initialize()
        {
            try
            {
                jig.Initialize();
            }
            catch (Exception e)
            {
                e.HandleException();
            }
        }

        public static List<MaterialModel> GetMemberLst()
        {
            var lst = new List<MaterialModel>();
            try
            {
                var query = jig.GetMemberLst();
                lst = query.Select((x, index) => new MaterialModel()
                {
                    Id = index + 1,
                    JointMark = x._joimk ?? "",
                    DesignMark = x._sekmk,
                    BeamDivisionString = x._gkubun,
                    Plating = x._mekki,
                    SideUseString = x._yoko,
                    Flange = x._typ_f,
                    Web = x._typ_w,
                    CentralMember = x._wbuf1,
                    FlangeStandard1 = x._wbuf2,
                    EndMember = x._wbuf3,
                    FlangeStandard2 = x._wbuf4.Length > 0 ? x._wbuf4.Split(",")[0] : "",
                    WebStandard2 = x._wbuf4.Length > 0 ? x._wbuf4.Split(",")[1] : "",
                    MatchMethod = x._htenflg,
                    BeamHaunch = x._hanchd,
                    JointHaunch = x._hanchj
                }
                ).Where(x => !string.IsNullOrWhiteSpace(x.JointMark) ||
                !string.IsNullOrWhiteSpace(x.DesignMark) ||
                !string.IsNullOrWhiteSpace(x.BeamDivisionString) ||
                !string.IsNullOrWhiteSpace(x.Plating) ||
                !string.IsNullOrWhiteSpace(x.SideUseString) ||
                !string.IsNullOrWhiteSpace(x.Flange) ||
                !string.IsNullOrWhiteSpace(x.Web) ||
                !string.IsNullOrWhiteSpace(x.CentralMember) ||
                !string.IsNullOrWhiteSpace(x.FlangeStandard1) ||
                !string.IsNullOrWhiteSpace(x.FlangeStandard2) ||
                !string.IsNullOrWhiteSpace(x.EndMember) ||
                !string.IsNullOrWhiteSpace(x.WebStandard1) ||
                !string.IsNullOrWhiteSpace(x.WebStandard2)
                ).ToList();
                //Log.Logger.Information($"Get Joint Material List");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return lst;
        }
        public static (int, int, int) GetFilterMemberList()
        {
            try
            {
                int spCntMed;
                int center;
                int bhInpMed;
                jig.GetFilterMemberLst(out spCntMed, out center, out bhInpMed);
                Log.Logger.Information($"DataRepository.cs DataRepository:GetFilterMemberList , Message : Get Filter Member List");
                return (spCntMed, center, bhInpMed);
            }
            catch (Exception e)
            {
                e.HandleException();
                return (0, 0, 0);
            }
        }
        public static void FilterMemberList(int spCntMed, int center, int bhInpMed)
        {
            try
            {
                jig.FilterMemberLst(spCntMed, center, bhInpMed);
                Log.Logger.Information($"DataRepository.cs DataRepository:FilterMemberList , Message : Filter Member List");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
        }
        public static MrkTypDat GetMrkTyp()
        {
            MrkTypDat mrkTyp = new MrkTypDat();
            try
            {
                mrkTyp = jig.GetMrkTyp();
                Log.Logger.Information($"DataRepository.cs DataRepository:GetMrkTyp , Message : Get MarkType Data");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return mrkTyp;
        }
        public static object GetFlgData(int flgTyp)
        {
            object flgData = new object();
            try
            {
                //flgData = jig.GetFlgData(Convert.ToInt16(flgTyp));
                //Log.Logger.Information($"Get Flange Data");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return flgData;
        }
        public static bool DeleteJointMrk(string jointMrk)
        {
            try
            {
                jig.DeleteJointMrk(jointMrk);
                Log.Logger.Information($"DataRepository.cs DataRepository:DeleteJointMrk , Message : Delete Joint: {jointMrk}");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }
        public static (string[], int[]) ValidateInp()
        {
            int[] status;
            string[] errMsgs;
            try
            {
                jig.ValidateInp(out errMsgs, out status);
                Log.Logger.Information($"DataRepository.cs DataRepository:ValidateInp , Message : Validate Joint Data Input");
                return (errMsgs, status);
            }
            catch (Exception e)
            {
                e.HandleException();
                return (new string[30], new int[30]);
            }
        }
        public static bool StartSavingProcess()
        {
            try
            {
                jig.StartSavingProcess();
                Log.Logger.Information($"DataRepository.cs DataRepository:StartSavingProcess , Message : Start Saving Joint Process");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }
        public static bool SaveJoint()
        {
            try
            {
                jig.SaveJoint();
                Log.Logger.Information($"DataRepository.cs DataRepository:SaveJoint , Message : Save Joint");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }

        }
        public static bool SelectJoint(string mrk)
        {
            try
            {
                jig.SelectJoint(mrk);
                Log.Logger.Information($"DataRepository.cs DataRepository:SelectJoint , Message : Select Joint: {mrk}");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }

        public static void UnselectJoint()
        {
            try
            {
                jig.UnselectJoint();
            }
            catch (Exception e)
            {
                e.HandleException();
                return;
            }
        }

        public static bool SelectJointMemberDat(int index, string mrk)
        {
            try
            {
                jig.SelectJointMemberDat(Convert.ToInt16(index), mrk);
                Log.Logger.Information($"DataRepository.cs DataRepository:SelectJointMemberDat , Message : Select Joint MemberDat: {mrk}");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }

        public static bool UpdateTmpMrkTyp(MrkTypDat mrkTyp)
        {
            try
            {
                jig.UpdateTmpMrkTyp(mrkTyp);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateTmpMrkTyp , Message : Update Temp MarkType: {mrkTyp._jmk}");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }
        public static bool UpdateTmpMembers(MemberDat[] memberDats)
        {
            try
            {
                jig.UpdateTmpMembers(memberDats);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateTmpMembers , Message : Update Temp Members");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }
        public static List<MemberDat> GetMembers()
        {
            List<MemberDat> members = null;
            try
            {
                members = jig.GetMembers().ToList();
                Log.Logger.Information($"DataRepository.cs DataRepository:GetMembers , Message : Get edit Members");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return members;
        }
        public static Dictionary<int, string> GetSuggestion(int flg, int index)
        {
            Dictionary<int, string> members = null;
            try
            {
                members = jig.SuggestMemberLst(Convert.ToInt16(flg), Convert.ToInt16(index))
                    .Select((data, i) => new { Index = i, Value = $"{i}.{data}" })
                    .Where(x => !string.IsNullOrWhiteSpace(x.Value))
                    .ToDictionary(x => x.Index, x => x.Value.Trim());
                Log.Logger.Information($"DataRepository.cs DataRepository:GetSuggestion , Message : Get Suggestion List");
            }
            catch (Exception e)
            {
                e.HandleException();
                return null;
            }
            return members;
        }
        public static Dictionary<int, string> GetStandardMembersMain()
        {
            Dictionary<int, string> standardMembers = new Dictionary<int, string>();
            try
            {
                standardMembers = jig.GetStandardMembers()
                    .Select((data, i) => new { Index = i, Value = $"{i}.{data}" })
                    .Where(x => !string.IsNullOrWhiteSpace(x.Value))
                    .ToDictionary(x => x.Index, x => x.Value.Trim());
                Log.Logger.Information($"DataRepository.cs DataRepository:GetStandardMembersMain , Message : Get Standard Members");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return standardMembers;
        }
        public static string[] GetStandardMembers()
        {
            string[] standardMembers = new string[0];
            try
            {
                standardMembers = jig.GetStandardMembers();
                Log.Logger.Information($"DataRepository.cs DataRepository:GetStandardMembers , Message : Get Standard Members");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return standardMembers;
        }
        public static Dictionary<int, string> GetJointListOfficialMrk()
        {
            Dictionary<int, string> officialMrk = new Dictionary<int, string>();
            try
            {
                officialMrk = GetOfficalMrk()
                .Where(x => !string.IsNullOrWhiteSpace(x._mrk))
                .Select((data, i) => new { Index = i, Value = data._mrk })
                .ToDictionary(x => x.Index, x => x.Value.Trim());
                Log.Logger.Information($"DataRepository.cs DataRepository:GetJointListOfficialMrk , Message : Get Official Marks: [ {officialMrk.Values.Join(", ")}]");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return officialMrk;
        }
        public static (List<FittingModel>, Dictionary<int, string>, int) GetJointMarkDetails()
        {
            List<FittingModel> fittings = new List<FittingModel>();
            Dictionary<int, string> marks = new Dictionary<int, string>();

            try
            {
                Log.Logger.Information("DataRepository.cs DataRepository:GetJointMarkDetails , Message : Start loading JointMarkDetails (Fitting Models)");
                var officialMrk = GetOfficalMrk().ToList();
                var fittingsRes = jig.GetJointLst();
                if (officialMrk.Count < fittingsRes.Length)
                {
                    for (int i = fittingsRes.Length; i > officialMrk.Count; i--)
                    {
                        officialMrk.Add(new FmlMrk() { _fmlMrk = "", _mrk = "0000" });
                    }
                }
                fittings = fittingsRes.Select((x, index) => new FittingModel()
                {
                    Index = index + 1,
                    Web = x._web,
                    Mark = x._mrk,
                    Flange = x._flg,
                    ElementName = x._element,
                    WebStandard = x._web_std,
                    FlangeStandard = x._flg_std,
                    OfficialMark = officialMrk[index]._fmlMrk.Length > 0 ? officialMrk[index]._fmlMrk : x._mrk,
                    FlangeType = x._flg != null && x._flg != "" ? flgDict[x._flg] : -1,
                    WebType = x._web != null && x._web != "" ? webDict[x._web] : -1,
                }
                ).ToList();
                marks = fittingsRes.Select((x, index) => new { Index = index, Value = x._mrk })
                                    .ToDictionary(x => x.Index, x => x.Value.Trim());
                Log.Logger.Information("DataRepository.cs DataRepository:GetJointMarkDetails , Message : Finish loading JointMarkDetails (Fitting Models)");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return (fittings, marks, marks.Count);
        }
        public static Dictionary<int, string> GetGradeCodes(int index)
        {
            Dictionary<int, string> gradeCodes = new Dictionary<int, string>();
            try
            {
                gradeCodes = jig.GetGradeCodes(short.Parse(index.ToString()))
                    .ToDictionary(x => int.Parse(x.Split('.')[0]), x => x.Trim());
                Log.Logger.Information($"DataRepository.cs DataRepository:GetGradeCodes , Message : Get Grade Codes");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return gradeCodes;
        }
        public static FlgPlyDat GetFlgPlyDat()
        {
            FlgPlyDat flgPlyDat = new FlgPlyDat();
            try
            {
                flgPlyDat = jig.GetFlgPlyDat();
                Log.Logger.Information($"DataRepository.cs DataRepository:GetFlgPlyDat , Message : GetFlgPlyDat is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return flgPlyDat;
        }
        public static FlgGyDat GetFlgGyDat()
        {
            FlgGyDat flgGyDat = new FlgGyDat();
            try
            {
                flgGyDat = jig.GetFlgGyDat();
                Log.Logger.Information($"DataRepository.cs DataRepository:GetFlgGyDat , Message : GetFlgGyDat is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return flgGyDat;
        }
        public static FlgTgpDat GetFlgTgpDat()
        {
            FlgTgpDat flgTgpDat = new FlgTgpDat();
            try
            {
                flgTgpDat = jig.GetFlgTgpDat();
                Log.Logger.Information($"DataRepository.cs DataRepository:GetFlgTgpDat , Message : GetFlgTgpDat is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return flgTgpDat;
        }
        public static FlgFspDat GetFlgFspDat()
        {
            FlgFspDat flgFspDat = new FlgFspDat();
            try
            {
                flgFspDat = jig.GetFlgFspDat();
                Log.Logger.Information($"DataRepository.cs DataRepository:GetFlgFspDat , Message : GetFlgFspDat is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return flgFspDat;
        }
        public static FlgGstSpDat GetFlgGstSpDat()
        {
            FlgGstSpDat flgGstSpDat = new FlgGstSpDat();
            try
            {
                flgGstSpDat = jig.GetFlgGstSpDat();
                Log.Logger.Information($"DataRepository.cs DataRepository:GetFlgGstSpDat , Message : GetFlgGstSpDat is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return flgGstSpDat;
        }
        public static FlgGstPlDat GetFlgGstPlDat()
        {
            FlgGstPlDat flgGstPlDat = new FlgGstPlDat();
            try
            {
                flgGstPlDat = jig.GetFlgGstPlDat();
                Log.Logger.Information($"DataRepository.cs DataRepository:GetFlgGstPlDat , Message : GetFlgGstPlDat is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return flgGstPlDat;
        }
        public static FlgWspDat GetFlgWspDat()
        {
            FlgWspDat flgWspDat = new FlgWspDat();
            try
            {
                flgWspDat = jig.GetFlgWspDat();
                Log.Logger.Information($"DataRepository.cs DataRepository:GetFlgWspDat , Message : GetFlgWspDat is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return flgWspDat;
        }
        public static bool UpdateFlgPlyDat(FlgPlyDat flgPlyDat)
        {
            try
            {
                jig.UpdateFlgPlyDat(flgPlyDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateFlgPlyDat , Message : UpdateFlgPlyDat is Success");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }
        public static bool UpdateFlgGyDat(ref FlgGyDat flgGyDat)
        {
            jig.UpdateFlgGyDat(ref flgGyDat);
            try
            {
                jig.UpdateFlgGyDat(ref flgGyDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateFlgGyDat , Message : UpdateFlgGyDat is Success");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }
        public static bool UpdateFlgTgpDat(ref FlgTgpDat flgTgpDat)
        {
            try
            {
                jig.UpdateFlgTgpDat(ref flgTgpDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateFlgTgpDat , Message : UpdateFlgTgpDat is Success");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }

        public static (int, string) UpdateDimensionTgp(ref FlgTgpDat flgTgpDat)
        {
            try
            {
                jig.UpdateDimensionTgp(ref flgTgpDat, out int retcode, out string mess);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateDimensionTgp , Message : UpdateDimensionTgp is Success");
                return (retcode, mess);
            }
            catch (Exception e)
            {
                e.HandleException();
                return (0, "");
            }
        }
        public static bool UpdateFlgFspDat(ref FlgFspDat flgFspDat)
        {
            try
            {
                jig.UpdateFlgFspDat(ref flgFspDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateFlgFspDat , Message : UpdateFlgFspDat is Success");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }
        public static bool UpdateFlgGstSpDat(ref FlgGstSpDat flgGstSpDat)
        {
            try
            {
                jig.UpdateFlgGstSpDat(ref flgGstSpDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateFlgGstSpDat , Message : UpdateFlgGstSpDat is Success");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }
        public static bool UpdateFlgGstPlDat(ref FlgGstPlDat flgGstPlDat)
        {
            try
            {
                jig.UpdateFlgGstPlDat(ref flgGstPlDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateFlgGstPlDat , Message : UpdateFlgGstPlDat is Success");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }
        public static bool UpdateFlgWspDat(ref FlgWspDat flgWspDat)
        {
            try
            {
                flgWspDat._brhsumu = "0";
                jig.UpdateFlgWspDat(ref flgWspDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateFlgWspDat , Message : UpdateFlgWspDat is Success");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }
        public static FmlMrk[] GetOfficalMrk()
        {
            FmlMrk[] mrks = new FmlMrk[0];
            try
            {
                mrks = jig.GetOfficialMrk();
                Log.Logger.Information($"DataRepository.cs DataRepository:GetOfficalMrk , Message : Get GetOfficalMrk is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return mrks;
        }
        public static bool SetOfficalMrk(FmlMrk[] fmlMrks)
        {
            try
            {
                jig.SetOfficalMrk(fmlMrks);
                Log.Logger.Information($"DataRepository.cs DataRepository:SetOfficalMrk , Message : Set Offical Mark Success");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }
        public static (string[], int[]) ValidateInpDat(DetailType type)
        {
            int[] status;
            string[] mess;
            try
            {
                jig.ValidateInpDat(type, out mess, out status);
                Log.Logger.Information($"DataRepository.cs DataRepository:ValidateInpDat , Message : Validate Input Data: {type}");
                return (mess, status);
            }
            catch (Exception e)
            {
                e.HandleException();
                return (new string[3], new int[3]);
            }
        }
        public static OgamibuDat GetOgamibu()
        {
            OgamibuDat ogamibuDat = new OgamibuDat();
            try
            {
                jig.GetOgamibu(out ogamibuDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:GetOgamibu , Message : Get GetOgamibu is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return ogamibuDat;
        }

        public static void UpdateTgpHerik(ref FlgTgpDat flgTgpDat)
        {
            try
            {
                jig.UpdateTgpHerik(ref flgTgpDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateTgpHerik , Message : Update TgpHerik is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
        }

        public static void UpdateTgpLen(ref FlgTgpDat flgTgpDat)
        {
            try
            {
                jig.UpdateTgpLen(ref flgTgpDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateTgpLen , Message : Update TgpLen is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
        }

        public static bool UpdateOgamibu(OgamibuDat ogamibuDat)
        {
            try
            {
                jig.SetOgamibu(ogamibuDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateOgamibu , Message :  Set Offical Mark Success");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }

        public static void UpdateUraribGstPl(ref FlgGstPlDat flgGstPlDat)
        {
            try
            {
                jig.UpdateUraribGstPl(ref flgGstPlDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateUraribGstPl , Message :  Update Urarib of GstPl is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
        }


        public static void UpdateUraribGstSp(ref FlgGstSpDat flgGstSpDat)
        {
            try
            {
                jig.UpdateUraribGstSp(ref flgGstSpDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateUraribGstSp , Message :  Update Urarib of GstSp is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
        }


        public static void UpdateUraribTgp(ref FlgTgpDat flgTgpDat)
        {
            try
            {
                jig.UpdateUraribTgp(ref flgTgpDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:UpdateUraribTgp , Message :  Update Urarib of Tgp is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
        }

        public static void FirstUpdateFspBkei(ref FlgFspDat flgFspDat)
        {
            try
            {
                jig.FirstUpdateFspBkei(ref flgFspDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:FirstUpdateFspBkei , Message :  First Update FspBkei is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
        }

        public static void FirstUpdateGspBkei(ref FlgGstSpDat flgGstSpDat)
        {
            try
            {
                jig.FirstUpdateGspBkei(ref flgGstSpDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:FirstUpdateGspBkei , Message :  First Update GspBkei is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
        }


        public static void FirstUpdateGstPlBkei(ref FlgGstPlDat flgGstPlDat)
        {
            try
            {
                jig.FirstUpdateGstPlBkei(ref flgGstPlDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:FirstUpdateGstPlBkei , Message :  First Update GstPlBkei is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
        }

        public static void FirstUpdateTgpBkei(ref FlgTgpDat flgTgpDat)
        {
            try
            {
                jig.FirstUpdateTgpBkei(ref flgTgpDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:FirstUpdateTgpBkei , Message :  First Update TgpBkei is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
        }

        public static void FirstUpdateWspBkei(ref FlgWspDat flgWspDat)
        {
            try
            {
                jig.FirstUpdateWspBkei(ref flgWspDat);
                Log.Logger.Information($"DataRepository.cs DataRepository:FirstUpdateWspBkei , Message :  First Update WspBkei is Success");
            }
            catch (Exception e)
            {
                e.HandleException();
            }
        }

        public static MasterMeishoDat[] GetMasterMeishoLst()
        {
            MasterMeishoDat[] meishoDats = new MasterMeishoDat[0];
            try
            {
                meishoDats = jig.GetMasterMeishoLst();
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return meishoDats;
        }
        public static JointMeishoDat[] GetMasterMeishoLst(string meino)
        {
            JointMeishoDat[] jointMeishos = new JointMeishoDat[0];
            try
            {
                jointMeishos = jig.GetJointMeishoLst(meino);
            }
            catch (Exception e)
            {
                e.HandleException();
            }
            return jointMeishos;
        }
        public static bool CreateJointFromMeisho(string master_meisyo, string jointMeisyo, string mrk)
        {
            try
            {
                if (mrk == null) return false;
                jig.CreateJointFromMeisho(master_meisyo, jointMeisyo, mrk);
                Log.Logger.Information($"DataRepository.cs DataRepository:CreateJointFromMeisho , Message :  Create Joint FromMeisho Success");
                return true;
            }
            catch (Exception e)
            {
                e.HandleException();
                return false;
            }
        }
        public static string GetCaption()
        {
            try
            {
                Log.Logger.Information($"DataRepository.cs DataRepository:GetCaption , Message : GetCaption Success");
                return jig.Caption();
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }
        public static string GetTitle(DetailType detailType)
        {
            try
            {
                string[] memberInfor;
                string title;
                jig.GetDetailHeader(detailType, out memberInfor, out title);

                Log.Logger.Information($"DataRepository.cs DataRepository:GetTitle , Message : GetTitle Success");
                return title;
            }
            catch (Exception e)
            {
                e.HandleException();
                return null;
            }
        }
        public static string[] GetMemberInfor(DetailType detailType)
        {
            try
            {
                string[] memberInfor;
                string title;
                jig.GetDetailHeader(detailType, out memberInfor, out title);
                Log.Logger.Information($"DataRepository.cs DataRepository:GetMemberInfor , Message : GetMemberInfor Success");
                return memberInfor;
            }
            catch (Exception e)
            {
                e.HandleException();
                return null;
            }
        }
    }
}

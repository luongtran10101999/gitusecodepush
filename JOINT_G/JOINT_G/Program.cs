using System.Windows;
using newfast;
using System.Windows.Forms.Integration;
using Application = System.Windows.Application;
using JOINT_G.Views.MainScreen;
using Serilog;
using JOINT_G.Repository;
using FastCrew;
using JOINT_G.Helpers;
using System;
using Microsoft.Extensions.Configuration;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;
using FastCrew.Application.UI;
using System.Windows.Forms;

namespace JOINT_G
{
    public partial class Program : Application
    {
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                WindowsFormsHost.EnableWindowsFormsInterop();

                // log configuration
                IConfigurationBuilder builder = new ConfigurationBuilder();
                BuildConfig(builder);

                Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(builder.Build(), "Serilog")
                    .CreateLogger();

                //（未実装）マスタ設定画面として起動されます。
                if (args != null && args.Length == 1 && args[0].ToUpper() == "BIM_MST")
                {
                    return;
                }

                // initialize COM object
                Log.Logger.Information("COM Initialize");
                DataRepository.Initialize();
                Log.Logger.Information("COM Initialize Success");

                // start the main UI
                Program app = new Program();
                app.Run();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            Log.Logger.Information("App starting...");
            CultureHelper.SetJapaneseUiCulture();
            SetupExceptionHandling();

            base.OnStartup(e);

            MainScreenWindow app = new MainScreenWindow();
            app.Show();
            Log.Logger.Information("App started");
        }
        private static void BuildConfig(IConfigurationBuilder builder)
        {
            builder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
                .AddEnvironmentVariables();
        }

        private static void SetupExceptionHandling()
        {
            AppDomain.CurrentDomain.UnhandledException += (s, e) =>
            {
                LogUnhandledException((Exception)e.ExceptionObject, nameof(AppDomain.CurrentDomain.UnhandledException));
            };

            Current.DispatcherUnhandledException += (s, e) =>
            {
                if (e.Exception is ValidationException)
                {
                    return;
                }

                LogUnhandledException(e.Exception, nameof(Current.DispatcherUnhandledException));
            };

            TaskScheduler.UnobservedTaskException += (s, e) =>
            {
                LogUnhandledException(e.Exception, nameof(TaskScheduler.UnobservedTaskException));
                e.SetObserved();
            };
        }

        private static void LogUnhandledException(Exception exception, string source)
        {
            const string MessageTemplate = "Unhandled exception: ({0})";
            Log.Logger.Fatal(exception, MessageTemplate, source);
        }
    }
}

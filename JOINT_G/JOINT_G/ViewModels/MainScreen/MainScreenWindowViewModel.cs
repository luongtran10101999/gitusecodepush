using newfast;
using Serilog;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using JOINT_G.Models;
using JOINT_G.Repository;
using JOINT_G.Views.MainScreen;
using JOINT_G.Views.Dialogs;
using JOINT_G.Managers.Mappers;
using JOINT_G.Models.Enums;
using JOINT_G.Views.DetailsScreen;
using JOINT_G.Helpers;
using JOINT_G.Extensions;
using FCWPF.FCCommon.FCControls.Combination;
using System.Collections.ObjectModel;
using FCWPF.FCCommon.FCControls.Text;

namespace JOINT_G.ViewModels.MainScreen
{
    /// <summary>
    /// メイン画面のビューモデル。
    /// </summary>
    public class MainScreenWindowViewModel : ViewModelBase
    {
        #region General Declare
        private MainScreenWindow _mainScreen;
        public const string TitleTemplate = "梁継手　　　　工事名：　{0}　登録件数：{1}/{2}";
        public const int MaxMemebersCount = 30;
        public const int MemeberHeight = 26;
        public TextBox TbxJointMark;
        public List<FCInputDimension> Dms;
        public List<FCTextBox> TbxtDept;
        public List<FCTextBox> TbxWidth;
        public List<FCTextBox> TbxHeight;
        public int showedMember = 0;
        private string _currentSelectedJoint = "";
        private string _title;
        private string _notificationMessage;
        private string _notificationBackground;
        private Visibility _errorVisible = Visibility.Collapsed;
        private ObservableCollection<ElementViewModel> _elementViewModels;
        private string _constructionName = "サンプル工事データ";
        private int _completedRegistrations = 0;
        public bool MarkTypeValid { get; set; } = false;
        public bool MemberValid { get; set; } = false;
        private string _jointAction { get; set; }
        private string _jointSelectedAction { get; set; }
        private int _totalRegistrations { get; set; }

        private string _SelectedLine = string.Empty;

        public string SelectedLine
        {
            get => _SelectedLine;
            set => SetProperty(ref _SelectedLine, value);
        }


        public static Dictionary<int, string> FlangeJointTypes => new()
        {
            { 0, "0.無し" },
            { 1, "1.スプライス(SP)" },
            { 4, "4.T型ガセット(TGP)" },
            { 7, "7.現場溶接(GY)" },
            { 8, "8.PL溶接(PL)" }
        };

        public Dictionary<int, string> WebJointTypes { get; set; }

        private Dictionary<int, string> _flangeStandards;
        public Dictionary<int, string> FlangeStandards
        {
            get => _flangeStandards;
            set => SetProperty(ref _flangeStandards, value);
        }

        private Dictionary<int, string> _webStandards;
        public Dictionary<int, string> WebStandards
        {
            get => _webStandards;
            set => SetProperty(ref _webStandards, value);
        }

        public static Dictionary<int, string> BeamDivisions => new()
        {
            { 0, "0.大梁" },
            { 1, "1.小梁" },
            { 2, "2.大梁片持ち" },
            { 3, "3.小梁片持ち" }
        };

        public static Dictionary<int, string> MemberInputMethods => new()
        {
            { 0, "0.中央のみ" },
            { 1, "1.中央・端部" }
        };

        public static Dictionary<int, string> GPTypes => new()
        {
            { 0, "0.共通" },
            { 1, "1.現場溶接" }
        };

        public static Dictionary<int, string> SettingTypes => new()
        {
            { 0, "0.全断面" },
            { 1, "1.フランジ" },
            { 2, "2.ウェブ" }
        };

        public static Dictionary<int, string> SPTypes => new()
        {
            { 0, "0.現場溶接" },
            { 1, "1.ＳＰ（下）" },
            { 2, "2.ＳＰ（上）" }

        };
        public static Dictionary<int, string> HasPlatings => new()
        {
            { 0, "0.無し" },
            { 1, "1.有り" }
        };

        public static Dictionary<int, string> Existance => new()
        {
            { 0, "0.NO" },
            { 1, "1.YES" },
            { 2, "2.縦横共用" }
        };
        public static Dictionary<int, string> MatchTypes => new()
        {
            { 0, "0.背合せ" },
            { 1, "1.腹合せ" }
        };

        public ObservableCollection<ElementViewModel> ElementViewModels
        {
            get => _elementViewModels;
            set => SetProperty(ref _elementViewModels, value);
        }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public string NotificationMessage
        {
            get => _notificationMessage;
            set
            {
                SetProperty(ref _notificationMessage, value.Length < 140 ? value : $"{value.Substring(0, 140)} ...");
                NotificationBackground = _notificationMessage.Length > 0 ? "LightYellow" : "White";
            }
        }

        public string NotificationBackground
        {
            get => _notificationBackground;
            set => SetProperty(ref _notificationBackground, value);
        }

        public Visibility ErrorVisible
        {
            get => _errorVisible;
            set => SetProperty(ref _errorVisible, value);
        }
        //Stopwatch sw;
        #endregion

        #region JointList Binding
        private List<FittingModel> _fittingModels;
        private int _jointMarkIndex;
        private int? _flangeJointType;
        private int? _webJointType;
        private int? _flangeSP;
        private int? _beamDevision;
        private int? _memberInputMethod;
        private int? _haunch;
        private int? _GPType;
        private int? _hasPlating;
        private int? _useSide;
        private int _endPartDepthWidth;
        private bool _copyButtonEnabled;
        private bool _deleteButtonEnabled;
        private bool _editMarkButtonEnabled;

        private Visibility _visibleJointTab;
        public List<FittingModel> FittingModels
        {
            get => _fittingModels;
            set => SetProperty(ref _fittingModels, value);
        }
        public Visibility VisibleJointTab
        {
            get => _visibleJointTab;
            set => SetProperty(ref _visibleJointTab, value);
        }
        private Visibility _visibleMemberTab;
        public Visibility VisibleMemberTab
        {
            get => _visibleMemberTab;
            set => SetProperty(ref _visibleMemberTab, value);

        }
        public bool CopyButtonEnabled
        {
            get => _copyButtonEnabled;
            set => SetProperty(ref _copyButtonEnabled, value);

        }
        public bool DeleteButtonEnabled
        {
            get => _deleteButtonEnabled;
            set => SetProperty(ref _deleteButtonEnabled, value);

        }
        public bool EditMarkButtonEnabled
        {
            get => _editMarkButtonEnabled;
            set => SetProperty(ref _editMarkButtonEnabled, value);

        }
        public int EndPartDepthWidth
        {
            get => _endPartDepthWidth;
            set => SetProperty(ref _endPartDepthWidth, value);

        }
        #endregion

        #region MemberList Binding
        private List<MaterialModel> _materialModels;
        private bool _flangeJointTypeEnabled = false;
        private bool _webJoinTypeEnabled = false;
        private bool _flangeSPEnabled = false;
        private bool _beamDivisionEnabled = false;
        private bool _memberInputMethodEnabled = false;
        private bool _haunchEnabled = false;
        private bool _GPTypeEnabled = false;
        private bool _hasPlatingEnabled = false;
        private bool _useSideEnabled = false;
        private bool _partAngleEnabled = false;
        public List<MaterialModel> MaterialModels
        {
            get => _materialModels;
            set => SetProperty(ref _materialModels, value);

        }
        #endregion

        #region MarkTypeInput Binding
        #region Visible Binding
        public bool FlangeJointTypeEnabled
        {
            get => _flangeJointTypeEnabled;
            set => SetProperty(ref _flangeJointTypeEnabled, value);

        }

        public bool WebJointTypeEnabled
        {
            get => _webJoinTypeEnabled;
            set => SetProperty(ref _webJoinTypeEnabled, value);

        }

        public bool FlangeSPEnabled
        {
            get => _flangeSPEnabled;
            set => SetProperty(ref _flangeSPEnabled, value);

        }

        public bool BeamDivisionEnabled
        {
            get => _beamDivisionEnabled;
            set => SetProperty(ref _beamDivisionEnabled, value);

        }

        public bool MemberInputMethodEnabled
        {
            get => _memberInputMethodEnabled;
            set => SetProperty(ref _memberInputMethodEnabled, value);

        }

        public bool HaunchEnabled
        {
            get => _haunchEnabled;
            set => SetProperty(ref _haunchEnabled, value);

        }

        public bool GPTypeEnabled
        {
            get => _GPTypeEnabled;
            set => SetProperty(ref _GPTypeEnabled, value);

        }

        public bool HasPlatingEnabled
        {
            get => _hasPlatingEnabled;
            set => SetProperty(ref _hasPlatingEnabled, value);

        }

        public bool UseSideEnabled
        {
            get => _useSideEnabled;
            set => SetProperty(ref _useSideEnabled, value);

        }

        public bool PartAngleEnabled
        {
            get => _partAngleEnabled;
            set => SetProperty(ref _partAngleEnabled, value);

        }
        #endregion
        private Dictionary<int, string> _jointMarks = new Dictionary<int, string>();
        public Dictionary<int, string> JointMarks
        {
            get => _jointMarks;
            set => SetProperty(ref _jointMarks, value);
        }
        private Dictionary<int, string> _haunchs = new Dictionary<int, string>();
        public Dictionary<int, string> Haunchs
        {
            get => _haunchs;
            set => SetProperty(ref _haunchs, value);
        }
        [Required]
        public int JointMarkIndex
        {
            get => _jointMarkIndex;
            set
            {
                SetProperty(ref _jointMarkIndex, value);
                SetJointMarkFitting(value);
            }
        }

        [Required]
        public int? FlangeJointType
        {
            get => _flangeJointType;
            set
            {
                SetProperty(ref _flangeJointType, value, null, true);
                ValidateProperty(value, skip: !FlangeJointTypeEnabled);
            }
        }

        [Required]
        public int? WebJointType
        {
            get => _webJointType;
            set
            {
                SetProperty(ref _webJointType, value, null, true);
                ValidateProperty(value, skip: !WebJointTypeEnabled);
            }
        }

        [Required]
        public int? FlangeSP
        {
            get => _flangeSP;
            set
            {
                SetProperty(ref _flangeSP, value, null, true);
                ValidateProperty(value, skip: !FlangeSPEnabled);
            }
        }

        [Required]
        public int? BeamDevision
        {
            get => _beamDevision;
            set
            {
                SetProperty(ref _beamDevision, value, null, true);
                ValidateProperty(value, skip: !BeamDivisionEnabled);
            }
        }

        [Required]
        public int? MemberInputMethod
        {
            get => _memberInputMethod;
            set
            {
                SetProperty(ref _memberInputMethod, value, null, true);
                ValidateProperty(value, skip: !MemberInputMethodEnabled);
            }
        }

        [Required]
        public int? Haunch
        {
            get => _haunch;
            set
            {
                SetProperty(ref _haunch, value, null, true);
                IsAddMemberBtnActive = CanAddNewMember();
                ValidateProperty(value, skip: !HaunchEnabled);
            }
        }

        [Required]
        public int? GPType
        {
            get => _GPType;
            set
            {
                SetProperty(ref _GPType, value, null, true);
                ValidateProperty(value, skip: !GPTypeEnabled);
            }
        }

        [Required]
        public int? HasPlating
        {
            get => _hasPlating;
            set
            {
                SetProperty(ref _hasPlating, value, null, true);
                ValidateProperty(value, skip: !HasPlatingEnabled);
            }
        }

        [Required]
        public int? UseSide
        {
            get => _useSide;
            set
            {
                SetProperty(ref _useSide, value, null, true);
                ValidateProperty(value, skip: !UseSideEnabled);
            }
        }

        #endregion

        #region MemberInput Binding
        private Visibility _suggestionVisible = Visibility.Hidden;
        public Visibility SuggestionVisible
        {
            get => _suggestionVisible;
            set => SetProperty(ref _suggestionVisible, value);

        }
        private Dictionary<int, string> _dimensionSuggest;
        public Dictionary<int, string> DimensionSuggest
        {
            get => _dimensionSuggest;
            set => SetProperty(ref _dimensionSuggest, value);
        }
        private bool _isAddMemberBtnActive = false;
        public bool IsAddMemberBtnActive
        {
            get => _isAddMemberBtnActive;
            set => SetProperty(ref _isAddMemberBtnActive, value);
        }
        #endregion

        public MainScreenWindowViewModel()
        {
            _webTypeMapper = new WebTypeMapper();
            FlangeStandards = WebStandards = DataRepository.GetStandardMembersMain();
            UpdateJointListData();
            UpdateMemberListData();
            ElementViewModels = new ObservableCollection<ElementViewModel>();
            for (int i = 0; i < MaxMemebersCount; i++)
            {
                ElementViewModels.Add(new ElementViewModel()
                {
                    Id = i + 1,
                    StartIndex = 120 + (i * 10),
                    Label = $"{ElementViewModel.LabelPrefix} {i + 1}",
                    MemberEnable = false,
                    MemberVisible = i < 3 ? Visibility.Visible : Visibility.Hidden,
                    MemberHeight = i < 3 ? MemeberHeight : 0,
                    DeleteButtonVisiblity = Visibility.Hidden,
                    Dimension1 = 0,
                    Dimension2 = 0,
                    Dimension3 = 0,
                    Dimension4 = 0,
                    FlangeStandard = 0,
                    WebStandard = 0,
                    MatchType = 0,
                    EdgeDepth = 0,
                    IsElementNameEnabled = false,
                    IsFlangeStdEnabled = false,
                    IsWebStdEnabled = false,
                    IsMatchTypeEnabled = false,
                    IsEdgeDepthEnabled = false,
                    IsWidthEnabled = false,
                    IsHeightEnabled = false,
                });
            }
            UpdateProperty(nameof(EndDepthWidth));
            //UpdateProperty(nameof(EndDepthColumnVisibility));
            _totalRegistrations = 800;
            Title = GetTitle();
        }

        public void LoadedWindowCommandExecuted(object sender, EventArgs e)
        {
            _mainScreen = sender as MainScreenWindow;
        }
        public void UpdateJointListData()
        {
            var result = DataRepository.GetJointMarkDetails();
            FittingModels = result.Item1;
            JointMarks = result.Item2;
            _completedRegistrations = result.Item3;
            Title = GetTitle();

        }
        public void UpdateMemberListData()
        {
            MaterialModels = DataRepository.GetMemberLst();

        }
        public (string[], int[]) ValidateJointInput()
        {
            return DataRepository.ValidateInp();
        }
        public bool OkBtnCommandExecuted(object sender = null, RoutedEventArgs e = null)
        {
            try
            {
                UpdTmpMrkTyp();
                UpdateTempElement();
                if (!DataRepository.StartSavingProcess()) return false;
                _mainScreen.Visibility = Visibility.Hidden;
            Dt03:
                if (FlangeJointType > 0)
                {
                    Window detailScreen_03 = new DetailsScreenJOINT_G_03((FlangeType)FlangeJointType);
                    detailScreen_03.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    var result = detailScreen_03.ShowDialog();
                    if (result == true && WebJointType <= 0)
                        goto End;
                    else if (result == false)
                    {
                        BackToMainScreen();
                        return false;
                    }
                }
                if (WebJointType > 0)
                {
                    Window detailScreen_04 = new DetailsScreenJOINT_G_04((WebType)WebJointType, FlangeJointType > 0);
                    var result = detailScreen_04.ShowDialog();
                    if (result == true)
                        goto End;
                    else
                    {
                        if ((detailScreen_04 as DetailsScreenJOINT_G_04).IsBackToDt03)
                        {
                            goto Dt03;
                        }
                        BackToMainScreen();
                        return false;
                    }
                }
            End:
                if (!DataRepository.SaveJoint())
                {
                    BackToMainScreen();
                    return false;
                }
                if (_jointAction == "change" && !DataRepository.DeleteJointMrk(_jointSelectedAction))
                {
                    string errMsg = $"Error Delete Joint after Change {_jointSelectedAction}";
                    _mainScreen.ShowWarning(errMsg, "エラー");
                    Log.Logger.Error(errMsg);
                }
                else
                    _jointSelectedAction = TbxJointMark.Text;
                UpdateJointListData();
                UpdateMemberListData();
                ResetJointMarkType();
                ResetJointMember();
                _currentSelectedJoint = "";
                _jointAction = "";
                System.Windows.Application.Current.MainWindow = _mainScreen;
                _mainScreen.Visibility = Visibility.Visible;
                return true;
                //_mainScreen.UpdateLayout();
                //TbxJointMark.Focus();
                //string[] actions = new[] { "copy", "change", "update" };
                //if (actions.Contains(_jointAction))
                //{
                //    JointMarkIndex = JointMarks.Values.ToList().IndexOf(_jointSelectedAction);
                //}
            }
            catch (System.Exception ex)
            {
                Log.Logger.Error(ex, "Error on opening DetailsWindow");
                return false;
            }
        }
        private void BackToMainScreen()
        {
            System.Windows.Application.Current.MainWindow = _mainScreen;
            _mainScreen.Visibility = Visibility.Visible;
            _mainScreen.UpdateLayout();
            _mainScreen.Focus();
            TbxJointMark.Focus();
        }
        public void CloseBtnCommandExecuted(object sender = null, RoutedEventArgs e = null)
        {
            try
            {
                Environment.Exit(0);
                Log.Logger.Information("MainScreenWindownViewModel.cs MainScreenWindownViewModel:CloseBtnCommandExecuted , Message : Close main window");
            }
            catch (System.Exception ex)
            {
                Log.Logger.Error(ex, "Error on Closing MainWindow");
            }
        }
        private string GetTitle()
        {
            string cap = DataRepository.GetCaption();
            return cap.Length > 0 ? cap : string.Format(TitleTemplate, _constructionName, _completedRegistrations, _totalRegistrations);
        }

        #region JointList functions

        public bool CopyCommandExecuted(string jmk)
        {
            try
            {
                var dialog = new CopyDialog(jmk, JointMarks.Select(x => x.Value).ToList(), "copy");
                var result = dialog.ShowDialog();
                string newMark = dialog.ViewModel.NewMarkName;
                if (result == true)
                {
                    TbxJointMark.Text = newMark;
                    _jointSelectedAction = newMark;
                    _jointAction = "copy";
                    return true;
                }
                else
                {
                    _jointAction = "";
                    _jointSelectedAction = "";
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return false;
            }
        }
        public bool DeleteCommandExecuted(string mrk)
        {
            try
            {
                return DataRepository.DeleteJointMrk(mrk);
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return false;
            }
        }
        public bool ChangeCommandExecuted(string jmk)
        {
            try
            {
                var dialog = new CopyDialog(jmk, JointMarks.Select(x => x.Value).ToList(), "change");
                var result = dialog.ShowDialog();
                string newMark = dialog.ViewModel.NewMarkName;
                if (result == true)
                {
                    TbxJointMark.Text = newMark;
                    _jointSelectedAction = jmk;
                    _jointAction = "change";
                    return true;
                }
                else
                {
                    _jointAction = "";
                    _jointSelectedAction = "";
                    return false;
                }

            }
            catch (Exception ex)
            {
                ex.HandleException();
                return false;
            }
        }
        public void OfficialMarkSetupCommandExecuted(object sender = null, RoutedEventArgs e = null)
        {
            try
            {
                var dialog = new OfficialMarkReg.OfficialMarkRegDialog(DataRepository.jig);
                Application.Current.MainWindow = dialog;
                var res = dialog.ShowDialog();
                System.Windows.Application.Current.MainWindow = _mainScreen;
                if (res is true)
                {
                    UpdateJointListData();
                }
                TbxJointMark.Focus();
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        public void BeamJointMasterCallCommandExecuted(string newMark)
        {
            try
            {
                IsAddMemberBtnActive = CanAddNewMember();
                UpdateProperty(nameof(EndDepthWidth));
                //UpdateProperty(nameof(EndDepthColumnVisibility));
                TbxJointMark.Text = newMark;
                _jointAction = "MasterCall";
                _jointSelectedAction = newMark;
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        #endregion

        #region MemberList functions
        public void DisplayMethodCommandExecuted(object sender = null, RoutedEventArgs e = null)
        {
            try
            {
                var dialog = new DisplayMethodDialog();
                Application.Current.MainWindow = dialog;
                var res = dialog.ShowDialog();
                Application.Current.MainWindow = _mainScreen;
                if (res is true)
                {
                    UpdateMemberListData();
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        public void OnMaterialModelSelected(int selectedIndex, string materialMark)
        {
            try
            {
                DataRepository.SelectJointMemberDat(selectedIndex + 1, materialMark);
                OnJointMarkIndexChanged(materialMark);
                _jointAction = "material";
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        #endregion

        #region MarkTypeInput functions
        public bool UpdateMarkTypeOnJointMark()
        {
            try
            {
                MrkTypDat mrkTyp = DataRepository.GetMrkTyp();

                TbxJointMark.Text = mrkTyp._jmk;

                WebJointTypes = _webTypeMapper.GetWebJointTypes(mrkTyp._ftyp, mrkTyp._ftyp_act == 1);

                Haunchs = _webTypeMapper.GetHaunchs(mrkTyp._bzinp, mrkTyp._bzinp_act == 1);

                if (mrkTyp._ftyp == 4 || mrkTyp._ftyp == 8) WebJointTypeEnabled = false;

                FlangeJointTypeEnabled = mrkTyp._ftyp_act == 1;

                WebJointTypeEnabled = mrkTyp._wtyp_act == 1;

                FlangeSPEnabled = mrkTyp._undflg_act == 1;

                GPTypeEnabled = mrkTyp._gpyoto_act == 1;

                BeamDivisionEnabled = mrkTyp._jgkbn_act == 1;

                MemberInputMethodEnabled = mrkTyp._bzinp_act == 1;

                HaunchEnabled = mrkTyp._hanch_act == 1;

                HasPlatingEnabled = mrkTyp._mekki_act == 1;

                UseSideEnabled = mrkTyp._yokoinp_act == 1;

                PartAngleEnabled = mrkTyp._ogami_act == 1;

                FlangeJointType = mrkTyp._ftyp > 8 ? 0 : mrkTyp._ftyp;

                FlangeSP = mrkTyp._undflg > SPTypes.Keys.Max() ? 0 : mrkTyp._undflg;

                BeamDevision = mrkTyp._jgkbn > BeamDivisions.Keys.Max() ? 0 : mrkTyp._jgkbn;

                Haunch = mrkTyp._hanch > Haunchs.Keys.Max() ? 0 : mrkTyp._hanch;

                MemberInputMethod = mrkTyp._bzinp > MemberInputMethods.Keys.Max() ? 0 : mrkTyp._bzinp;

                WebJointType = mrkTyp._wtyp > WebJointTypes.Keys.Max() ? 0 : mrkTyp._wtyp;

                GPType = mrkTyp._gpyoto > GPTypes.Keys.Max() ? 0 : mrkTyp._gpyoto;

                HasPlating = mrkTyp._mekki > HasPlatings.Keys.Max() ? 0 : mrkTyp._mekki;

                UseSide = mrkTyp._yokoinp > Existance.Keys.Max() ? 0 : mrkTyp._yokoinp;

            }
            catch (Exception ex)
            {
                ex.HandleException();
                return false;
            }
            return true;
        }

        public bool IsSelectedJoint()
        {
            var mrkTyp = DataRepository.GetMrkTyp();
            return mrkTyp._jmk.Length != 0;
        }

        public void ResetJointMarkType()
        {
            try
            {
                WebJointTypes = _webTypeMapper.GetWebJointTypes(0, false);
                Haunchs = _webTypeMapper.GetHaunchs(0, false);
                TbxJointMark.Text = "";
                FlangeJointTypeEnabled = false;
                WebJointTypeEnabled = false;
                FlangeSPEnabled = false;
                GPTypeEnabled = false;
                BeamDivisionEnabled = false;
                MemberInputMethodEnabled = false;
                HaunchEnabled = false;
                HasPlatingEnabled = false;
                UseSideEnabled = false;
                PartAngleEnabled = false;
                JointMarkIndex = -1;
                FlangeJointType = 0;
                FlangeSP = 0;
                BeamDevision = 0;
                Haunch = 0;
                MemberInputMethod = 0;
                WebJointType = 0;
                GPType = 0;
                HasPlating = 0;
                UseSide = 0;
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        public void ResetJointMember()
        {
            try
            {
                for (int i = 0; i < showedMember; i++)
                {
                    if (ElementViewModels[i].MemberEnable)
                    {
                        HideElement(i);
                        if(i < 3)
                        {
                            ElementViewModels[i].MemberVisible = Visibility.Visible;
                            ElementViewModels[i].MemberHeight = MemeberHeight;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        public void UpdTmpMrkTyp()
        {
            MrkTypDat tmpMrkTyp = new MrkTypDat()
            {
                _jmk = TbxJointMark.Text,
                _ftyp = FlangeJointType ?? 0,
                _undflg = FlangeSP ?? 0,
                _jgkbn = BeamDevision ?? 0,
                _bzinp = MemberInputMethod ?? 0,
                _hanch = Haunch ?? 0,
                _wtyp = WebJointType ?? 0,
                _gpyoto = GPType ?? 0,
                _mekki = HasPlating ?? 0,
                _yokoinp = UseSide ?? 0
            };

            DataRepository.UpdateTmpMrkTyp(tmpMrkTyp);
            _mainScreen.UpdateDrawWin();
            UpdateMarkTypeOnJointMark();
        }

        public void PartAngleCommandExecuted(object sender = null, RoutedEventArgs e = null)
        {
            try
            {

                var dialog = new JoinPartAngleDialog();
                System.Windows.Application.Current.MainWindow = dialog;
                dialog.ShowDialog();
                System.Windows.Application.Current.MainWindow = _mainScreen;
                if (dialog.DialogResult is true)
                {
                    //do action
                }
                TbxJointMark.Focus();
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        /// <summary>
        /// SetJointMarkFitting
        /// </summary>
        /// <param name="selectedIndex">JointMark Index</param>
        /// <param name="inputjmk">JointMark Text Input</param>
        /// <param name="isLostFocus">Is Enter Click</param>
        /// <returns></returns>
        public void SetJointMarkFitting(int selectedIndex)
        {
            try
            {
                MarkTypeValid = true;
                MemberValid = true;
                if (selectedIndex >= 0)
                {
                    JointMarks.TryGetValue(selectedIndex, out string changeJmk);
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        public void OnJointMarkIndexChanged(string value, bool isCopy = false)
        {
            try
            {
                if (value != null && !isCopy)
                {
                    UpdTmpMrkTyp();
                    UpdateListElement();
                    IsAddMemberBtnActive = CanAddNewMember();
                    UpdateProperty(nameof(EndDepthWidth));
                    //UpdateProperty(nameof(EndDepthColumnVisibility));
                }
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        public bool SetJointMarkTextChange(string jmk)
        {
            try
            {
                MarkTypeValid = true;
                MemberValid = true;
                if (jmk.Length > 0)
                {
                    if (DataRepository.SelectJoint(jmk))
                    {
                        _currentSelectedJoint = jmk;
                        _jointAction = "update";
                        UpdTmpMrkTyp();
                        UpdateListElement();
                        IsAddMemberBtnActive = CanAddNewMember();
                        UpdateProperty(nameof(EndDepthWidth));
                        //UpdateProperty(nameof(EndDepthColumnVisibility));

                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return false;
            }
        }

        public void UnselectJoint()
        {
            try
            {
                DataRepository.UnselectJoint();
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return;
            }
        }

        #endregion

        #region MemberInput functions
        private void PutDataToEle(int index, MemberDat element)
        {
            HandleMemberControlEnabled(element._fcd, index);
            ElementViewModels[index].MemberEnable = true;
            ElementViewModels[index].MemberVisible = Visibility.Visible;
            ElementViewModels[index].MemberHeight = MemeberHeight;
            if (index >= 3) ElementViewModels[index].DeleteButtonVisiblity = Visibility.Visible;
            ElementViewModels[index].GradeCodeId = element._fcd;
            ElementViewModels[index].Dimension1 = element._fbz0;
            ElementViewModels[index].Dimension2 = element._fbz1;
            ElementViewModels[index].Dimension3 = element._fbz2;
            ElementViewModels[index].Dimension4 = element._fbz3;
            //FlangeStd
            ElementViewModels[index].FlangeStandard = element._fkk;
            //WebStd
            ElementViewModels[index].WebStandard = element._wkk;
            //MatchType
            ElementViewModels[index].MatchType = element._seawa;
            //EdgeDepth
            ElementViewModels[index].EdgeDepth = element._tdep;
            //Width
            ElementViewModels[index].Width = element._haba;
            //Height
            ElementViewModels[index].Height = element._taka;
        }

        public void UpdateListElement()
        {
            try
            {
                List<MemberDat> data = DataRepository.GetMembers().Where((x, index) => x._fcd != 0 || index < 3).ToList();
                Log.Logger.Information("MainScreenWindownViewModel.cs MainScreenWindownViewModel:UpdateListElement , Message : Start UpdateListElement");
                showedMember = data.Count;
                if (MemberInputMethod == 0)
                {
                    for (int i = 0; i < data.Count; i++)
                    {
                        ElementViewModels[i].Label = $"{ElementViewModel.LabelPrefix} {i + 1}";
                        PutDataToEle(i, data[i]);
                    }
                }
                else if (MemberInputMethod == 1)
                {
                    ElementViewModels[0].Label = $"中央部材";
                    ElementViewModels[1].Label = $"端部部材";
                    ElementViewModels[2].Label = $"合わせ方";
                    for (int i = 0; i < 2; i++)
                    {
                        PutDataToEle(i, data[i]);
                    }
                    MemberDat element2 = data.ElementAt(2);
                    ElementViewModels[2].MemberEnable = true;
                    ElementViewModels[2].MatchType = element2._seawa;
                }

                for (int i = showedMember; i < MaxMemebersCount; i++)
                {
                    ElementViewModels[i].MemberVisible = Visibility.Hidden;
                    ElementViewModels[i].MemberHeight = 0;
                }
                IsAddMemberBtnActive = CanAddNewMember();
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }

            Log.Logger.Information("MainScreenWindownViewModel.cs MainScreenWindownViewModel:UpdateListElement , Message : Done UpdateListElement");
        }
        public void HandleMemberControlEnabled(int key, int index)
        {
            const int INPUT_WIDTH = 70;
            try
            {
                WidthColumnWidth = 0;
                HeightColumnWidth = 0;
                ElementViewModels[index].WidthTextboxWidth = 0;
                ElementViewModels[index].HeightTextboxWidth = 0;
                ElementViewModels[index].IsEdgeDepthEnabled = IsEndDepthVisiable();
                switch (key)
                {
                    case 0:
                        ResetElementValue(index);
                        break;
                    case 1:
                    case 19:
                        ElementViewModels[index].IsElementNameEnabled = true;
                        ElementViewModels[index].IsFlangeStdEnabled = true;
                        ElementViewModels[index].IsWebStdEnabled = false;
                        ElementViewModels[index].IsMatchTypeEnabled = false;

                        break;
                    case 2:
                    case 3:
                    case 5:
                    case 9:
                    case 10:
                    case 50:
                    case 51:
                    case 52:
                        ElementViewModels[index].IsElementNameEnabled = true;
                        ElementViewModels[index].IsFlangeStdEnabled = true;
                        ElementViewModels[index].IsWebStdEnabled = false;
                        ElementViewModels[index].IsMatchTypeEnabled = false;

                        break;
                    case 53:
                        ElementViewModels[index].IsElementNameEnabled = false;
                        ElementViewModels[index].IsFlangeStdEnabled = true;
                        ElementViewModels[index].IsWebStdEnabled = false;
                        ElementViewModels[index].IsMatchTypeEnabled = false;

                        break;
                    case 90:
                    case 94:
                        ElementViewModels[index].IsElementNameEnabled = true;
                        ElementViewModels[index].IsFlangeStdEnabled = true;
                        ElementViewModels[index].IsWebStdEnabled = true;
                        ElementViewModels[index].IsMatchTypeEnabled = false;

                        break;
                    case 4:
                    case 11:
                    case 16:
                    case 23:
                        ElementViewModels[index].IsElementNameEnabled = true;
                        ElementViewModels[index].IsFlangeStdEnabled = true;
                        ElementViewModels[index].IsWebStdEnabled = false;
                        ElementViewModels[index].IsMatchTypeEnabled = false;

                        break;
                    case 205:
                    case 210:
                        ElementViewModels[index].IsElementNameEnabled = true;
                        ElementViewModels[index].IsFlangeStdEnabled = true;
                        ElementViewModels[index].IsWebStdEnabled = false;
                        ElementViewModels[index].IsMatchTypeEnabled = true;

                        break;
                    case 310:
                        WidthColumnWidth = INPUT_WIDTH;
                        ElementViewModels[index].WidthTextboxWidth = INPUT_WIDTH;
                        ElementViewModels[index].IsElementNameEnabled = true;
                        ElementViewModels[index].IsFlangeStdEnabled = true;
                        ElementViewModels[index].IsWebStdEnabled = false;
                        ElementViewModels[index].IsMatchTypeEnabled = true;

                        break;
                    case 204:
                    case 211:
                        ElementViewModels[index].IsElementNameEnabled = true;
                        ElementViewModels[index].IsFlangeStdEnabled = true;
                        ElementViewModels[index].IsWebStdEnabled = false;
                        ElementViewModels[index].IsMatchTypeEnabled = true;

                        break;
                    case 311:
                        WidthColumnWidth = INPUT_WIDTH;
                        ElementViewModels[index].WidthTextboxWidth = INPUT_WIDTH;
                        ElementViewModels[index].IsElementNameEnabled = true;
                        ElementViewModels[index].IsFlangeStdEnabled = true;
                        ElementViewModels[index].IsWebStdEnabled = false;
                        ElementViewModels[index].IsMatchTypeEnabled = true;

                        break;
                    case 404:
                        WidthColumnWidth = INPUT_WIDTH;
                        HeightColumnWidth = INPUT_WIDTH;
                        ElementViewModels[index].WidthTextboxWidth = INPUT_WIDTH;
                        ElementViewModels[index].HeightTextboxWidth = INPUT_WIDTH;
                        ElementViewModels[index].IsElementNameEnabled = true;
                        ElementViewModels[index].IsFlangeStdEnabled = true;
                        ElementViewModels[index].IsWebStdEnabled = false;
                        ElementViewModels[index].IsMatchTypeEnabled = false;
                        ElementViewModels[index].IsEdgeDepthEnabled = false;
                        break;
                    default:
                        break;
                }
                if (ElementViewModels[index].GradeCodeId == null)
                    ElementViewModels[index].GradeCodeId = 0;
                if (ElementViewModels[index].FlangeStandard == null)
                    ElementViewModels[index].FlangeStandard = 0;
                if (ElementViewModels[index].WebStandard == null)
                    ElementViewModels[index].WebStandard = 0;
                if (ElementViewModels[index].MatchType == null)
                    ElementViewModels[index].MatchType = 0;
                if (ElementViewModels[index].EdgeDepth == null)
                    ElementViewModels[index].EdgeDepth = 0;
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        public void UpdateTempElement()
        {
            try
            {
                //if (CanUpdateMember is true)
                //{
                var elm = ElementViewModels;
                MemberDat[] mds = new MemberDat[MaxMemebersCount];
                for (int i = 0; i < showedMember; i++)
                {
                    double.TryParse(Dms[i].Txt1.Text, out double Dms1);
                    double.TryParse(Dms[i].Txt2.Text, out double Dms2);
                    double.TryParse(Dms[i].Txt3.Text, out double Dms3);
                    double.TryParse(Dms[i].Txt4.Text, out double Dms4);
                    double.TryParse(TbxtDept[i].Text, out double tDepth);
                    double.TryParse(TbxWidth[i].Text, out double width);
                    double.TryParse(TbxHeight[i].Text, out double height);
                    mds[i] = new MemberDat()
                    {
                        _fcd = elm[i].GradeCodeId ?? 0,
                        _fbz0 = Dms1,
                        _fbz1 = Dms2,
                        _fbz2 = Dms3,
                        _fbz3 = Dms4,
                        _fkk = elm[i].FlangeStandard ?? 0,
                        _wkk = elm[i].WebStandard ?? 0,
                        _seawa = elm[i].MatchType ?? 0,
                        _tdep = tDepth,
                        _haba = width,
                        _taka = height,
                    };
                }
                DataRepository.UpdateTmpMembers(mds);
                _mainScreen.UpdateDrawWin();
                IsAddMemberBtnActive = CanAddNewMember();
                //}
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        public bool IsCheckPassed { get; set; }
        public int EndDepthWidth => IsEndDepthVisiable() ? 75 : 0;

        private int _widthColumnWidth;
        public int WidthColumnWidth
        {
            get => _widthColumnWidth;
            set
            {
                SetProperty(ref _widthColumnWidth, value);
            }
        }
        private int _heightColumnWidth;
        public int HeightColumnWidth
        {
            get => _heightColumnWidth;
            set
            {
                SetProperty(ref _heightColumnWidth, value);
            }
        }

        private WebTypeMapper _webTypeMapper { get; }

        public void AddNewMember()
        {
            try
            {
                var add = ElementViewModels[showedMember];
                add.MemberEnable = true;
                add.MemberVisible = Visibility.Visible;
                add.MemberHeight = MemeberHeight;
                add.DeleteButtonVisiblity = Visibility.Visible;
                add.GradeCodeId = 0;
                showedMember += 1;
                IsAddMemberBtnActive = CanAddNewMember();
                //UpdateProperty(nameof(EndDepthColumnVisibility));
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        public void DeleteMember(int id)
        {
            try
            {

                if (id < showedMember - 1)
                {
                    for (int i = id; i < showedMember - 1; i++)
                    {
                        if (i >= 3) ElementViewModels[i].DeleteButtonVisiblity = Visibility.Visible;
                        ElementViewModels[i].GradeCodeId = ElementViewModels[i + 1].GradeCodeId;
                        ElementViewModels[i].Dimension1 = ElementViewModels[i + 1].Dimension1;
                        ElementViewModels[i].Dimension2 = ElementViewModels[i + 1].Dimension2;
                        ElementViewModels[i].Dimension3 = ElementViewModels[i + 1].Dimension3;
                        ElementViewModels[i].Dimension4 = ElementViewModels[i + 1].Dimension4;
                        //FlangeStd
                        ElementViewModels[i].FlangeStandard = ElementViewModels[i + 1].FlangeStandard;
                        //WebStd
                        ElementViewModels[i].WebStandard = ElementViewModels[i + 1].WebStandard;
                        //MatchType
                        ElementViewModels[i].MatchType = ElementViewModels[i + 1].MatchType;
                        //EdgeDepth
                        ElementViewModels[i].EdgeDepth = ElementViewModels[i + 1].EdgeDepth;
                        //Width
                        ElementViewModels[i].Width = ElementViewModels[i + 1].Width;
                        //Height
                        ElementViewModels[i].Height = ElementViewModels[i + 1].Height;
                        HandleMemberControlEnabled(ElementViewModels[i + 1].GradeCodeId ?? 0, i);
                    }
                }
                var delete = ElementViewModels[showedMember - 1];
                delete.MemberEnable = false;
                delete.MemberVisible = Visibility.Hidden;
                delete.MemberHeight = 0;
                delete.Dimension1 = 0;
                delete.Dimension2 = 0;
                delete.Dimension3 = 0;
                delete.Dimension4 = 0;
                delete.FlangeStandard = 0;
                delete.GradeCodeId = 0;
                delete.WebStandard = 0;
                delete.MatchType = 0;
                delete.Width = 0;
                delete.Height = 0;
                showedMember -= 1;
                IsAddMemberBtnActive = CanAddNewMember();
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        public void CheckChanged(bool val)
        {
            IsCheckPassed = val;
        }

        private void ResetElementValue(int index)
        {
            ElementViewModels[index].IsElementNameEnabled = false;
            ElementViewModels[index].IsFlangeStdEnabled = false;
            ElementViewModels[index].IsWebStdEnabled = false;
            ElementViewModels[index].IsMatchTypeEnabled = false;
            ElementViewModels[index].IsEdgeDepthEnabled = false;
            ElementViewModels[index].Dimension1 = 0;
            ElementViewModels[index].Dimension2 = 0;
            ElementViewModels[index].Dimension3 = 0;
            ElementViewModels[index].Dimension4 = 0;
            ElementViewModels[index].FlangeStandard = 0;
            ElementViewModels[index].GradeCodeId = 0;
            ElementViewModels[index].WebStandard = 0;
            ElementViewModels[index].MatchType = 0;
            ElementViewModels[index].EdgeDepth = 0;
            ElementViewModels[index].Width = 0;
            ElementViewModels[index].Height = 0;
        }
        private void HideElement(int index)
        {
            ElementViewModels[index].MemberEnable = false;
            ElementViewModels[index].MemberVisible = Visibility.Hidden;
            ElementViewModels[index].MemberHeight = 0;
            ElementViewModels[index].DeleteButtonVisiblity = Visibility.Hidden;
            ResetElementValue(index);
        }

        public void LimitNewMembers()
        {
            try
            {
                if (IsEndDepthVisiable())
                {
                    for (int i = 3; i < MaxMemebersCount; i++)
                    {
                        HideElement(i);
                    };
                }
                UpdateProperty(nameof(EndDepthWidth));
                //UpdateProperty(nameof(EndDepthColumnVisibility));
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }

        public bool CanAddNewMember()
        {
            return showedMember < MaxMemebersCount
                && ElementViewModels.Where(x => x.MemberHeight > 0 && x.GradeCodeId <= 0).ToList().Count == 0
                && !IsEndDepthVisiable();
        }

        public bool IsEndDepthVisiable()
        {
            return MemberInputMethod == 0 && Haunch != 0;
        }

        public bool HandleSelectSuggestion(int tag)
        {
            var res = DataRepository.GetSuggestion(ElementViewModels[tag].GradeCodeId ?? -1, tag);
            if (res != null && res.Count > 0)
            {
                DimensionSuggest = res;
                SuggestionVisible = Visibility.Visible;
                return true;
            }
            return false;
        }

        internal void HandleJointRowSelected(int itemsCount)
        {
            CopyButtonEnabled = itemsCount == 1;
            DeleteButtonEnabled = itemsCount >= 1;
            EditMarkButtonEnabled = itemsCount == 1;
        }
        #endregion
    }
}

using System.Collections.Generic;
using System.Windows;

namespace JOINT_G.ViewModels
{
    /// <summary>
    /// 部材のビューモデル。
    /// </summary>
    public class ElementViewModel : ViewModelBase
    {
        public readonly static string LabelPrefix = "部材";

        private bool _memberEnable = false;
        private Visibility _memberVisible;
        private bool _isElementNameEnabled;
        private bool _isFlangeStdEnabled;
        private bool _isWebStdEnabled;
        private bool _isMatchTypeEnabled;
        private bool _isEdgeDepthEnabled;
        private bool _isWidthEnabled;
        private bool _isHeightEnabled;
        private string _label;

        public bool IsElementNameEnabled
        {
            get => _isElementNameEnabled;
            set => SetProperty(ref _isElementNameEnabled, value);
        }
        public bool IsFlangeStdEnabled
        {
            get => _isFlangeStdEnabled;
            set => SetProperty(ref _isFlangeStdEnabled, value);
        }
        public bool IsWebStdEnabled
        {
            get => _isWebStdEnabled;
            set => SetProperty(ref _isWebStdEnabled, value);
        }
        public bool IsMatchTypeEnabled
        {
            get => _isMatchTypeEnabled;
            set => SetProperty(ref _isMatchTypeEnabled, value);
        }
        public bool IsEdgeDepthEnabled
        {
            get => _isEdgeDepthEnabled;
            set => SetProperty(ref _isEdgeDepthEnabled, value);
        }
        public bool IsWidthEnabled
        {
            get => _isWidthEnabled;
            set => SetProperty(ref _isWidthEnabled, value);
        }
        public bool IsHeightEnabled
        {
            get => _isHeightEnabled;
            set => SetProperty(ref _isHeightEnabled, value);
        }

        private int _widthTextboxWidth;
        public int WidthTextboxWidth
        {
            get => _widthTextboxWidth;
            set
            {
                SetProperty(ref _widthTextboxWidth, value);
                IsWidthEnabled = value > 0;
            }
        }
        private int _heightTextboxWidth;
        public int HeightTextboxWidth
        {
            get => _heightTextboxWidth;
            set
            {
                SetProperty(ref _heightTextboxWidth, value);
                IsHeightEnabled = value > 0;
            }
        }
        private Visibility _deleteButtonVisiblity;
        public Visibility DeleteButtonVisiblity
        {
            get => _deleteButtonVisiblity;
            set => SetProperty(ref _deleteButtonVisiblity, value);

        }
        public Visibility Dimension3Visible => Visibility.Visible;

        public Visibility Dimension4Visible => Visibility.Visible;
        private int _id;
        private int _memberHeight;
        private string _elementName;
        private int? _flangeCodeId;
        private double? _dimension1;
        private double? _dimension2;
        private double? _dimension3;
        private double? _dimension4;
        private int? _flangeStandard;
        private int? _webStandard;
        private int? _matchType;
        private double? _edgeDepth;
        private double? _width;
        private double? _height;
        public ElementViewModel()
        {
        }
        public string Label
        {
            get => _label;
            set => SetProperty(ref _label, value);
        }

        public bool MemberEnable
        {
            get => _memberEnable;
            set => SetProperty(ref _memberEnable, value);

        }
        public Visibility MemberVisible
        {
            get => _memberVisible;
            set => SetProperty(ref _memberVisible, value);
        }
        public int MemberHeight
        {
            get => _memberHeight;
            set => SetProperty(ref _memberHeight, value);
        }
        public int Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }
        public int? GradeCodeId
        {
            get => _flangeCodeId;
            set => SetProperty(ref _flangeCodeId, value, null, true);
        }
        public string ElementName
        {
            get => _elementName;
            set => SetProperty(ref _elementName, value, null, true);
        }
        public double? Dimension1
        {
            get => _dimension1;
            set => SetProperty(ref _dimension1, value);
        }
        public double? Dimension2
        {
            get => _dimension2;
            set => SetProperty(ref _dimension2, value);
        }
        public double? Dimension3
        {
            get => _dimension3;
            set => SetProperty(ref _dimension3, value);
        }
        public double? Dimension4
        {
            get => _dimension4;
            set => SetProperty(ref _dimension4, value);
        }

        public int? FlangeStandard
        {
            get => _flangeStandard;
            set => SetProperty(ref _flangeStandard, value is not null or not 0 ? value : _flangeStandard);
        }
        public int? WebStandard
        {
            get => _webStandard;
            set => SetProperty(ref _webStandard, value is not null or not 0 ? value : _webStandard);
        }
        public int? MatchType
        {
            get => _matchType;
            set => SetProperty(ref _matchType, value is not null or not 0 ? value : _matchType);
        }
        public double? EdgeDepth
        {
            get => _edgeDepth;
            set => SetProperty(ref _edgeDepth, value);
        }

        public double? Width
        {
            get => _width;
            set => SetProperty(ref _width, value);
        }

        public double? Height
        {
            get => _height;
            set => SetProperty(ref _height, value);
        }
        #region TabIndex Properties

        public int StartIndex { get; set; }

        public int GradeCodeIndex => StartIndex + 1;

        public int ElementNameIndex => StartIndex + 2;

        public int FlangeStandardIndex => StartIndex + 6;

        public int WebStandardIndex => StartIndex + 7;

        public int MatchTypeIndex => StartIndex + 8;

        public int EndDepthIndex => StartIndex + 9;

        public int WidthIndex => StartIndex + 10;

        public int HeightIndex => StartIndex + 11;

        #endregion

    }
}

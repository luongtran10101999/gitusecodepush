using JOINT_G.Views.Dialogs;
using System;
using System.ComponentModel.DataAnnotations;
using System.Windows;

namespace JOINT_G.ViewModels.Dialogs
{
    /// <summary>
    /// JOINT-G-01-DIACOPY：コピーダイアログのビューモデル。
    /// </summary>
    public class CopyDialogViewModel : BaseDialogViewModel
    {
        private string _newMarkName;
        private string _jointMarkEnterLabel = "JOINTマークを設定して下さい。";
        private Visibility _jointMarkEnterVisible;
        private Visibility _oldMarkVisible;
        private bool _okButtonEnabled;
        private CopyDialog _copyDialog;
        #region Data Binding
        private bool? dialogResult;
        public string UserEnteredData { get; set; }
        public bool IsCheckPassed { get; set; }

        private const string CopyTitle = "継手データコピー";
        private const string CopyOldMarkLabel = "コピー元マーク：";
        private const string CopyNewMarkLabel = "コピー先マーク：";

        private const string ChangeTitle = "継手マーク変更";
        private const string ChangeOldMarkLabel = "変更前マーク：";
        private const string ChangeNewMarkLabel = "変更後マーク：";
        private const string IputNewMarkLabel = "JOINTマーク：";
        public string Title { get; }
        public string JointMarkEnterLabel
        {
            get => _jointMarkEnterLabel;
            set
            {
                _jointMarkEnterLabel = value;
                UpdateProperty();
            }
        }
        public Visibility JointMarkEnterVisible
        {
            get => _jointMarkEnterVisible;
            set
            {
                _jointMarkEnterVisible = value;
                UpdateProperty();
            }
        }
        public Visibility OldMarkVisible
        {
            get => _oldMarkVisible;
            set
            {
                _oldMarkVisible = value;
                UpdateProperty();
            }
        }
        public string OldMarkLabel { get; }

        public string NewMarkLabel { get; }

        public string OldMarkName { get; set; }

        [Required]
        public string NewMarkName
        {
            get => _newMarkName;
            set
            {
                _newMarkName = value;
                if (value != null)
                {
                    OkButtonEnabled = true;
                }
                UpdateProperty();
                ValidateProperty(value);
            }
        }

        public bool OkButtonEnabled
        {
            get => _okButtonEnabled;
            set
            {
                _okButtonEnabled = value;
                UpdateProperty();
            }
        }
        public bool? DialogResult
        {
            get => dialogResult;
            set
            {
                dialogResult = value;
                UpdateProperty();
            }
        }
        #endregion

        public CopyDialogViewModel(string oldMarkName, string type)
        {
            OldMarkName = oldMarkName;
            JointMarkEnterVisible = type == "edit" ? Visibility.Visible : Visibility.Hidden;
            OldMarkVisible = type == "copy" || type == "change" ? Visibility.Visible : Visibility.Hidden;
            Title = type == "copy" ? CopyTitle : ChangeTitle;
            OldMarkLabel = type == "copy" ? CopyOldMarkLabel : type == "change" ? ChangeOldMarkLabel : "";
            NewMarkLabel = type == "copy" ? CopyNewMarkLabel : type == "change" ? ChangeNewMarkLabel : IputNewMarkLabel;
            if (type == "edit") OkButtonEnabled = true;
        }
        public void LoadedWindowCommandExecuted(object sender, EventArgs e)
        {
            _copyDialog = sender as CopyDialog;
        }
        public void CheckChanged(bool val)
        {
            IsCheckPassed = val;
            OkButtonEnabled = val;
            UpdateProperty(nameof(OkButtonEnabled));
        }
    }
}

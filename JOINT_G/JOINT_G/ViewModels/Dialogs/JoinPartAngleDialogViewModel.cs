using JOINT_G.Repository;
using newfast;
using System;
using System.Collections.Generic;

namespace JOINT_G.ViewModels.Dialogs
{
    /// <summary>
    /// JOINT-G-01-OGAMIBUF¶qÝÌr[fB
    /// </summary>
    public class JoinPartAngleDialogViewModel : BaseDialogViewModel
    {
        private int _shapeValue;
        private int _gradientInputMethodValue;
        private double _frequency;
        private double _tan1;
        private double _tan2;
        private bool _okButtonEnabled;

        private bool _tan1Enabled;
        private bool _tan2Enabled;
        private bool _gradientInputMethodEnabled;
        private bool _frequencyEnabled;
        public JoinPartAngleDialogViewModel()
        {

            Shapes = new() { { 0, "0.¼" }, { 1, "1.R^" } };
            GradientInputMethods = new() { { 0, "0.x" }, { 1, "1.s`m" } };



            _okButtonEnabled = true;
            var result = DataRepository.GetOgamibu();

            Frequency = result._dosu;
            Tan1 = result._tan1;
            Tan2 = result._tan2;
            ShapeValue = result._spkj;
            GradientInputMethodValue = result._koubai;


        }
        public Dictionary<int, string> Shapes { get; set; }
        public Dictionary<int, string> GradientInputMethods { get; set; }


        public int ShapeValue
        {
            get => _shapeValue;
            set
            {
                _shapeValue = value;
                UpdateProperty();
            }
        }

        public int GradientInputMethodValue
        {
            get => _gradientInputMethodValue;
            set
            {
                _gradientInputMethodValue = value;
                UpdateProperty();
            }
        }


        public double Frequency
        {
            get => _frequency;
            set
            {
                _frequency = value;
                UpdateProperty();
            }
        }


        public double Tan1
        {
            get => _tan1;
            set
            {
                _tan1 = value;
                UpdateProperty();
            }
        }


        public double Tan2
        {
            get => _tan2;
            set
            {
                _tan2 = value;
                UpdateProperty();

            }
        }

        public bool OkButtonEnabled
        {
            get => _okButtonEnabled;
            set
            {
                _okButtonEnabled = value;
                UpdateProperty();
            }
        }

        public bool Tan1Enabled
        {
            get => _tan1Enabled;
            set
            {
                _tan1Enabled = value;
                UpdateProperty();
            }
        }
        public bool Tan2Enabled
        {
            get => _tan2Enabled;
            set
            {
                _tan2Enabled = value;
                UpdateProperty();
            }
        }

        public bool GradientInputMethodEnabled
        {
            get => _gradientInputMethodEnabled;
            set
            {
                _gradientInputMethodEnabled = value;
                UpdateProperty();
            }
        }

        public bool FrequencyEnabled
        {
            get => _frequencyEnabled;
            set
            {
                _frequencyEnabled = value;
                UpdateProperty();
            }
        }

        public void Update()
        {
            OgamibuDat ogamibuDat = new OgamibuDat();

            ogamibuDat._spkj = ShapeValue;
            ogamibuDat._koubai = GradientInputMethodValue;
            ogamibuDat._dosu = Frequency;
            ogamibuDat._tan1 = Tan1;
            ogamibuDat._tan2 = Tan2;

            DataRepository.UpdateOgamibu(ogamibuDat);

        }


        public void Tan2Do()
        {
            double rad;

            if (Tan1 < 0.01 || Tan2 < 0.01)
                Frequency = 0.0;
            else
            {
                rad = Math.Atan2(Tan1, Tan2);
                Frequency = 180.0 * rad / Math.PI;
                if (Frequency < -0.001)
                    Frequency *= (-1.0);
            }
        }
    }
}

using JOINT_G.Repository;
using JOINT_G.Views.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JOINT_G.ViewModels.Dialogs
{
    /// <summary>
    /// JOINT-G-02-MATDISP：部材リストの表示方法設定ダイアログのビューモデル。
    /// </summary>
    public class DisplayMethodDialogViewModel : BaseDialogViewModel
    {
        private DisplayMethodDialog _displayMethodDialog;

        #region Data Bindings
        private bool? _dialogResult;
        public Dictionary<int, string> CalcMethods { get; set; }
        public Dictionary<int, string> Positions { get; set; }
        public Dictionary<int, string> OutputMethods { get; set; }
        private int? _calcMethodValue;
        private int? _positionValue;
        private int? _outputMethodValue;
        private bool _okButtonEnabled;

        private bool _positionComboboxEnabled;
        private bool _outputMethodComboboxEnabled;

        public bool PositionComboboxEnabled
        {
            get => _positionComboboxEnabled;
            set
            {
                _positionComboboxEnabled = value;
                UpdateProperty();
            }
        }
        public bool OutputMethodComboboxEnabled
        {
            get => _outputMethodComboboxEnabled;
            set
            {
                _outputMethodComboboxEnabled = value;
                UpdateProperty();
            }
        }

        [Required]
        public int? CalcMethodValue
        {
            get => _calcMethodValue;
            set
            {
                _calcMethodValue = value;
                UpdateProperty();
                ValidateProperty(value);
            }
        }
        [Required]
        public int? PositionValue
        {
            get => _positionValue;
            set
            {
                _positionValue = value;
                UpdateProperty();
                ValidateProperty(value, nameof(PositionValue), true);
            }
        }
        [Required]
        public int? OutputMethodValue
        {
            get => _outputMethodValue;
            set
            {
                _outputMethodValue = value;
                UpdateProperty();
                ValidateProperty(value, nameof(OutputMethodValue), true);
            }
        }
        public bool OkButtonEnabled
        {
            get => _okButtonEnabled;
            set
            {
                _okButtonEnabled = value;
                UpdateProperty();
            }
        }
        public bool? DialogResult
        {
            get => _dialogResult;
            set
            {
                _dialogResult = value;
                UpdateProperty();
            }
        }
        #endregion

        public DisplayMethodDialogViewModel()
        {
            CalcMethods = new() { { 0, "0.全断面" }, { 1, "1.ＦＷ別" } };
            Positions = new() { { 0, "0.中央のみ" }, { 1, "1.中央・端部" } };
            OutputMethods = new() { { 0, "0.全断面出力" }, { 1, "1.ＦＷ毎ＰＬ出力" } };

            CalcMethodValue = 0;
            PositionValue = 0;
            OutputMethodValue = 0;
            OkButtonEnabled = true;
        }
        public void LoadedWindowCommandExecuted(object sender, EventArgs e)
        {
            _displayMethodDialog = sender as DisplayMethodDialog;
            var init = DataRepository.GetFilterMemberList();
            CalcMethodValue = init.Item1;
            PositionValue = init.Item2;
            OutputMethodValue = init.Item3;

        }
        public void UpdateFilter()
        {
            DataRepository.FilterMemberList(CalcMethodValue ?? 0, PositionValue ?? 0, OutputMethodValue ?? 0);
        }
    }
}

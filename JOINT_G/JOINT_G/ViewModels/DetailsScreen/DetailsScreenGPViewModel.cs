using JOINT_G.Models;
using JOINT_G.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using newfast;
using JOINT_G.Models.Interfaces;
using JOINT_G.Helpers;
using System.Text.RegularExpressions;
using JOINT_G.CustomControls;

namespace JOINT_G.ViewModels.DetailsScreen
{
    /// <summary>
    /// �ڍ׉�ʂ̃r���[���f���B
    /// </summary>
    public class DetailsScreenGPViewModel :
        ViewModelBase, IDetailScreenViewModel
    {
        #region Combobox Item
        public static Dictionary<int, string> Prop1Item => new()
        {
            { 0, "����" },
            { 1, "�璹" }
        };
        private static Dictionary<int, string> prop4Item = (new Func<Dictionary<int, string>>(() =>
         {
             try
             {
                 var result = DataRepository.GetFlgGstSpDat()._bolkk_list;

                 result = result.Trim();
                 Regex trimmer = new Regex(@"\s\s+");
                 result = trimmer.Replace(result, " ");

                 var data = new Dictionary<int, string>();
                 int j = 0;
                 foreach (var i in result.Split(" "))
                 {

                     data.Add(j, i);
                     j++;
                 }
                 return data;
             }
             catch (Exception ex)
             {
                 ex.HandleException();
                 return new Dictionary<int, string>();
             }
         })).Invoke();
        public static Dictionary<int, string> Prop4Item { get => prop4Item; }
        public static Dictionary<int, string> Prop12Item => new()
        {
            { 0, "����" },
            { 1, "�Ώ̐U�蕪��" }
        };

        public static Dictionary<int, string> Prop17Item => Helpers.DetailScreenHelper.GetStandards();

        public static Dictionary<int, string> Prop19Item => new()
        {
            { 1, "�e�k�f�،���" },
            { 2, "���e�k�f�،�" },
            { 3, "��e�k�f�،�" },
            { 4, "�㉺�e�k�f��" }
        };

        public static Dictionary<int, string> Prop20Item => new()
        {
            { 0, "�L�i�f�o����)" },
            { 1, "��" },
            { 2, "�L�i������)" },
        };

        public static Dictionary<int, string> Prop23Item => new()
        {
            { 1, "1��" },
            { 2, "2��" },
        };
        #endregion

        private int _selectedValueProp1;
        private int _selectedValueProp4;
        private int _selectedValueProp12;
        private int _selectedValueProp17;
        private int _selectedValueProp19;
        private int _selectedValueProp20;
        private int _selectedValueProp23;

        private bool _prop7Enabled;
        private bool _prop11Enabled;
        private bool _prop13Enabled;
        private bool _prop14Enabled;
        private bool _prop21Enabled;
        private bool _prop23Enabled;
        private bool _prop24Enabled;
        private bool _prop25Enabled;
        private bool _category3Enabled;
        private List<ErrorDetails> _errors;
        private bool _isFirstUpdateGspBkei = true;
        private bool _isCheckAll = false;
        private WdWindowPanel _drawArea;
        public DetailsScreenGPViewModel(
            PropertyGridModelBase propertyGridModel, WdWindowPanel drawArea)
        {
            PropertyGridModel = propertyGridModel;
            _drawArea = drawArea;
            _errors = new List<ErrorDetails>();
            LoadDataToModel();
        }

        #region Binding Selected Value
        private string _SelectedLine = string.Empty;
        public string SelectedLine
        {
            get => _SelectedLine;
            set => SetProperty(ref _SelectedLine, value);
        }
        public int SelectedValueProp1
        {
            get => _selectedValueProp1;
            set
            {
                _selectedValueProp1 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp4
        {
            get => _selectedValueProp4;
            set
            {
                _selectedValueProp4 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp12
        {
            get => _selectedValueProp12;
            set
            {
                _selectedValueProp12 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp17
        {
            get => _selectedValueProp17;
            set
            {
                _selectedValueProp17 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp19
        {
            get => _selectedValueProp19;
            set
            {
                _selectedValueProp19 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp20
        {
            get => _selectedValueProp20;
            set
            {
                _selectedValueProp20 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp23
        {
            get => _selectedValueProp23;
            set
            {
                _selectedValueProp23 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }

        #endregion

        #region IsEnableBinding
        public bool Category3Enabled
        {
            get => _category3Enabled;
            set
            {
                _category3Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop7Enabled
        {
            get => _prop7Enabled;
            set
            {
                _prop7Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop11Enabled
        {
            get => _prop11Enabled;
            set
            {
                _prop11Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop13Enabled
        {
            get => _prop13Enabled;
            set
            {
                _prop13Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop14Enabled
        {
            get => _prop14Enabled;
            set
            {
                _prop14Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop21Enabled
        {
            get => _prop21Enabled;
            set
            {
                _prop21Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop23Enabled
        {
            get => _prop23Enabled;
            set
            {
                _prop23Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop24Enabled
        {
            get => _prop24Enabled;
            set
            {
                _prop24Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop25Enabled
        {
            get => _prop25Enabled;
            set
            {
                _prop25Enabled = value;
                UpdateProperty();
            }
        }


        #endregion

        #region Public Field
        public string Title => DetailScreenHelper.GetTitle(DetailType.GST_PL);
        public PropertyGridModelBase PropertyGridModel { get; }
        public string[] IntTextBoxList { get; } = { "Prop2", "Prop5", "Prop6", "Prop7", "Prop15", "Prop25" };
        public int PropNumber { get => 25; }
        public string[] MemberInfors => DetailScreenHelper.GetMemberInfor(DetailType.GST_PL);
        public Dictionary<string, string> FormatStrings => new Dictionary<string, string>()
        {
            {"Prop1" , "%1d" },
            {"Prop2" , "%3d" },
            {"Prop3" , "%4.1f" },
            {"Prop4" , "%2d" },
            {"Prop5" , "%2d" },
            {"Prop6" , "%3d" },
            {"Prop7" , "%3d" },
            {"Prop8" , "%6.1f" },
            {"Prop9" , "%6.1f" },
            {"Prop10" , "%6.1f" },
            {"Prop11" , "%6.1f" },
            {"Prop12" , "%1d" },
            {"Prop13" , "%s" },
            {"Prop14" , "%6.1f" },
            {"Prop15" ,"%3d"},
            {"Prop16" , "%6.1f" },
            {"Prop17" ,"%2d" },
            {"Prop18" ,"%6.1f" },
            {"Prop19" , "%1d" },
            {"Prop20" , "%1d" },
            {"Prop21" , "%6.1f"},
            {"Prop22" , "%s" },
            {"Prop23" , "%1d" },
            {"Prop24" , "%4.1f" },
            {"Prop25" , "%2d" },
        };

        public List<ErrorDetails> Errors => _errors;

        #endregion

        private void LoadDataToModel(FlgGstPlDat result)
        {
            var model = (DetailsModelGP)PropertyGridModel;

            #region
            SelectedValueProp1 = result._hiTyp; //model.Prop1 = result._hiTyp;
            model.Prop2 = result._bkei.ToString();
            model.Prop3 = result._akei.ToString();

            SelectedValueProp4 = result._bkk; //model.Prop4 = result._bkk;
            model.Prop5 = result._retu.ToString();
            model.Prop6 = result._honsu1.ToString();


            model.Prop7 = result._honsu2.ToString();
            model.Prop8 = result._hasi1.ToString();
            model.Prop9 = result._hasi2.ToString();



            model.Prop10 = result._pit.ToString();
            model.Prop11 = result._rpit.ToString();
            SelectedValueProp12 = result._herik; //model.Prop12 = result._herik;


            model.Prop13 = result._herlb_label ?? "";
            model.Prop14 = result._heri.ToString();
            model.Prop15 = result._clr.ToString();

            model.Prop16 = result._spatu.ToString();
            SelectedValueProp17 = result._spkk; // model.Prop17 = result._spkk;
            model.Prop18 = result._futi.ToString();

            SelectedValueProp19 = result._keityp; //model.Prop19 = result._keityp;
            //SelectedValueProp20 = result._urarib; //model.Prop20 = result._urarib;
            model.Prop21 = result._urariba.ToString();


            model.Prop22 = result._splb_label ?? "";
            SelectedValueProp23 = result._gms;  // model.Prop23 = result._gms;
            model.Prop24 = result._bxgpclr.ToString();
            model.Prop25 = result._wkai.ToString();

            Prop7Enabled = Convert.ToBoolean(result._honsu2_act);
            Prop11Enabled = Convert.ToBoolean(result._rpit_act);
            Prop13Enabled = Convert.ToBoolean(result._herlb_act);
            Prop14Enabled = Convert.ToBoolean(result._heri_act);

            //Prop21Enabled = Convert.ToBoolean(result._urariba_act);
            Prop23Enabled = Convert.ToBoolean(result._gms_act);
            //Prop24Enabled = Convert.ToBoolean(result.bx);
            Prop25Enabled = Convert.ToBoolean(result._wkai_act);
            Category3Enabled = Convert.ToBoolean(result._wkai_act);

            UpdateProperty("PropertyGridModel");
            #endregion
        }
        private FlgGstPlDat UpdatePropertyGrid(bool onlyAssign = false)
        {
            FlgGstPlDat flgGstPlDat = new FlgGstPlDat();
            try
            {
                var model = (DetailsModelGP)PropertyGridModel;

                #region

                flgGstPlDat._hiTyp = SelectedValueProp1;
                flgGstPlDat._bkei = Convert.ToInt32(model.Prop2);
                flgGstPlDat._akei = Convert.ToDouble(model.Prop3);
                flgGstPlDat._bkk = SelectedValueProp4;
                flgGstPlDat._retu = Convert.ToInt32(model.Prop5);
                flgGstPlDat._honsu1 = Convert.ToInt32(model.Prop6);
                flgGstPlDat._honsu2 = Convert.ToInt32(model.Prop7);
                flgGstPlDat._hasi1 = Convert.ToDouble(model.Prop8);
                flgGstPlDat._hasi2 = Convert.ToDouble(model.Prop9);
                flgGstPlDat._pit = Convert.ToDouble(model.Prop10);
                flgGstPlDat._rpit = Convert.ToDouble(model.Prop11);
                flgGstPlDat._herik = SelectedValueProp12;
                flgGstPlDat._herlb_label = model.Prop13;
                flgGstPlDat._heri = Convert.ToDouble(model.Prop14);
                flgGstPlDat._clr = Convert.ToInt32(model.Prop15);
                flgGstPlDat._spatu = Convert.ToDouble(model.Prop16);
                flgGstPlDat._spkk = SelectedValueProp17;
                flgGstPlDat._futi = Convert.ToDouble(model.Prop18);
                flgGstPlDat._keityp = SelectedValueProp19;
                flgGstPlDat._urarib = SelectedValueProp20;
                flgGstPlDat._urariba = Convert.ToDouble(model.Prop21);

                flgGstPlDat._splb_label = model.Prop22;
                flgGstPlDat._gms = (short)SelectedValueProp23;
                flgGstPlDat._bxgpclr = Convert.ToDouble(model.Prop24);
                flgGstPlDat._wkai = Convert.ToInt16(model.Prop25);

                flgGstPlDat._honsu2_act = Convert.ToInt32(Prop7Enabled);
                flgGstPlDat._herlb_act = Convert.ToInt32(Prop13Enabled);
                flgGstPlDat._heri_act = Convert.ToInt32(Prop14Enabled);
                flgGstPlDat._rpit_act = Convert.ToInt32(Prop11Enabled);
                flgGstPlDat._urariba_act = Convert.ToInt32(Prop21Enabled);
                flgGstPlDat._gms_act = Convert.ToInt32(Prop23Enabled);
                flgGstPlDat._wkai_act = Convert.ToInt32(Prop25Enabled);

                if (onlyAssign)
                    return flgGstPlDat;
                #endregion

                DataRepository.UpdateFlgGstPlDat(ref flgGstPlDat);
                LoadDataToModel(flgGstPlDat);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
            return flgGstPlDat;
        }
        private void LoadDataToModel()
        {
            try
            {
                var model = (DetailsModelGP)PropertyGridModel;
                var flgGstPlDat = DataRepository.GetFlgGstPlDat();

                SelectedValueProp20 = flgGstPlDat._urarib;
                model.Prop21 = flgGstPlDat._urariba.ToString();
                Prop21Enabled = Convert.ToBoolean(flgGstPlDat._urariba_act);
                LoadDataToModel(flgGstPlDat);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        public bool ValidateProperty(string propName)
        {
            try
            {
                _errors = new List<ErrorDetails>();
                if (propName == "ALL")
                {
                    _isCheckAll = true;
                    UpdatePropertyGrid();
                    UpdateDrawWin();
                    for (var i = 1; i <= PropNumber; i++)
                    {
                        CheckInput($"Prop{i}");
                        if (_errors.Any(x => x.ErrorStatus == -1))
                        {
                            _isCheckAll = false;
                            return false;
                        }
                    }
                    _isCheckAll = false;
                }
                else
                    CheckInput(propName);
                return _errors.Count == 0;
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return true;
            }
        }
        private void CheckInput(string propName)
        {
            string errorMess = "";
            int isValid = 0;
            if (!DetailScreenHelper.CanCheckProp(propName))
                return;

            switch (propName)
            {
                case "Prop1":
                case "Prop4":
                case "Prop12":
                case "Prop17":
                case "Prop19":
                case "Prop20":
                case "Prop23":
                    if (DetailScreenHelper.HandelCombobox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
                default:
                    if (DetailScreenHelper.ValidateTextBox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
            }

            var model = (DetailsModelGP)PropertyGridModel;

            var jMS_GST_HITYP = SelectedValueProp1;
            int jMS_GST_BKEI;
            int.TryParse(model.Prop2, out jMS_GST_BKEI);

            double jMS_GST_AKEI;
            double.TryParse(model.Prop3, out jMS_GST_AKEI);
            int jMS_GST_RETU;
            int.TryParse(model.Prop5, out jMS_GST_RETU);
            int jMS_GST_HONSU1;
            int.TryParse(model.Prop6, out jMS_GST_HONSU1);
            int jMS_GST_HONSU2;
            int.TryParse(model.Prop7, out jMS_GST_HONSU2);

            double jMS_GST_HASI1;
            double.TryParse(model.Prop8, out jMS_GST_HASI1);
            double jMS_GST_HASI2;
            double.TryParse(model.Prop9, out jMS_GST_HASI2);
            double jMS_GST_RPIT;
            double.TryParse(model.Prop11, out jMS_GST_RPIT);
            double jMS_GST_HERI;
            double.TryParse(model.Prop14, out jMS_GST_HERI);

            int jMS_GST_CLR;
            int.TryParse(model.Prop15, out jMS_GST_CLR);

            double jMS_GSP_BXGPCLR = Convert.ToDouble(model.Prop24);
            int jMS_GSP_WKAI;
            int.TryParse(model.Prop25, out jMS_GSP_WKAI);



            switch (propName)
            {
                case "Prop2":  //JMS_GST_BKEI
                    if (jMS_GST_BKEI < -1)
                    {
                        isValid = -1;
                    }
                    else
                    {
                        //if (mod == 0)
                        //    wsp_data_get(Start_flg);
                    }
                    break;
                case "Prop3": // JMS_GST_AKEI
                    if (jMS_GST_AKEI < 0.1)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop5": // JMS_GST_RETU

                    if (jMS_GST_RETU < 1 || jMS_GST_RETU > 15) isValid = -1;
                    if (jMS_GST_HITYP == 1)
                    {
                        if (jMS_GST_RETU < 2) isValid = -1;
                    }
                    //if (isValid != -1) pit_act();
                    break;
                case "Prop6": // JMS_GST_HONSU1
                              //pit_act();
                case "Prop7": // JMS_GST_HONSU2
                    int intValue;
                    if (propName == "Prop6")
                        intValue = jMS_GST_HONSU1;
                    else intValue = jMS_GST_HONSU2;

                    if (intValue < 1)
                    {
                        isValid = -1;
                        break;
                    }

                    if (jMS_GST_RETU < 8)
                    {
                        if (intValue > 32)
                        {
                            isValid = -1;
                            break;
                        }
                    }
                    else if (jMS_GST_RETU > 7)
                    {
                        if (intValue > 16)
                        {
                            isValid = -1;
                            break;
                        }
                    }
                    if (propName == "Prop6")
                    {
                        //if (jMS_GST_HITYP == 0)
                        //    model.Prop7 = intValue.ToString();
                    }
                    if (propName == "Prop6")
                    {
                        //if (Jg_select == 1 && Inp_hi_typ == 0)
                        //{
                        //    Inp_nbolt2 = Inp_nbolt1;
                        //    JmsTextReset(&Jm_s[JMS_GST_HONSU2]);
                        //}
                    }
                    else if (propName == "Prop7")
                    {
                        if (
                            Math.Abs(jMS_GST_HONSU1 - intValue) != 1
                            )
                        {
                            isValid = -1;
                            break;
                        }
                    }
                    break;
                case "Prop8": // JMS_GST_HASI1
                    if (jMS_GST_HASI1 < jMS_GST_AKEI * 0.5)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop9":// JMS_GST_HASI2
                    if (jMS_GST_HASI2 < jMS_GST_AKEI * 0.5)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop10": // JMS_GST_PIT , Valid : COM
                    break;
                case "Prop11": // JMS_GST_RPIT
                    if (jMS_GST_RPIT < jMS_GST_AKEI)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop14": //JMS_GST_HERI
                    if (jMS_GST_HERI < 0.1)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop15": //JMS_GST_CLR
                    if (jMS_GST_CLR < 1) isValid = -1;
                    else if (jMS_GST_CLR < 5) isValid = -10;
                    //if (isValid > -1)
                    //    Console.WriteLine("web_hi_wid()");
                    //web_hi_wid();
                    break;
                case "Prop24": // JMS_GSP_BXGPCLR
                    if (jMS_GSP_BXGPCLR < (float)0.0) isValid = -1;
                    break;
                case "Prop25": // JMS_GSP_WKAI
                    if (jMS_GSP_WKAI < 0 || jMS_GSP_WKAI >= 90) isValid = -1;
                    break;
            }
            if (isValid < 0)
            {

                if (isValid == -10 || isValid == -2)
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = $"�N���A��{jMS_GST_CLR}�ł�";
                }
                else
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = "���͒l���s���ł��B";
                }

                _errors.Add(new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = isValid, PropName = propName });
            }
            else
            {
                if (!_isCheckAll || propName == "Prop20")
                {
                    if ("Prop2" == propName && _isFirstUpdateGspBkei == true)
                    {
                        FlgGstPlDat flgGstPlDat = UpdatePropertyGrid(true);

                        DataRepository.FirstUpdateGstPlBkei(ref flgGstPlDat);

                        LoadDataToModel(flgGstPlDat);

                        _isFirstUpdateGspBkei = false;
                    }
                    else if (propName == "Prop20")
                    {
                        FlgGstPlDat flgGstPlDat = new FlgGstPlDat();
                        flgGstPlDat._urarib = SelectedValueProp20;
                        flgGstPlDat._urariba_act = Convert.ToInt32(Prop21Enabled);
                        flgGstPlDat._urariba = Convert.ToDouble(model.Prop21);

                        DataRepository.UpdateUraribGstPl(ref flgGstPlDat);

                        SelectedValueProp20 = flgGstPlDat._urarib;
                        model.Prop21 = flgGstPlDat._urariba.ToString();
                        Prop21Enabled = Convert.ToBoolean(flgGstPlDat._urariba_act);

                    }
                    else
                        UpdatePropertyGrid();

                    UpdateDrawWin();
                }
                var validationResult = CheckInputCom(propName);

                if (validationResult != null)
                {
                    _errors.Add(validationResult);
                }
            }
        }
        private ErrorDetails CheckInputCom(string propName)
        {
            var validationResult = DataRepository.ValidateInpDat(DetailType.GST_PL);
            var length = validationResult.Item2.Length;

            ErrorDetails error = null;


            int controlIndex = GetControlIndexFromPropName(propName);

            if (controlIndex == 0)
                return error;

            for (var i = 0; i < length; i++)
            {
                var retCode = validationResult.Item2[i];
                var errorMess = validationResult.Item1[i];

                if (retCode < 0 && i + 1 == controlIndex)
                {
                    error = new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = retCode, PropName = propName };
                    break;
                }
            }
            return error;
        }
        private int GetControlIndexFromPropName(string propName)
        {
            int controlIndex = 0;
            switch (propName)
            {
                case "Prop10": // JMS_GST_PIT
                    controlIndex = 1;
                    break;
                case "Prop16": // JMS_GST_SPATU    
                    controlIndex = 2;
                    break;
                case "Prop17": // JMS_GST_SPKK        
                    controlIndex = 3;
                    break;
                case "Prop18": // JMS_GST_FUTI        
                    controlIndex = 4;
                    break;
                case "Prop21": //JMS_GST_URARIBA
                    controlIndex = 5;
                    break;
            }
            return controlIndex;
        }

        public void UpdateDrawWin()
        {
            _drawArea.Manager.DrawWinFlgGstPl(_drawArea.WinID);
        }

        public void SelectBolt()
        {
            _drawArea.Manager.SelBoltFlgGstPl(_drawArea.WinID);
        }

        public void ClearSelectBolt()
        {
            _drawArea.Manager.ClrBoltFlgGstPl(_drawArea.WinID);
        }
    }
}

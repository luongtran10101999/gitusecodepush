using JOINT_G.Helpers;
using JOINT_G.Models;
using JOINT_G.Models.Interfaces;
using JOINT_G.Repository;
using newfast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using JOINT_G.CustomControls;
namespace JOINT_G.ViewModels.DetailsScreen
{
    /// <summary>
    /// �ڍ׉�ʂ̃r���[���f���B
    /// </summary>
    public class DetailsScreenFSPViewModel : ViewModelBase, IDetailScreenViewModel
    {
        #region Combobox Item
        public static Dictionary<int, string> Prop1Item => new()
        {
            { 1, "���s2��" },
            { 2, "���s4��" },
            { 3, "�璹����" },
            { 4, "�璹�O��" },
            { 5, "���s6��" },
            { 6, "���s8��" },
            { 7, "�璹6���" },
            { 8, "�璹6��O" },
            { 9, "�璹8���" },
            { 10, "�璹8��O" },
        };

        private static Dictionary<int, string> _prop4Item = (new Func<Dictionary<int, string>>(() =>
         {
             try
             {
                 var result = DataRepository.GetFlgFspDat()._bolkk_list;

                 result = result.Trim();
                 Regex trimmer = new Regex(@"\s\s+");
                 result = trimmer.Replace(result, " ");

                 var data = new Dictionary<int, string>();
                 int j = 0;
                 foreach (var i in result.Split(" "))
                 {
                     data.Add(j, i);
                     j++;
                 }
                 return data;
             }
             catch (Exception ex)
             {
                 ex.HandleException();
                 return new Dictionary<int, string>();
             }
         })).Invoke();

        public static Dictionary<int, string> Prop4Item { get => _prop4Item; }

        public static Dictionary<int, string> Prop17Item => Helpers.DetailScreenHelper.GetStandards();
        public static Dictionary<int, string> Prop21Item => Prop17Item;

        #endregion

        private int _selectedValueProp1;
        private int _selectedValueProp4;
        private int _selectedValueProp17;
        private int _selectedValueProp21;
        private bool _prop6Enabled;
        private bool _prop7Enabled;
        private bool _prop8Enabled;
        private bool _prop9Enabled;
        private bool _prop12Enabled;
        private bool _prop19Enabled;
        private bool _prop20Enabled;
        private bool _prop21Enabled;

        private double _flgHaba;

        private List<ErrorDetails> _errors;
        private bool _isFirstUpdateGspBkei = true;
        private bool _isCheckAll = false;
        //private IDetailScreenView _window;
        private WdWindowPanel _drawArea;
        public DetailsScreenFSPViewModel(
            PropertyGridModelBase propertyGridModel,
            WdWindowPanel drawArea)
        {
            PropertyGridModel = propertyGridModel;
            _drawArea = drawArea;
            _errors = new List<ErrorDetails>();
            LoadDataToModel();
        }

        #region Selected Value Binding 
        private string _SelectedLine = string.Empty;
        public string SelectedLine
        {
            get => _SelectedLine;
            set => SetProperty(ref _SelectedLine, value);
        }
        public int SelectedValueProp1
        {
            get => _selectedValueProp1;
            set
            {
                _selectedValueProp1 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp4
        {
            get => _selectedValueProp4;
            set
            {
                _selectedValueProp4 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp17
        {
            get => _selectedValueProp17;
            set
            {
                _selectedValueProp17 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp21
        {
            get => _selectedValueProp21;
            set
            {
                _selectedValueProp21 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }

        #endregion

        #region IsEnable Binding
        public bool Prop6Enabled
        {
            get => _prop6Enabled;
            set
            {
                _prop6Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop7Enabled
        {
            get => _prop7Enabled;
            set
            {
                _prop7Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop8Enabled
        {
            get => _prop8Enabled;
            set
            {
                _prop8Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop9Enabled
        {
            get => _prop9Enabled;
            set
            {
                _prop9Enabled = value;
                UpdateProperty();
            }
        }

        public bool Prop12Enabled
        {
            get => _prop12Enabled;
            set
            {
                _prop12Enabled = value;
                UpdateProperty();
            }
        }

        public bool Prop19Enabled
        {
            get => _prop19Enabled;
            set
            {
                _prop19Enabled = value;
                UpdateProperty();
            }
        }

        public bool Prop20Enabled
        {
            get => _prop20Enabled;
            set
            {
                _prop20Enabled = value;
                UpdateProperty();
            }
        }

        public bool Prop21Enabled
        {
            get => _prop21Enabled;
            set
            {
                _prop21Enabled = value;
                UpdateProperty();
            }
        }

        #endregion

        #region Public Field
        public string Title => DetailScreenHelper.GetTitle(DetailType.FSP);
        public PropertyGridModelBase PropertyGridModel { get; }
        public int PropNumber => 21;
        public string[] MemberInfors => DetailScreenHelper.GetMemberInfor(DetailType.FSP);
        public Dictionary<string, string> FormatStrings => new Dictionary<string, string>()
        {
            {"Prop1" , "%2d" },
            {"Prop2" , "%4d" },
            {"Prop3" , "%4.1f" },
            {"Prop4" , "%1d" },
            {"Prop5" , "%2d" },
            {"Prop6" , "%6.1f" },
            {"Prop7" , "%6.1f" },
            {"Prop8" , "%6.1f" },
            {"Prop9" , "%6.1f" },
            {"Prop10" , "%6.1f" },
            {"Prop11" , "%6.1f" },
            {"Prop12" , "%6.1f" },
            {"Prop13" , "%3d" },
            {"Prop14" , "%6.1f" },
            {"Prop15" , "%6.1f" },
            {"Prop16" , "%s" },
            {"Prop17" , "%2d" },
            {"Prop18" , "%6.1f" },
            {"Prop19" , "%6.1f" },
            {"Prop20" , "%s" },
            {"Prop21" , "%2d" },
        };
        public List<ErrorDetails> Errors => _errors;
        #endregion

        private void LoadDataToModel(FlgFspDat flgFspDat)
        {

            try
            {
                var result = flgFspDat;
                var model = (DetailsModelFSP)PropertyGridModel;

                _flgHaba = _flgHaba == 0 ? result._flg_haba : _flgHaba;

                SelectedValueProp1 = result._hiTyp;
                model.Prop2 = result._bkei.ToString();
                model.Prop3 = result._akei.ToString();

                SelectedValueProp4 = result._bkk;
                model.Prop5 = result._honsu.ToString();
                model.Prop6 = result._gage1.ToString();
                model.Prop7 = result._gage2.ToString();
                model.Prop8 = result._gage3.ToString();
                model.Prop9 = result._gage4.ToString();
                model.Prop10 = result._hasi1.ToString();
                model.Prop11 = result._hasi2.ToString();
                model.Prop12 = result._rpit.ToString();
                model.Prop13 = result._clr.ToString();
                model.Prop14 = result._outsp.ToString();
                model.Prop15 = result._outhb.ToString();
                model.Prop16 = result._outln ?? "";
                SelectedValueProp17 = result._outkk;
                model.Prop18 = result._insp.ToString();

                model.Prop19 = result._inhb.ToString();
                model.Prop20 = result._inln ?? "";
                SelectedValueProp21 = result._inkk;

                Prop6Enabled = Convert.ToBoolean(result._gage1_act);
                Prop7Enabled = Convert.ToBoolean(result._gage2_act);
                Prop8Enabled = Convert.ToBoolean(result._gage3_act);
                Prop9Enabled = Convert.ToBoolean(result._gage4_act);
                Prop12Enabled = Convert.ToBoolean(result._rpit_act);
                Prop19Enabled = Convert.ToBoolean(result._inhb_act);
                Prop20Enabled = Convert.ToBoolean(result._inln_act);
                Prop21Enabled = Convert.ToBoolean(result._inkk_act);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        private FlgFspDat UpdatePropertyGrid(bool onlyAssign = false)
        {
            var flgFspDat = new FlgFspDat();
            try
            {
                var model = (DetailsModelFSP)PropertyGridModel;

                flgFspDat._hiTyp = SelectedValueProp1;
                flgFspDat._bkei = Convert.ToInt32(model.Prop2);
                flgFspDat._akei = Convert.ToDouble(model.Prop3);
                flgFspDat._bkk = SelectedValueProp4;
                flgFspDat._honsu = Convert.ToInt32(model.Prop5);
                flgFspDat._gage1 = Convert.ToDouble(model.Prop6);
                flgFspDat._gage2 = Convert.ToDouble(model.Prop7);
                flgFspDat._gage3 = Convert.ToDouble(model.Prop8);
                flgFspDat._gage4 = Convert.ToDouble(model.Prop9);
                flgFspDat._hasi1 = Convert.ToDouble(model.Prop10);
                flgFspDat._hasi2 = Convert.ToDouble(model.Prop11);
                flgFspDat._rpit = Convert.ToDouble(model.Prop12);
                flgFspDat._clr = Convert.ToInt32(model.Prop13);
                flgFspDat._outsp = Convert.ToDouble(model.Prop14);
                flgFspDat._outhb = Convert.ToDouble(model.Prop15);
                flgFspDat._outln = model.Prop16;
                flgFspDat._outkk = (short)SelectedValueProp17;
                flgFspDat._insp = Convert.ToDouble(model.Prop18);
                flgFspDat._inhb = Convert.ToDouble(model.Prop19);
                flgFspDat._inln = model.Prop20;
                flgFspDat._inkk = (short)SelectedValueProp21;

                flgFspDat._gage1_act = Convert.ToInt32(Prop6Enabled);
                flgFspDat._gage2_act = Convert.ToInt32(Prop7Enabled);
                flgFspDat._gage3_act = Convert.ToInt32(Prop8Enabled);
                flgFspDat._gage4_act = Convert.ToInt32(Prop9Enabled);
                flgFspDat._rpit_act = Convert.ToInt32(Prop12Enabled);
                flgFspDat._inhb_act = Convert.ToInt32(Prop19Enabled);
                flgFspDat._inln_act = Convert.ToInt32(Prop20Enabled);
                flgFspDat._inkk_act = Convert.ToInt32(Prop21Enabled);
                if (onlyAssign)
                    return flgFspDat;
                DataRepository.UpdateFlgFspDat(ref flgFspDat);

                LoadDataToModel(flgFspDat);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
            return flgFspDat;
        }
        public bool ValidateProperty(string propName)
        {
            try
            {
                _errors = new List<ErrorDetails>();
                if (propName == "ALL")
                {
                    _isCheckAll = true;
                    UpdatePropertyGrid();
                    UpdateDrawWin();
                    for (var i = 1; i <= PropNumber; i++)
                    {
                        CheckInput($"Prop{i}");
                        if (_errors.Any(x => x.ErrorStatus == -1))
                        {
                            _isCheckAll = false;
                            return false;
                        }
                    }
                    _isCheckAll = false;
                }
                else
                    CheckInput(propName);
                return _errors.Count == 0;
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return true;
            }
        }
        private void LoadDataToModel()
        {
            try
            {
                LoadDataToModel(DataRepository.GetFlgFspDat());
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        private void CheckInput(string propName)
        {
            string errorMess = "";
            int isValid = 0;
            if (!DetailScreenHelper.CanCheckProp(propName))
                return;
            switch (propName)
            {
                case "Prop1":
                case "Prop4":
                case "Prop17":
                case "Prop21":
                    if (DetailScreenHelper.HandelCombobox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
                default:
                    if (DetailScreenHelper.ValidateTextBox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
            }

            var model = (DetailsModelFSP)PropertyGridModel;

            int jMS_FSP_BKEI;
            int.TryParse(model.Prop2, out jMS_FSP_BKEI);
            double jMS_FSP_AKEI;
            double.TryParse(model.Prop3, out jMS_FSP_AKEI);
            int jMS_FSP_HONSU;
            int.TryParse(model.Prop5, out jMS_FSP_HONSU);

            double jMS_FSP_GAGE1;
            double.TryParse(model.Prop6, out jMS_FSP_GAGE1);
            double jMS_FSP_GAGE2;
            double.TryParse(model.Prop7, out jMS_FSP_GAGE2);
            double jMS_FSP_GAGE3;
            double.TryParse(model.Prop8, out jMS_FSP_GAGE3);
            double jMS_FSP_GAGE4;
            double.TryParse(model.Prop9, out jMS_FSP_GAGE4);
            double jMS_FSP_HASI1;
            double.TryParse(model.Prop10, out jMS_FSP_HASI1);
            double jMS_FSP_HASI2;
            double.TryParse(model.Prop11, out jMS_FSP_HASI2);
            double jMS_FSP_RPIT;
            double.TryParse(model.Prop12, out jMS_FSP_RPIT);
            int jMS_FSP_CLR;
            int.TryParse(model.Prop13, out jMS_FSP_CLR);

            double jMS_FSP_OUTHB;
            double.TryParse(model.Prop15, out jMS_FSP_OUTHB);
            double jMS_FSP_INHB;
            double.TryParse(model.Prop19, out jMS_FSP_INHB);



            int[] iblt = new int[2];
            int ip, nb;
            switch (propName)
            {
                case "Prop2": //JMS_FSP_BKEI
                    if (jMS_FSP_BKEI < 1) isValid = -1;
                    else
                    {
                        //if (mod == 0)
                        //{
                        //    /*��ԍŏ��̓��͂������ꍇ�ɏ��������s��*/
                        //    fsp_bkei_get(Start_flg);
                        //}
                    }
                    break;
                case "Prop3": //JMS_FSP_AKEI
                    if (jMS_FSP_BKEI > jMS_FSP_AKEI)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop5": //JMS_FSP_HONSU
                    if (jMS_FSP_HONSU > 0)
                    {
                        //JMS_FSP_HITYP
                        var tt = SelectedValueProp1;
                        switch (tt)
                        {
                            case 1:
                            case 3:
                            case 4:
                                if (jMS_FSP_HONSU % 2 != 0)
                                    isValid = -1;
                                break;

                            case 2:
                                if (jMS_FSP_HONSU % 4 != 0)
                                    isValid = -1;
                                break;
                            case 5:     // ���s6��
                                if (jMS_FSP_HONSU % 6 != 0)
                                    isValid = -1;
                                break;
                            case 6:     // ���s8��
                                if (jMS_FSP_HONSU % 8 != 0)
                                    isValid = -1;
                                break;
                            case 7:     // �璹6��
                            case 8:     // �璹6��
                                if (jMS_FSP_HONSU < 6)
                                {
                                    isValid = -1;
                                    break;
                                }

                                if (tt == 7)
                                {
                                    iblt[0] = 2;    // �{���g��P��{��
                                    iblt[1] = 4;    // �{���g��Q��{��
                                }
                                else
                                {
                                    iblt[0] = 4;    // �{���g��P��{��
                                    iblt[1] = 2;    // �{���g��Q��{��
                                }

                                nb = jMS_FSP_HONSU;
                                ip = 0;
                                while (nb > 0)
                                {
                                    nb -= iblt[ip];
                                    if (nb <= 0)
                                        break;

                                    ip = 1 - ip;
                                }
                                if (nb < 0)
                                    isValid = -1;

                                break;
                            case 9:     // �璹8��
                            case 10:
                                if (jMS_FSP_HONSU < 8)
                                {
                                    isValid = -1;
                                    break;
                                }

                                if (jMS_FSP_HONSU % 4 != 0)
                                    isValid = -1;
                                break;
                        }
                    }
                    else isValid = -1;
                    break;
                case "Prop6": //JMS_FSP_GAGE1
                    if (jMS_FSP_GAGE1 < jMS_FSP_AKEI || (jMS_FSP_GAGE1 + jMS_FSP_AKEI) > _flgHaba)
                        isValid = -1;
                    break;
                case "Prop7": //JMS_FSP_GAGE2
                    if (jMS_FSP_GAGE2 < jMS_FSP_GAGE1 + jMS_FSP_AKEI || (jMS_FSP_GAGE2 + jMS_FSP_AKEI) > _flgHaba)
                        isValid = -1;
                    break;
                case "Prop8": //JMS_FSP_GAGE3
                              // Flg_sp2.gage1 : Prop6 ,   Flg_sp2.dhall : Prop3
                              // Flg_sp2.gage2 : Prop7
                              // Flg_sp2.gage3 : Prop8
                    if ((jMS_FSP_GAGE3 < (jMS_FSP_AKEI + jMS_FSP_GAGE1)) ||
                        (jMS_FSP_GAGE3 < (jMS_FSP_AKEI + jMS_FSP_GAGE2)))
                        isValid = -1;
                    break;
                case "Prop9": //JMS_FSP_GAGE4
                    // Flg_sp2.gage1 : Prop6 ,   Flg_sp2.dhall : Prop3
                    // Flg_sp2.gage2 : Prop7
                    // Flg_sp2.gage3 : Prop8
                    if ((jMS_FSP_GAGE4 < (jMS_FSP_AKEI + jMS_FSP_GAGE1)) ||
                      (jMS_FSP_GAGE4 < (jMS_FSP_AKEI + jMS_FSP_GAGE2)) ||
                      (jMS_FSP_GAGE4 < (jMS_FSP_AKEI + jMS_FSP_GAGE3)))
                        isValid = -1;
                    break;
                case "Prop10": //JMS_FSP_HASI1
                    // Flg_sp2.dhall : Prop3
                    if (jMS_FSP_HASI1 < 0.5 * jMS_FSP_AKEI)
                        isValid = -1;
                    break;
                case "Prop11": //JMS_FSP_HASI2
                    if (jMS_FSP_HASI2 < 0.5 * jMS_FSP_AKEI)
                        isValid = -1;
                    break;
                case "Prop12": //JMS_FSP_RPIT
                    //Flg_sp2.pit is rpit
                    if (jMS_FSP_RPIT < jMS_FSP_AKEI)
                        isValid = -1;
                    break;
                case "Prop13": //JMS_FSP_CLR
                    if (jMS_FSP_CLR < 1)
                    {
                        isValid = -1;
                        break;
                    }
                    else if (jMS_FSP_CLR < 5) isValid = -10;
                    //if (sts > -1)
                    //    len_output(zz);
                    break;

                case "Prop15": //JMS_FSP_OUTHB
                    if (jMS_FSP_OUTHB < (double)0.1)
                    {
                        isValid = -1;
                        break;
                    }
                    break;
                case "Prop19": //JMS_FSP_INHB
                    if (jMS_FSP_INHB < (float)0.1) isValid = -1;
                    break;
            }
            if (isValid < 0)
            {
                if (isValid == -10 || isValid == -2)
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = $"�N���A��{jMS_FSP_CLR}�ł�";
                }
                else
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = "���͒l���s���ł��B";
                }

                _errors.Add(new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = isValid, PropName = propName });
            }
            else
            {
                if (!_isCheckAll)
                {
                    if ("Prop2" == propName && _isFirstUpdateGspBkei == true)
                    {
                        FlgFspDat flgFspDat = UpdatePropertyGrid(true);

                        DataRepository.FirstUpdateFspBkei(ref flgFspDat);

                        LoadDataToModel(flgFspDat);

                        _isFirstUpdateGspBkei = false;
                    }
                    else
                        UpdatePropertyGrid();

                    UpdateDrawWin();
                }
                var validationResult = CheckInputCom(propName);

                if (validationResult != null)
                {
                    _errors.Add(validationResult);
                }
            }

        }
        private ErrorDetails CheckInputCom(string propName)
        {
            var validationResult = DataRepository.ValidateInpDat(DetailType.FSP);
            var length = validationResult.Item2.Length;

            ErrorDetails error = null;


            int controlIndex = GetControlIndexFromPropName(propName);

            if (controlIndex == 0)
                return error;

            for (var i = 0; i < length; i++)
            {
                var retCode = validationResult.Item2[i];
                var errorMess = validationResult.Item1[i];

                if (retCode < 0 && i + 1 == controlIndex)
                {
                    error = new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = retCode, PropName = propName };
                    break;
                }
            }
            return error;
        }
        private int GetControlIndexFromPropName(string propName)
        {
            int controlIndex = 0;
            switch (propName)
            {
                case "Prop14": // JMS_FSP_OUTSP
                    controlIndex = 1;
                    break;
                case "Prop17": // JMS_FSP_OUTKK    
                    controlIndex = 2;
                    break;
                case "Prop18": // JMS_FSP_INSP        
                    controlIndex = 3;
                    break;
                case "Prop21": // JMS_FSP_INKK        
                    controlIndex = 4;
                    break;
            }
            return controlIndex;
        }

        public void UpdateDrawWin()
        {
            _drawArea.Manager.DrawWinFlgGstPl(_drawArea.WinID);
        }

        public void SelectBolt()
        {
        }

        public void ClearSelectBolt()
        {
        }
    }
}

using FCWPF.FCCommon.FCExtentions.PropertyGrid;
using JOINT_G.Helpers;
using JOINT_G.Models;
using JOINT_G.Models.Interfaces;
using JOINT_G.Repository;
using newfast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using JOINT_G.CustomControls;
using FCWPF.FCCommon.FCControls.Combination;

namespace JOINT_G.ViewModels.DetailsScreen
{
    /// <summary>
    /// �ڍ׉�ʂ̃r���[���f���B
    /// </summary>
    public class DetailsScreenTGPViewModel :
        ViewModelBase, IDetailScreenViewModel
    {
        #region Combobox Item
        public static Dictionary<int, string> Prop1Item => new()
        {
            { 0, "����" },
            { 1, "�璹" }
        };

        public static Dictionary<int, string> Prop4Item => new()
        {
            { 0, "F10T" },
            { 1, "S10T" },
            { 2, "���{���g" },
            { 3, "F8T" },
            { 4, "ST14" }
        };

        public static Dictionary<int, string> Prop12Item => new()
        {
            { 0, "����" },
            { 1, "�Ώ̐U�蕪��" }
        };
        public static Dictionary<int, string> Prop17Item => Helpers.DetailScreenHelper.GetStandards();
        public static Dictionary<int, string> Prop19Item => new()
        {
            { 1, "�e�k�f�،���" },
            { 2, "���e�k�f�،�" },
            { 3, "��e�k�f�،�" },
            { 4, "�㉺�e�k�f�،�" }
        };

        public static Dictionary<int, string> Prop20Item => new()
        {
            { 0, "�L�i�f�o����)" },
            { 1, "��" },
            { 2, "�L�i�����́j" }
        };

        private static Dictionary<int, string> _jW_TX_GTBZComboboxItem = (new Func<Dictionary<int, string>>(() =>
         {
             try
             {

                 var result = DataRepository.GetFlgTgpDat()._ct_list;
                 result = result.Trim();
                 Regex trimmer = new Regex(@"\s\s+");
                 result = trimmer.Replace(result, " ");

                 var data = new Dictionary<int, string>();
                 foreach (var i in result.Split(" "))
                 {
                     int val = Convert.ToInt32(Regex.Replace(i, @"\D", @" "));
                     data.Add(val, i.Replace(val.ToString() + ".", ""));
                 }


                 return data;
             }
             catch (Exception ex)
             {
                 ex.HandleException();
                 return new Dictionary<int, string>();
             }
         })).Invoke();

        public static Dictionary<int, string> JW_TX_GTBZComboboxItem { get => _jW_TX_GTBZComboboxItem; }

        public static Dictionary<int, string> JW_TX_GTKKComboboxItem => Helpers.DetailScreenHelper.GetStandards();

        #endregion

        private int _seletedValue_JW_TX_GTKK;
        private int _seletedValue_JW_TX_GTBZ;
        private double _text1Dimension { get; set; }
        private double _text2Dimension { get; set; }
        private double _text3Dimension { get; set; }
        private double _text4Dimension { get; set; }

        private int _selectedValueProp1;
        private int _selectedValueProp4;
        private int _selectedValueProp12;
        private int _selectedValueProp17;
        private int _selectedValueProp19;
        private int _selectedValueProp20;

        private bool _prop7Enabled;
        private bool _prop10Enabled;
        private bool _prop11Enabled;
        private bool _prop12Enabled;
        private bool _prop13Enabled;
        private bool _prop14Enabled;
        private bool _prop21Enabled;

        private List<ErrorDetails> _errors;
        private bool _isFirstUpdateGspBkei = true;
        private bool _isCheckAll = false;
        private WdWindowPanel _drawArea;
        public DetailsScreenTGPViewModel(
            PropertyGridModelBase propertyGridModel, WdWindowPanel drawArea)
        {
            PropertyGridModel = propertyGridModel;
            _drawArea = drawArea;
            _errors = new List<ErrorDetails>();
            LoadDataToModel();
        }

        #region Selected Value Binding
        private string _SelectedLine = string.Empty;
        public string SelectedLine
        {
            get => _SelectedLine;
            set => SetProperty(ref _SelectedLine, value);
        }
        public int SelectedValueProp1
        {
            get => _selectedValueProp1;
            set
            {
                _selectedValueProp1 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp4
        {
            get => _selectedValueProp4;
            set
            {
                _selectedValueProp4 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp12
        {
            get => _selectedValueProp12;
            set
            {
                _selectedValueProp12 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp17
        {
            get => _selectedValueProp17;
            set
            {
                _selectedValueProp17 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp19
        {
            get => _selectedValueProp19;
            set
            {
                _selectedValueProp19 = value;
                if (DetailScreenHelper.CanUpdateCombobox() || _isFirstUpdateGspBkei == false)
                    UpdateProperty();
            }
        }
        public int SelectedValueProp20
        {
            get => _selectedValueProp20;
            set
            {
                _selectedValueProp20 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public double Text1Dimension
        {
            get => _text1Dimension;
            set
            {
                _text1Dimension = value;
                UpdateProperty();
            }
        }
        public double Text2Dimension
        {
            get => _text2Dimension;
            set
            {
                _text2Dimension = value;
                UpdateProperty();
            }
        }
        public double Text3Dimension
        {
            get => _text3Dimension;
            set
            {
                _text3Dimension = value;
                UpdateProperty();
            }
        }
        public double Text4Dimension
        {
            get => _text4Dimension;
            set
            {
                _text4Dimension = value;
                UpdateProperty();
            }
        }
        public int SeletedValue_JW_TX_GTKK
        {
            get => _seletedValue_JW_TX_GTKK;
            set
            {
                _seletedValue_JW_TX_GTKK = value;
                UpdateProperty();
            }
        }
        public int SeletedValue_JW_TX_GTBZ
        {
            get => _seletedValue_JW_TX_GTBZ;
            set
            {
                _seletedValue_JW_TX_GTBZ = value;
                UpdateProperty();
            }
        }
        #endregion 

        #region IsEnable Binding

        public bool Prop7Enabled
        {
            get => _prop7Enabled;
            set
            {
                _prop7Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop10Enabled
        {
            get => _prop10Enabled;
            set
            {
                _prop10Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop11Enabled
        {
            get => _prop11Enabled;
            set
            {
                _prop11Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop12Enabled
        {
            get => _prop12Enabled;
            set
            {
                _prop12Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop13Enabled
        {
            get => _prop13Enabled;
            set
            {
                _prop13Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop14Enabled
        {
            get => _prop14Enabled;
            set
            {
                _prop14Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop21Enabled
        {
            get => _prop21Enabled;
            set
            {
                _prop21Enabled = value;
                UpdateProperty();
            }
        }

        #endregion

        #region Public Field
        public string Title => DetailScreenHelper.GetTitle(DetailType.TGP);
        public PropertyGridModelBase PropertyGridModel { get; }
        public string[] IntTextBoxList { get; } = { "Prop2", "Prop5", "Prop6", "Prop7", "Prop15", "Prop25" };
        public int PropNumber => 22;
        public string[] MemberInfors => DetailScreenHelper.GetMemberInfor(DetailType.TGP);
        public Dictionary<string, string> FormatStrings => new Dictionary<string, string>
        {
            { "Prop1" , "%1d" },
            { "Prop2" , "%3d" },
            { "Prop3" , "%4.1f" },
            { "Prop4" , "%2d" },
            { "Prop5" , "%2d" },
            { "Prop6" , "%3d" },
            { "Prop7" ,"%3d" },
            { "Prop8" , "%6.1f" },
            { "Prop9" , "%6.1f" },
            { "Prop10" ,"%6.1f" },
            { "Prop11" , "%6.1f" },
            { "Prop12" , "%1d" },
            { "Prop13" , "%1s"  },
            { "Prop14" , "%6.1f" },
            { "Prop15" , "%3d" },
            { "Prop16" , "%6.1f" },
            { "Prop17" ,"%2d" },
            { "Prop18" , "%6.1f" },
            { "Prop19" , "%1d"},
            { "Prop20" , "%1d" },
            { "Prop21" , "%6.1f" },
            { "Prop22" , "%s" },
        };

        public List<ErrorDetails> Errors => _errors;



        #endregion

        private void LoadDataToModel(FlgTgpDat flgTgpDat)
        {

            var result = flgTgpDat;

            var model = (DetailsModelTGP)PropertyGridModel;

            SelectedValueProp1 = result._hiTyp;  //model.Prop1 = result._hiTyp;   


            model.Prop2 = result._bkei.ToString();
            model.Prop3 = result._akei.ToString();
            SelectedValueProp4 = result._bkk;  //model.Prop4 = result._bkk;
            model.Prop5 = result._retu.ToString();
            model.Prop6 = result._honsu1.ToString();
            model.Prop7 = result._honsu2.ToString();
            model.Prop8 = result._hasi1.ToString();
            model.Prop9 = result._hasi2.ToString();
            model.Prop10 = result._pit.ToString();
            model.Prop11 = result._rpit.ToString();

            SelectedValueProp12 = flgTgpDat._herik; //model.Prop12 = result._herik;

            model.Prop13 = result._herlb_label ?? "";
            model.Prop14 = result._heri.ToString();
            model.Prop15 = result._clr.ToString();
            model.Prop16 = result._gpatu.ToString();
            SelectedValueProp17 = result._gpkk;            //  model.Prop17 = result._gpkk;
            model.Prop18 = result._futi.ToString();
            SelectedValueProp19 = result._keityp; //  model.Prop19 = result._keityp;
            //SelectedValueProp20 = result._urarib; // model.Prop20 = result._urarib;
            model.Prop21 = result._urariba.ToString();
            model.Prop22 = result._gplb_label ?? "";




            SeletedValue_JW_TX_GTKK = result._tgaskk;
            SeletedValue_JW_TX_GTBZ = result._tgscod;
            //Input_JW_TX_LEN = result._tgasl;

            //Text1Dimension = result._tgasgsp_0;
            //Text2Dimension = result._tgasgsp_1;
            //Text3Dimension = result._tgasgsp_2;
            //Text4Dimension = result._tgasgsp_3;



            Prop7Enabled = Convert.ToBoolean(result._honsu2_act);
            Prop10Enabled = Convert.ToBoolean(result._pit_act);
            Prop11Enabled = Convert.ToBoolean(result._rpit_act);
            Prop12Enabled = Convert.ToBoolean(result._herik_act);
            Prop13Enabled = Convert.ToBoolean(result._herlb_act);
            Prop14Enabled = Convert.ToBoolean(result._heri_act);
            //Prop21Enabled = Convert.ToBoolean(result._urariba_act);
            UpdateProperty("PropertyGridModel");
        }
        private FlgTgpDat UpdatePropertyGrid(bool onlyAssign = false)
        {
            var flgTgpDat = new FlgTgpDat();
            try
            {
                var model = (DetailsModelTGP)PropertyGridModel;

                flgTgpDat._hiTyp = SelectedValueProp1;
                flgTgpDat._bkei = Convert.ToInt32(model.Prop2);
                flgTgpDat._akei = Convert.ToDouble(model.Prop3);
                flgTgpDat._bkk = SelectedValueProp4;
                flgTgpDat._retu = Convert.ToInt32(model.Prop5);
                flgTgpDat._honsu1 = Convert.ToInt32(model.Prop6);
                flgTgpDat._honsu2 = Convert.ToInt32(model.Prop7);
                flgTgpDat._hasi1 = Convert.ToDouble(model.Prop8);
                flgTgpDat._hasi2 = Convert.ToDouble(model.Prop9);
                flgTgpDat._pit = Convert.ToDouble(model.Prop10);
                flgTgpDat._rpit = Convert.ToDouble(model.Prop11);
                flgTgpDat._herik = SelectedValueProp12;
                flgTgpDat._herlb_label = model.Prop13;
                flgTgpDat._heri = Convert.ToDouble(model.Prop14);
                flgTgpDat._clr = Convert.ToInt32(model.Prop15);
                flgTgpDat._gpatu = Convert.ToDouble(model.Prop16);
                flgTgpDat._gpkk = SelectedValueProp17;
                flgTgpDat._futi = Convert.ToDouble(model.Prop18);
                flgTgpDat._keityp = SelectedValueProp19;
                flgTgpDat._urarib = SelectedValueProp20;
                flgTgpDat._urariba = Convert.ToDouble(model.Prop21);
                flgTgpDat._gplb_label = model.Prop22;

                flgTgpDat._tgscod = SeletedValue_JW_TX_GTBZ;
                flgTgpDat._tgaskk = SeletedValue_JW_TX_GTKK;
                flgTgpDat._tgasgsp_0 = Text1Dimension;
                flgTgpDat._tgasgsp_1 = Text2Dimension;
                flgTgpDat._tgasgsp_2 = Text3Dimension;
                flgTgpDat._tgasgsp_3 = Text4Dimension;
                flgTgpDat._tgasl = Convert.ToDouble(model.CTValue4);

                flgTgpDat._honsu2_act = Convert.ToInt32(Prop7Enabled);
                flgTgpDat._pit_act = Convert.ToInt32(Prop10Enabled);
                flgTgpDat._rpit_act = Convert.ToInt32(Prop11Enabled);
                flgTgpDat._herik_act = Convert.ToInt32(Prop12Enabled);
                flgTgpDat._herlb_act = Convert.ToInt32(Prop13Enabled);
                flgTgpDat._heri_act = Convert.ToInt32(Prop14Enabled);
                flgTgpDat._urariba_act = Convert.ToInt32(Prop21Enabled);

                if (onlyAssign)
                    return flgTgpDat;

                DataRepository.UpdateFlgTgpDat(ref flgTgpDat);
                LoadDataToModel(flgTgpDat);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
            return flgTgpDat;
        }
        public bool ValidateProperty(string propName)
        {
            try
            {
                if (propName == "ALL")
                {
                    _isCheckAll = true;
                    UpdatePropertyGrid();
                    UpdateDrawWin();
                    // Check All CtCompomemnt first
                    //_errors = new List<ErrorDetails>();
                    for (var i = 1; i <= PropNumber; i++)
                    {
                        CheckInput($"Prop{i}");
                        if (_errors.Any(x => x.ErrorStatus == -1))
                        {
                            _isCheckAll = false;
                            return false;
                        }
                    }
                    _isCheckAll = false;
                }
                else
                {
                    _errors = new List<ErrorDetails>();
                    CheckInput(propName);
                }
                return _errors.Count == 0;
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return false;
            }
        }
        public bool ValidateCompomentCt(string name, FCPropertyGrid propertyGrid)
        {
            try
            {
                _errors = new List<ErrorDetails>();

                if (name == "ALL")
                {
                    _isCheckAll = true;
                    UpdatePropertyGrid();
                    UpdateDrawWin();
                    string[] compomentCt = { "CTValue1", "CTValue3", "CTValue4", "Text1Dimension", "Text2Dimension", "Text3Dimension", "Text4Dimension" };

                    foreach (string compoment in compomentCt)
                    {
                        CheckInput(compoment, propertyGrid);
                    }
                    _isCheckAll = false;
                }
                else
                {

                    CheckInput(name, propertyGrid);
                }
                return _errors.Count == 0;
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return true;
            }
        }
        private void LoadDataToModel()
        {
            try
            {
                var model = (DetailsModelTGP)PropertyGridModel;
                var flgTgpDat = DataRepository.GetFlgTgpDat();

                SelectedValueProp20 = flgTgpDat._urarib;
                model.Prop21 = flgTgpDat._urariba.ToString();
                Prop21Enabled = Convert.ToBoolean(flgTgpDat._urariba_act);



                Text1Dimension = flgTgpDat._tgasgsp_0;
                Text2Dimension = flgTgpDat._tgasgsp_1;
                Text3Dimension = flgTgpDat._tgasgsp_2;
                Text4Dimension = flgTgpDat._tgasgsp_3;
                model.CTValue4 = flgTgpDat._tgasl.ToString();

                LoadDataToModel(flgTgpDat);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        private void CheckInput(string propName, FCPropertyGrid propertyGrid = null)
        {
            string errorMess = "";
            int isValid = 0;


            if (!DetailScreenHelper.CanCheckProp(propName))
                return;

            var inputDimension = propertyGrid?.GetEditor("CTValue2") as FCInputDimension;
            switch (propName)
            {
                case "CTValue1":
                    if (DetailScreenHelper.HandelCombobox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
                case "CTValue3":
                    if (DetailScreenHelper.HandelCombobox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
                case "CTValue4":
                    if (DetailScreenHelper.ValidateTextBox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
                case "Text1Dimension":
                    if (DetailScreenHelper.ValidateTextBox(propName, inputDimension.Txt1))
                    {
                        Text1Dimension = Convert.ToDouble(inputDimension.Txt1.Text);
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
                case "Text2Dimension":
                    if (DetailScreenHelper.ValidateTextBox(propName, inputDimension.Txt2))
                    {
                        Text2Dimension = Convert.ToDouble(inputDimension.Txt2.Text);
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
                case "Text3Dimension":
                    if (DetailScreenHelper.ValidateTextBox(propName, inputDimension.Txt3))
                    {
                        Text3Dimension = Convert.ToDouble(inputDimension.Txt3.Text);
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
                case "Text4Dimension":
                    if (DetailScreenHelper.ValidateTextBox(propName, inputDimension.Txt4))
                    {
                        Text4Dimension = Convert.ToDouble(inputDimension.Txt4.Text);
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }

                case "Prop1":
                case "Prop4":
                case "Prop12":
                case "Prop17":
                case "Prop19":
                case "Prop20":
                case "Prop23":
                    if (DetailScreenHelper.HandelCombobox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
                default:
                    if (DetailScreenHelper.ValidateTextBox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
            }
            var model = (DetailsModelTGP)PropertyGridModel;
            int jMS_GST_CLR;
            int.TryParse(model.Prop15, out jMS_GST_CLR);
            if (isValid == 0)
            {

                var jMS_GST_HITYP = SelectedValueProp1;
                int jMS_TGP_BKEI;
                int.TryParse(model.Prop2, out jMS_TGP_BKEI);
                double jMS_TGP_AKEI;
                double.TryParse(model.Prop3, out jMS_TGP_AKEI);
                int jMS_GST_RETU;
                int.TryParse(model.Prop5, out jMS_GST_RETU);
                int jMS_GST_HONSU1;
                int.TryParse(model.Prop6, out jMS_GST_HONSU1);
                int jMS_GST_HONSU2;
                int.TryParse(model.Prop7, out jMS_GST_HONSU2);

                double jMS_GST_HASI1;
                double.TryParse(model.Prop8, out jMS_GST_HASI1);
                double jMS_GST_HASI2;
                double.TryParse(model.Prop9, out jMS_GST_HASI2);
                double jMS_GST_PIT;
                double.TryParse(model.Prop10, out jMS_GST_PIT);
                double jMS_GST_RPIT;
                double.TryParse(model.Prop11, out jMS_GST_RPIT);
                double jMS_GST_HERI;
                double.TryParse(model.Prop14, out jMS_GST_HERI);

                double input_JW_TX_LEN;
                double.TryParse(model.CTValue4, out input_JW_TX_LEN);

                switch (propName)
                {
                    case "Prop2": // JMS_TGP_BKEI
                        if (jMS_TGP_BKEI < 1) isValid = -1;
                        else
                        {
                            //if (mod == 0)
                            //{
                            //    wsp_data_get(Start_flg);
                            //}
                        }
                        break;
                    case "Prop3": //JMS_TGP_AKEI
                        if (jMS_TGP_AKEI < 0.1) isValid = -1;
                        break;
                    case "Prop5": // JMS_GST_RETU
                        if (jMS_GST_RETU < 1 || jMS_GST_RETU > 15) isValid = -1;
                        if (jMS_GST_HITYP == 1)
                        {
                            if (jMS_GST_RETU < 2) isValid = -1;
                        }
                        //if (isValid != -1) pit_act();
                        break;
                    case "Prop6": // JMS_GST_HONSU1
                                  //pit_act();
                    case "Prop7": // JMS_GST_HONSU2
                        int intValue;
                        if (propName == "Prop6")
                            intValue = jMS_GST_HONSU1;
                        else intValue = jMS_GST_HONSU2;
                        if (intValue < 1)
                        {
                            isValid = -1;
                            break;
                        }
                        if (jMS_GST_RETU < 8)
                        {
                            if (intValue > 32)
                            {
                                isValid = -1;
                                break;
                            }
                        }
                        else if (jMS_GST_RETU > 7)
                        {
                            if (intValue > 16)
                            {
                                isValid = -1;
                                break;
                            }
                        }
                        if (propName == "Prop6")
                        {
                            //if (jMS_GST_HITYP == 0)
                            //    model.Prop7 = intValue.ToString();
                        }
                        if (propName == "Prop7")
                        {
                            if (
                                Math.Abs(jMS_GST_HONSU1 - intValue) != 1
                                )
                            {
                                isValid = -1;
                                break;
                            }
                        }
                        break;
                    case "Prop8": // JMS_GST_HASI1
                        if (jMS_GST_HASI1 < jMS_TGP_AKEI * 0.5)
                        {
                            isValid = -1;
                        }
                        break;
                    case "Prop9":// JMS_GST_HASI2
                        if (jMS_GST_HASI2 < jMS_TGP_AKEI * 0.5)
                        {
                            isValid = -1;
                        }
                        break;
                    case "Prop10": // JMS_GST_PIT , Valid : COM
                        if (jMS_GST_PIT < jMS_TGP_AKEI)
                        {
                            isValid = -1;
                        }

                        //if (Mod_data <= 0)      // BUG : 2018.01.05 FC T.Tsuchida
                        //{
                        //    ji_wsp_heriini(J_inp, hi_typ, nbolt1, nbolt2, Tgs_inp2.pit, &zz, &Tgs_inp2.heria);
                        //    heri_change_act(heri_flg);
                        //    JmsTextReset(&Jm_s[JMS_TGP_HERIK]);
                        //    if (heri_flg == 1)
                        //    {
                        //        JmsTextReset(&Jm_s[JMS_TGP_HERI]);
                        //    }
                        //}

                        break;
                    case "Prop11": // JMS_GST_RPIT
                        if (jMS_GST_RPIT < jMS_TGP_AKEI)
                        {
                            isValid = -1;
                        }
                        break;
                    case "Prop14": //JMS_GST_HERI
                        if (jMS_GST_HERI < 0.1)
                        {
                            isValid = -1;
                        }
                        break;
                    case "Prop15": //JMS_GST_CLR
                        if (jMS_GST_CLR < 1) isValid = -1;
                        else if (jMS_GST_CLR < 5) isValid = -10;
                        //if (isValid > -1)
                        //    Console.WriteLine("web_hi_wid()");
                        //web_hi_wid();
                        break;
                    case "CTValue4": //JW_TX_LEN
                        if (input_JW_TX_LEN < 0.1) isValid = -1;
                        break;
                    case "Text1Dimension": //JW_TX_STR1
                        if (Text1Dimension < 0.1) isValid = -1;
                        break;
                    case "Text2Dimension": //JW_TX_STR2
                        if (Text2Dimension < 0.1) isValid = -1;
                        break;
                    case "Text3Dimension": // JW_TX_STR3
                        if (Text3Dimension < 0.1) isValid = -1;
                        break;
                    case "Text4Dimension": //JW_TX_STR4
                        if (Text4Dimension < 0.1) isValid = -1;
                        break;
                }
            }
            if (isValid < 0)
            {
                if (isValid == -10 || isValid == -2)
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = $"�N���A��{jMS_GST_CLR}�ł�";
                }
                else
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = "���͒l���s���ł��B";
                }

                _errors.Add(new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = isValid, PropName = propName });

            }
            else
            {
                if (!_isCheckAll || propName.Contains("Dimension") || propName == "JW_TX_LEN" || propName == "Prop20")
                {
                    if ("Prop2" == propName && _isFirstUpdateGspBkei == true)
                    {
                        FlgTgpDat flgTgpDat = UpdatePropertyGrid(true);

                        DataRepository.FirstUpdateTgpBkei(ref flgTgpDat);

                        _isFirstUpdateGspBkei = false;

                        LoadDataToModel(flgTgpDat);


                    }
                    else if (propName.Contains("Dimension"))
                    {
                        FlgTgpDat flgTgpDat = UpdatePropertyGrid(true);

                        flgTgpDat._tgasgsp_0 = Text1Dimension;
                        flgTgpDat._tgasgsp_1 = Text2Dimension;
                        flgTgpDat._tgasgsp_2 = Text3Dimension;
                        flgTgpDat._tgasgsp_3 = Text4Dimension;


                        double input_JW_TX_LEN;
                        double.TryParse(model.CTValue4, out input_JW_TX_LEN);
                        flgTgpDat._tgasl = input_JW_TX_LEN;

                        var validation = DataRepository.UpdateDimensionTgp(ref flgTgpDat);

                        Text1Dimension = flgTgpDat._tgasgsp_0;
                        Text2Dimension = flgTgpDat._tgasgsp_1;
                        Text3Dimension = flgTgpDat._tgasgsp_2;
                        Text4Dimension = flgTgpDat._tgasgsp_3;
                        model.CTValue4 = flgTgpDat._tgasl.ToString();

                        if (validation.Item1 < 0 && propName == "Text4Dimension")
                            _errors.Add(new ErrorDetails() { ErrorMessage = validation.Item2, ErrorStatus = validation.Item1, PropName = propName });
                        return;
                    }
                    else if (propName == "CTValue4")
                    {
                        FlgTgpDat flgTgpDat = UpdatePropertyGrid(true);

                        double input_JW_TX_LEN;
                        double.TryParse(model.CTValue4, out input_JW_TX_LEN);

                        flgTgpDat._tgasl = input_JW_TX_LEN;

                        DataRepository.UpdateTgpLen(ref flgTgpDat);

                        model.CTValue4 = flgTgpDat._tgasl.ToString();

                        DataRepository.UpdateFlgTgpDat(ref flgTgpDat);

                        LoadDataToModel(flgTgpDat);

                        return;

                    }
                    else if (propName == "Prop20")
                    {
                        FlgTgpDat flgTgpDat = new FlgTgpDat();
                        flgTgpDat._urarib = SelectedValueProp20;
                        flgTgpDat._urariba_act = Convert.ToInt32(Prop21Enabled);
                        flgTgpDat._urariba = Convert.ToDouble(model.Prop21);

                        DataRepository.UpdateUraribTgp(ref flgTgpDat);

                        model.Prop21 = flgTgpDat._urariba.ToString();
                        Prop21Enabled = Convert.ToBoolean(flgTgpDat._urariba_act);

                        UpdateProperty("PropertyGridModel");

                    }
                    else
                        UpdatePropertyGrid();

                    UpdateDrawWin();
                }
                var validationResult = CheckInputCom(propName);

                if (validationResult != null)
                {
                    _errors.Add(validationResult);
                }
            }
        }
        private ErrorDetails CheckInputCom(string propName)
        {
            var validationResult = DataRepository.ValidateInpDat(DetailType.TGP);
            var length = validationResult.Item2.Length;

            ErrorDetails error = null;


            int controlIndex = GetControlIndexFromPropName(propName);

            if (controlIndex == 0)
                return error;

            for (var i = 0; i < length; i++)
            {
                var retCode = validationResult.Item2[i];
                var errorMess = validationResult.Item1[i];

                if (retCode < 0 && i + 1 == controlIndex)
                {
                    error = new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = retCode, PropName = propName };
                    break;
                }
            }
            return error;
        }
        private int GetControlIndexFromPropName(string propName)
        {
            int controlIndex = 0;
            switch (propName)
            {
                case "Prop16": // JMS_TGP_GPATU
                    controlIndex = 1;
                    break;
                case "Prop17": // JMS_TGP_GPKK   
                    controlIndex = 2;
                    break;
                case "Prop21": // JMS_TGP_URARIBA      
                    controlIndex = 3;
                    break;
            }
            return controlIndex;
        }

        public void UpdateDrawWin()
        {
            _drawArea.Manager.DrawWinFlgTgp(_drawArea.WinID);
        }

        public void SelectBolt()
        {
        }

        public void ClearSelectBolt()
        {
        }
    }
}

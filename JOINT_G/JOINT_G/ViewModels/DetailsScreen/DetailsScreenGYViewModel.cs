using JOINT_G.Helpers;
using JOINT_G.Models;
using JOINT_G.Models.Interfaces;
using JOINT_G.Repository;
using newfast;
using System;
using System.Collections.Generic;
using System.Linq;
using JOINT_G.CustomControls;

namespace JOINT_G.ViewModels.DetailsScreen
{
using System.Reflection;
    /// <summary>
    /// 詳細画面のビューモデル。
    /// </summary>
    public class DetailsScreenGYViewModel :
        ViewModelBase, IDetailScreenViewModel
    {
        #region Item Combobox
        public static Dictionary<int, string> Prop1Item => new()
        {
            { 0, "従来型、改良型" },
            { 2, "角型" }
        };

        public static Dictionary<int, string> Prop7Item => new()
        {
            { 0, "無" },
            { 1, "有" }
        };
        public static Dictionary<int, string> Prop8Item => new()
        {
            { 0, "無" },
            { 1, "有" }
        };
        public static Dictionary<int, string> Prop17Item => new()
        {
            { 0, "無" },
            { 1, "有" }
        };
        public static Dictionary<int, string> Prop19Item => new()
        {
            { 0, "子梁FLG厚" },
            { 1, "入力" }
        };

        public static Dictionary<int, string> Prop22Item => new()
        {
            { 0, "子梁FLG同規格" },
            { 1, "入力" }
        };

        public static Dictionary<int, string> Prop23Item => Helpers.DetailScreenHelper.GetStandards();

        public static Dictionary<int, string> Prop24Item => new()
        {
            { 0, "子梁FLG巾＋フカシ" },
            { 1, "入力" }
        };
        #endregion

        private int _selectedValueProp1;
        private int _selectedValueProp7;
        private int _selectedValueProp8;
        private int _selectedValueProp17;
        private int _selectedValueProp19;
        private int _selectedValueProp22;
        private int _selectedValueProp23;
        private int _selectedValueProp24;

        private bool _prop4Enabled;
        private bool _prop5Enabled;
        private bool _prop7Enabled;
        private bool _prop8Enabled;
        private bool _prop9Enabled;
        private bool _prop10Enabled;
        private bool _prop11Enabled;
        private bool _prop12Enabled;
        private bool _prop13Enabled;
        private bool _prop14Enabled;
        private bool _prop18Enabled;
        private bool _prop19Enabled;
        private bool _prop20Enabled;
        private bool _prop21Enabled;
        private bool _prop22Enabled;
        private bool _prop23Enabled;
        private bool _prop24Enabled;
        private bool _prop25Enabled;
        private bool _prop26Enabled;
        private bool _prop27Enabled;
        private bool _prop28Enabled;
        private bool _isCheckAll = false;
        private List<ErrorDetails> _errors;
        private WdWindowPanel _drawArea;
        public DetailsScreenGYViewModel(
            PropertyGridModelBase propertyGridModel,
            WdWindowPanel drawArea)
        {
            _errors = new List<ErrorDetails>();
            PropertyGridModel = propertyGridModel;
            _drawArea = drawArea;
            LoadDataToModel();
        }

        #region SelectedValue Binding

        private string _SelectedLine = string.Empty;
        public string SelectedLine
        {
            get => _SelectedLine;
            set => SetProperty(ref _SelectedLine, value);
        }
        public int SelectedValueProp1
        {
            get => _selectedValueProp1;
            set
            {
                _selectedValueProp1 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp7
        {
            get => _selectedValueProp7;
            set
            {
                _selectedValueProp7 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp8
        {
            get => _selectedValueProp8;
            set
            {
                _selectedValueProp8 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp17
        {
            get => _selectedValueProp17;
            set
            {
                _selectedValueProp17 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp19
        {
            get => _selectedValueProp19;
            set
            {
                _selectedValueProp19 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp22
        {
            get => _selectedValueProp22;
            set
            {
                _selectedValueProp22 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp23
        {
            get => _selectedValueProp23;
            set
            {
                _selectedValueProp23 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp24
        {
            get => _selectedValueProp24;
            set
            {
                _selectedValueProp24 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }

        #endregion

        #region IsEnable Bindings 


        public bool Prop4Enabled
        {
            get => _prop4Enabled;
            set
            {
                _prop4Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop5Enabled
        {
            get => _prop5Enabled;
            set
            {
                _prop5Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop7Enabled
        {
            get => _prop7Enabled;
            set
            {
                _prop7Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop8Enabled
        {
            get => _prop8Enabled;
            set
            {
                _prop8Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop9Enabled
        {
            get => _prop9Enabled;
            set
            {
                _prop9Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop10Enabled
        {
            get => _prop10Enabled;
            set
            {
                _prop10Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop11Enabled
        {
            get => _prop11Enabled;
            set
            {
                _prop11Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop12Enabled
        {
            get => _prop12Enabled;
            set
            {
                _prop12Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop13Enabled
        {
            get => _prop13Enabled;
            set
            {
                _prop13Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop14Enabled
        {
            get => _prop14Enabled;
            set
            {
                _prop14Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop18Enabled
        {
            get => _prop18Enabled;
            set
            {
                _prop18Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop19Enabled
        {
            get => _prop19Enabled;
            set
            {
                _prop19Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop20Enabled
        {
            get => _prop20Enabled;
            set
            {
                _prop20Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop21Enabled
        {
            get => _prop21Enabled;
            set
            {
                _prop21Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop22Enabled
        {
            get => _prop22Enabled;
            set
            {
                _prop22Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop23Enabled
        {
            get => _prop23Enabled;
            set
            {
                _prop23Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop24Enabled
        {
            get => _prop24Enabled;
            set
            {
                _prop24Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop25Enabled
        {
            get => _prop25Enabled;
            set
            {
                _prop25Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop26Enabled
        {
            get => _prop26Enabled;
            set
            {
                _prop26Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop27Enabled
        {
            get => _prop27Enabled;
            set
            {
                _prop27Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop28Enabled
        {
            get => _prop28Enabled;
            set
            {
                _prop28Enabled = value;
                UpdateProperty();
            }
        }

        #endregion

        #region Public Field
        public string Title => DetailScreenHelper.GetTitle(DetailType.GY);
        public string[] IntTextBoxList { get; } = { "Prop20" };
        public PropertyGridModelBase PropertyGridModel { get; set; }
        public int PropNumber => 28;
        public string[] MemberInfors => DetailScreenHelper.GetMemberInfor(DetailType.GY);

        public Dictionary<string, string> FormatStrings => new Dictionary<string, string>()
        {
            {"Prop1" , "%1d" },
            {"Prop2" , "%6.1f" },
            {"Prop3" , "%6.1f" },
            {"Prop4" , "%6.1f" },
            {"Prop5" , "%6.1f"},
            {"Prop6" , "%6.1f" },
            {"Prop7" , "%1d" },
            {"Prop8" , "%1d" },
            {"Prop9" , "%6.1f" },
            {"Prop10" , "%6.1f" },
            {"Prop11" , "%6.1f" },
            {"Prop12" , "%6.1f" },
            {"Prop13" , "%6.1f" },
            {"Prop14" , "%6.1f" },
            {"Prop15" , "%6.1f" },
            {"Prop16" , "%6.1f" },
            {"Prop17" , "%1d" },
            {"Prop18" , "%6.1f" },
            {"Prop19" , "%1d" },
            {"Prop20" , "%2d" },
            {"Prop21" , "%5.1f" },
            {"Prop22" ,"%1d" },
             {"Prop23" , "%2d" },
              {"Prop24" , "%1d" },
              {"Prop25" , "%5.1f" },
              {"Prop26" , "%5.1f" },
              {"Prop27" , "%5.1f" },
              {"Prop28" , "%5.1f" },
        };

        public List<ErrorDetails> Errors => _errors;

        #endregion

        private void LoadDataToModel(FlgGyDat flgGyDat)
        {
            #region Load Data From Com to View

            var result = flgGyDat;

            var model = (DetailsModelGY)PropertyGridModel;

            SelectedValueProp1 = result._gyTyp;
            model.Prop2 = result._kai1.ToString();
            model.Prop3 = result._kai2.ToString();
            model.Prop4 = result._skr1.ToString();
            model.Prop5 = result._skr2.ToString();
            model.Prop6 = result._loot.ToString();
            SelectedValueProp7 = result._tanskr1;
            SelectedValueProp8 = result._tanskr2;

            model.Prop9 = result._kakusnp1_u.ToString();
            model.Prop10 = result._kakusnp2_u.ToString();
            model.Prop11 = result._kakusnp3_u.ToString();
            model.Prop12 = result._kakusnp1_d.ToString();
            model.Prop13 = result._kakusnp2_d.ToString();
            model.Prop14 = result._kakusnp3_d.ToString();

            model.Prop15 = result._atu1.ToString();
            model.Prop16 = result._atu2.ToString();

            SelectedValueProp17 = result._ribumu;
            model.Prop18 = result._norib.ToString();
            SelectedValueProp19 = result._itakj;
            model.Prop20 = result._sizeup.ToString();
            model.Prop21 = result._itainp.ToString();
            SelectedValueProp22 = result._kkkkj;
            SelectedValueProp23 = result._kkkinp;
            SelectedValueProp24 = result._habakj;
            model.Prop25 = result._fukashi.ToString();
            model.Prop26 = result._habasnp.ToString();
            model.Prop27 = result._mechi1.ToString();
            model.Prop28 = result._mechi2.ToString();


            Prop4Enabled = Convert.ToBoolean(result._skr_act);
            Prop5Enabled = Convert.ToBoolean(result._skr_act);

            Prop7Enabled = Convert.ToBoolean(result._tanskr_act);
            Prop8Enabled = Convert.ToBoolean(result._tanskr_act);

            Prop9Enabled = Convert.ToBoolean(result._kakusnp_act);
            Prop10Enabled = Convert.ToBoolean(result._kakusnp_act);
            Prop11Enabled = Convert.ToBoolean(result._kakusnp_act);
            Prop12Enabled = Convert.ToBoolean(result._kakusnp_act);
            Prop13Enabled = Convert.ToBoolean(result._kakusnp_act);
            Prop14Enabled = Convert.ToBoolean(result._kakusnp_act);

            Prop18Enabled = Convert.ToBoolean(result._norib_act);
            Prop19Enabled = Convert.ToBoolean(result._itakj_act);

            Prop20Enabled = Convert.ToBoolean(result._sizeup_act);
            Prop21Enabled = Convert.ToBoolean(result._itainp_act);
            Prop22Enabled = Convert.ToBoolean(result._kkkkj_act);
            Prop23Enabled = Convert.ToBoolean(result._kkkinp_act);
            Prop24Enabled = Convert.ToBoolean(result._habakj_act);
            Prop25Enabled = Convert.ToBoolean(result._fukashi_act);
            Prop26Enabled = Convert.ToBoolean(result._habasnp_act);
            Prop27Enabled = Convert.ToBoolean(result._mechi_act);
            Prop28Enabled = Convert.ToBoolean(result._mechi_act);

            #endregion
        }
        private void UpdatePropertyGrid()
        {
            try
            {
                FlgGyDat flgGyDat = new FlgGyDat();
                var model = (DetailsModelGY)PropertyGridModel;

                #region Update Property
                flgGyDat._gyTyp = (short)SelectedValueProp1;
                flgGyDat._kai1 = Convert.ToDouble(model.Prop2);
                flgGyDat._kai2 = Convert.ToDouble(model.Prop3);
                flgGyDat._skr1 = Convert.ToDouble(model.Prop4);
                flgGyDat._skr2 = Convert.ToDouble(model.Prop5);
                flgGyDat._loot = Convert.ToDouble(model.Prop6);
                flgGyDat._tanskr1 = (short)SelectedValueProp7;
                flgGyDat._tanskr2 = (short)SelectedValueProp8;
                flgGyDat._kakusnp1_u = Convert.ToDouble(model.Prop9);
                flgGyDat._kakusnp2_u = Convert.ToDouble(model.Prop10);
                flgGyDat._kakusnp3_u = Convert.ToDouble(model.Prop11);
                flgGyDat._kakusnp1_d = Convert.ToDouble(model.Prop12);
                flgGyDat._kakusnp2_d = Convert.ToDouble(model.Prop13);
                flgGyDat._kakusnp3_d = Convert.ToDouble(model.Prop14);
                flgGyDat._atu1 = Convert.ToDouble(model.Prop15);
                flgGyDat._atu2 = Convert.ToDouble(model.Prop16);
                flgGyDat._ribumu = (short)SelectedValueProp17;
                flgGyDat._norib = Convert.ToDouble(model.Prop18);
                flgGyDat._itakj = (short)SelectedValueProp19;
                flgGyDat._sizeup = Convert.ToInt16(model.Prop20);
                flgGyDat._itainp = Convert.ToDouble(model.Prop21);
                flgGyDat._kkkkj = (short)SelectedValueProp22;
                flgGyDat._kkkinp = (short)SelectedValueProp23;
                flgGyDat._habakj = (short)SelectedValueProp24;
                flgGyDat._fukashi = Convert.ToDouble(model.Prop25);
                flgGyDat._habasnp = Convert.ToDouble(model.Prop26);
                flgGyDat._mechi1 = Convert.ToDouble(model.Prop27);
                flgGyDat._mechi2 = Convert.ToDouble(model.Prop28);


                flgGyDat._skr_act = Convert.ToInt32(Prop4Enabled);
                flgGyDat._tanskr_act = Convert.ToInt32(Prop7Enabled);
                flgGyDat._kakusnp_act = Convert.ToInt32(Prop9Enabled);
                flgGyDat._norib_act = Convert.ToInt32(Prop18Enabled);
                flgGyDat._itakj_act = Convert.ToInt32(Prop19Enabled);
                flgGyDat._sizeup_act = Convert.ToInt32(Prop20Enabled);
                flgGyDat._itainp_act = Convert.ToInt32(Prop21Enabled);
                flgGyDat._kkkkj_act = Convert.ToInt32(Prop22Enabled);
                flgGyDat._kkkinp_act = Convert.ToInt32(Prop23Enabled);
                flgGyDat._habakj_act = Convert.ToInt32(Prop24Enabled);
                flgGyDat._fukashi_act = Convert.ToInt32(Prop25Enabled);
                flgGyDat._habasnp_act = Convert.ToInt32(Prop26Enabled);
                flgGyDat._mechi_act = Convert.ToInt32(Prop27Enabled);

                DataRepository.UpdateFlgGyDat(ref flgGyDat);
                #endregion

                LoadDataToModel(flgGyDat);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        public bool ValidateProperty(string propName)
        {
            try
            {
                _errors = new List<ErrorDetails>();
                if (propName == "ALL")
                {
                    _isCheckAll = true;
                    UpdatePropertyGrid();
                    UpdateDrawWin();
                    for (var i = 1; i <= PropNumber; i++)
                    {
                        CheckInput($"Prop{i}");
                    }
                    _isCheckAll = false;
                }
                else
                    CheckInput(propName);
                return _errors.Count == 0;
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return true;
            }
        }
        private void LoadDataToModel()
        {
            try
            {
                LoadDataToModel(DataRepository.GetFlgGyDat());
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        private void CheckInput(string propName)
        {
            string errorMess = "";
            int isValid = 0;

            switch (propName)
            {
                case "Prop1": // JMS_GY_TYPE
                case "Prop7": // JMS_GY_TANSKR1
                case "Prop8":// JMS_GY_TANSKR2
                case "Prop17":
                case "Prop19":
                case "Prop22":
                case "Prop23":
                case "Prop24":
                    if (DetailScreenHelper.HandelCombobox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        switch (propName)
                        {
                            case "Prop17": // JMS_GY_RIBUMU
                                errorMess = "梁付き段差部の水平リブ有無の入力値が不正です。";
                                break;
                            case "Prop19": // JMS_GY_ITAKJ
                                errorMess = "板厚基準の入力値が不正です。";
                                break;
                            case "Prop22": // JMS_GY_KKKKJ
                                errorMess = "規格基準の入力値が不正です。";
                                break;
                            case "Prop23": // JMS_GY_KKKINP
                                errorMess = "規格の入力値が不正です。";
                                break;
                            case "Prop24": // JMS_GY_HABAKJ
                                errorMess = "巾基準の入力値が不正です。";
                                break;
                        }
                        break;
                    }
                default:
                    if (DetailScreenHelper.ValidateTextBox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
            }

            if (!DetailScreenHelper.CanCheckProp(propName))
                return;


            var model = (DetailsModelGY)PropertyGridModel;
            double jMS_GY_KAI1;
            double.TryParse(model.Prop2, out jMS_GY_KAI1);
            double jMS_GY_KAI2;
            double.TryParse(model.Prop3, out jMS_GY_KAI2);
            double jMS_GY_SKR1;
            double.TryParse(model.Prop4, out jMS_GY_SKR1);
            double jMS_GY_SKR2;
            double.TryParse(model.Prop5, out jMS_GY_SKR2);
            double jMS_GY_LOOT;
            double.TryParse(model.Prop6, out jMS_GY_LOOT);
            double jMS_GY_ATU1;
            double.TryParse(model.Prop15, out jMS_GY_ATU1);
            double jMS_GY_ATU2;
            double.TryParse(model.Prop16, out jMS_GY_ATU2);
            double jMS_GY_NORIB;
            double.TryParse(model.Prop18, out jMS_GY_NORIB);
            int sizeUp;
            int.TryParse(model.Prop20, out sizeUp);
            double jMS_GY_ITAINP;
            double.TryParse(model.Prop21, out jMS_GY_ITAINP);
            double jMS_GY_HABASNP;
            double.TryParse(model.Prop26, out jMS_GY_HABASNP);
            double jMS_GY_MECHI1;
            double.TryParse(model.Prop27, out jMS_GY_MECHI1);
            double jMS_GY_MECHI2;
            double.TryParse(model.Prop28, out jMS_GY_MECHI2);
            switch (propName)
            {
                case "Prop2": //JMS_GY_KAI1
                    if (jMS_GY_KAI1 < 0.0 || jMS_GY_KAI1 > 89.99)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop3": // JMS_GY_KAI2
                    if (jMS_GY_KAI2 < 0.0 || jMS_GY_KAI2 > 89.99)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop4": //JMS_GY_SKR1
                    if (jMS_GY_SKR1 < 0.0 || jMS_GY_SKR1 > 99.99)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop5": //JMS_GY_SKR2 
                    if (jMS_GY_SKR2 < 0.0 || jMS_GY_SKR2 > 99.99)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop6": // JMS_GY_LOOT
                    if (jMS_GY_LOOT < 0.0 || jMS_GY_LOOT > 99.99)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop9": //JMS_GY_KAKUSNP1_U
                case "Prop10": // JMS_GY_KAKUSNP2_U
                case "Prop11": // JMS_GY_KAKUSNP3_U
                case "Prop12": // JMS_GY_KAKUSNP1_D
                case "Prop13": //JMS_GY_KAKUSNP2_D
                case "Prop14": //JMS_GY_KAKUSNP3_D
                    Type myType = model.GetType();
                    IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
                    double doubleValue;
                    var value = props.FirstOrDefault(x => x.Name == propName);
                    double.TryParse(value.GetValue(model).ToString(), out doubleValue);

                    if (doubleValue < 1.0)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop15": //JMS_GY_ATU1
                    if (jMS_GY_ATU1 < 0.0 || jMS_GY_ATU1 > 99.99)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop16": //JMS_GY_ATU2

                    if (jMS_GY_ATU2 < jMS_GY_ATU1 + 0.1)
                    {
                        isValid = -1;
                    }
                    else if (jMS_GY_ATU2 < 30)
                    {
                        isValid = -10;
                    }
                    break;
                case "Prop18": //JMS_GY_NORIB
                    if (jMS_GY_NORIB < (float)0.0)
                    {
                        isValid = -1;
                        errorMess = "水平リブ無し条件の入力値が不正です。";
                    }
                    break;
                case "Prop20": // SizeUp
                    if (sizeUp < -9 || sizeUp > 9)
                    {
                        isValid = -1;
                        errorMess = "SizeUpの入力値が不正です。（ -10 < SizeUp < 10 ）";
                    }
                    break;
                case "Prop21":  //JMS_GY_ITAINP
                    if (jMS_GY_ITAINP < (float)0.1)
                    {
                        isValid = -1;
                        errorMess = "板厚の入力値が不正です。";
                    }
                    break;
                case "Prop25": //JMS_GY_FUKASHI
                    break;
                case "Prop26": //JMS_GY_HABASNP
                    if (jMS_GY_HABASNP < (float)0.1)
                    {
                        isValid = -1;
                        errorMess = "巾寸法の入力値が不正です。";
                    }
                    break;
                case "Prop27": //JMS_GY_MECHI1
                    if (jMS_GY_MECHI1 < (float)(-99.9) || jMS_GY_MECHI1 > (float)99.9)
                    {
                        isValid = -1;
                        errorMess = @"めちがい防止・上FLG側の入力値が不正です。|　　 ( -100 < 入力値 < 100 )";
                    }
                    break;
                case "Prop28": //JMS_GY_MECHI2
                    if (jMS_GY_MECHI2 < (float)(-99.9) || jMS_GY_MECHI2 > (float)99.9)
                    {
                        isValid = -1;
                        errorMess = @"めちがい防止・下FLG側の入力値が不正です。|　　 ( -100 < 入力値 < 100 )";
                    }
                    break;
            }
            if (isValid < 0)
            {
                if (isValid == -10 || isValid == -2)
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = "適応板厚の推奨地は、| 下限＝６　上限＝１００です。";
                }
                else
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = "入力値が不正です。";
                }
                _errors.Add(new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = isValid, PropName = propName });
            }
            else
            {
                if (!_isCheckAll)
                {
                    UpdatePropertyGrid();
                    UpdateDrawWin();
                }
                var validationResult = CheckInputCom(propName);

                if (validationResult != null)
                {
                    _errors.Add(validationResult);
                }
            }
        }
        private ErrorDetails CheckInputCom(string propName)
        {
            var validationResult = DataRepository.ValidateInpDat(DetailType.GY);
            var length = validationResult.Item2.Length;

            ErrorDetails error = null;


            int controlIndex = GetControlIndexFromPropName(propName);

            if (controlIndex == 0)
                return error;

            for (var i = 0; i < length; i++)
            {
                var retCode = validationResult.Item2[i];
                var errorMess = validationResult.Item1[i];

                if (retCode < 0 && i + 1 == controlIndex)
                {
                    error = new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = retCode, PropName = propName };
                    break;
                }
            }
            return error;
        }
        private int GetControlIndexFromPropName(string propName)
        {
            int controlIndex = 0;
            switch (propName)
            {
                case "Prop21": // JMS_GY_ITAINP      
                    controlIndex = 1;
                    break;
            }
            return controlIndex;
        }

        public void UpdateDrawWin()
        {
            _drawArea.Manager.DrawWinFlgGy(_drawArea.WinID);
        }

        public void SelectBolt()
        {
        }

        public void ClearSelectBolt()
        {
        }
    }
}

using JOINT_G.Models;
using JOINT_G.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using newfast;
using JOINT_G.Models.Interfaces;
using JOINT_G.Helpers;
using JOINT_G.CustomControls;

namespace JOINT_G.ViewModels.DetailsScreen
{
    /// <summary>
    /// 詳細画面のビューモデル。
    /// </summary>
    public class DetailsScreenPLViewModel : ViewModelBase, IDetailScreenViewModel
    {
        #region Combobox Item 
        public static Dictionary<int, string> Prop6Item => Helpers.DetailScreenHelper.GetStandards();

        #endregion

        private const double _pLY_MIMIMAX = 999.0;

        private List<ErrorDetails> _errors;
        private bool _isCheckAll = false;

        private WdWindowPanel _drawArea;
        public DetailsScreenPLViewModel(PropertyGridModelBase propertyGridModel, WdWindowPanel drawArea)
        {
            PropertyGridModel = propertyGridModel;
            _drawArea = drawArea;
            _errors = new List<ErrorDetails>();
            LoadDataToModel();
        }

        #region Public Field
        public string Title => DetailScreenHelper.GetTitle(DetailType.PLY);
        public int SelectedValueProp6 { get; set; }
        public PropertyGridModelBase PropertyGridModel { get; }
        public string[] IntTextBoxList { get; } = { };
        public int PropNumber => 6;
        public string[] MemberInfors => DetailScreenHelper.GetMemberInfor(DetailType.PLY);

        private string _SelectedLine = string.Empty;
        public string SelectedLine
        {
            get => _SelectedLine;
            set => SetProperty(ref _SelectedLine, value);
        }

        public Dictionary<string, string> FormatStrings => new Dictionary<string, string>()
        {
            { "Prop1" , "%6.1f" },
            { "Prop2" , "%6.1f" },
            { "Prop3" , "%6.1f" },
            { "Prop4" , "%6.1f" },
            { "Prop5" , "%6.1f" },
            { "Prop6" , "%2d" },
        };

        public List<ErrorDetails> Errors => _errors;

        #endregion

        private void LoadDataToModel(FlgPlyDat flgPlyDat)
        {
            var result = flgPlyDat;

            var model = (DetailsModelPL)PropertyGridModel;
            model.Prop1 = result._mimi1.ToString();
            model.Prop2 = result._mimi2.ToString();
            model.Prop3 = result._mimi3.ToString();
            model.Prop4 = result._mimi4.ToString();
            model.Prop5 = result._atu.ToString();
            SelectedValueProp6 = result._atukk;
        }
        private void UpdatePropertyGrid()
        {
            try
            {
                var flgPlyDat = new FlgPlyDat();
                var model = (DetailsModelPL)PropertyGridModel;

                flgPlyDat._mimi1 = Convert.ToDouble(model.Prop1);
                flgPlyDat._mimi2 = Convert.ToDouble(model.Prop2);
                flgPlyDat._mimi3 = Convert.ToDouble(model.Prop3);

                flgPlyDat._mimi4 = Convert.ToDouble(model.Prop4);
                flgPlyDat._atu = Convert.ToDouble(model.Prop5);
                flgPlyDat._atukk = SelectedValueProp6;

                DataRepository.UpdateFlgPlyDat(flgPlyDat);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        public bool ValidateProperty(string propName)
        {
            try
            {
                _errors = new List<ErrorDetails>();
                if (propName == "ALL")
                {
                    _isCheckAll = true;
                    UpdatePropertyGrid();
                    UpdateDrawWin();
                    for (var i = 1; i <= PropNumber; i++)
                    {
                        CheckInput($"Prop{i}");
                        if (_errors.Any(x => x.ErrorStatus == -1))
                        {
                            _isCheckAll = false;
                            return false;
                        }
                    }
                    _isCheckAll = false;
                }
                else
                    CheckInput(propName);
                return _errors.Count == 0;
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return true;
            }
        }
        private void LoadDataToModel()
        {
            try
            {
                LoadDataToModel(DataRepository.GetFlgPlyDat());
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        private void CheckInput(string propName)
        {
            string errorMess = "";
            int isValid = 0;
            if (!DetailScreenHelper.CanCheckProp(propName))
                return;
            switch (propName)
            {
                case "Prop6":
                    if (DetailScreenHelper.HandelCombobox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
                default:
                    if (DetailScreenHelper.ValidateTextBox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
            }
            var model = (DetailsModelPL)PropertyGridModel;

            double jMS_PLY_MIMI1;
            double jMS_PLY_MIMI2;
            double jMS_PLY_MIMI3;
            double jMS_PLY_MIMI4;

            double.TryParse(model.Prop1, out jMS_PLY_MIMI1);
            double.TryParse(model.Prop2, out jMS_PLY_MIMI2);
            double.TryParse(model.Prop3, out jMS_PLY_MIMI3);
            double.TryParse(model.Prop4, out jMS_PLY_MIMI4);

            switch (propName)
            {
                case "Prop1": // JMS_PLY_MIMI1
                    if (jMS_PLY_MIMI1 < 0.0 || jMS_PLY_MIMI1 > _pLY_MIMIMAX) isValid = -1;
                    break;
                case "Prop2": // JMS_PLY_MIMI2
                    if (jMS_PLY_MIMI2 < 0.0 || jMS_PLY_MIMI3 > _pLY_MIMIMAX) isValid = -1;
                    break;
                case "Prop3": // JMS_PLY_MIMI3
                    if (jMS_PLY_MIMI3 < 0.0 || jMS_PLY_MIMI3 > _pLY_MIMIMAX) isValid = -1;
                    break;
                case "Prop4": // JMS_PLY_MIMI4
                    if (jMS_PLY_MIMI4 < 0.0 || jMS_PLY_MIMI4 > _pLY_MIMIMAX) isValid = -1;
                    break;
                case "Prop5": // JMS_PLY_ATU
                    break;
                case "Prop6": // JMS_PLY_ATUKK
                    break;

            }
            if (isValid < 0)
            {

                if (isValid == -10 || isValid == -2)
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = "適応板厚の推奨地は、| 下限＝６　上限＝１００です。";
                }
                else
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = "入力値が不正です。";
                }

                _errors.Add(new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = isValid, PropName = propName });
            }
            else
            {
                if (!_isCheckAll)
                {
                    UpdatePropertyGrid();
                    UpdateDrawWin();
                }
                var validationResult = CheckInputCom(propName);

                if (validationResult != null)
                {
                    _errors.Add(validationResult);
                }
            }
        }
        private ErrorDetails CheckInputCom(string propName)
        {
            var validationResult = DataRepository.ValidateInpDat(DetailType.PLY);
            var length = validationResult.Item2.Length;

            ErrorDetails error = null;


            int controlIndex = GetControlIndexFromPropName(propName);

            if (controlIndex == 0)
                return error;

            for (var i = 0; i < length; i++)
            {
                var retCode = validationResult.Item2[i];
                var errorMess = validationResult.Item1[i];

                if (retCode < 0 && i + 1 == controlIndex)
                {
                    error = new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = retCode, PropName = propName };
                    break;
                }
            }
            return error;
        }
        private int GetControlIndexFromPropName(string propName)
        {
            int controlIndex = 0;
            switch (propName)
            {
                case "Prop5": // JMS_PLY_ATU      
                    controlIndex = 1;
                    break;
            }
            return controlIndex;
        }

        public void UpdateDrawWin()
        {
            _drawArea.Manager.DrawWinFlgFsp(_drawArea.WinID);
        }

        public void SelectBolt()
        {
        }

        public void ClearSelectBolt()
        {
        }
    }
}

using JOINT_G.Models;
using JOINT_G.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using newfast;
using JOINT_G.Models.Interfaces;
using JOINT_G.Helpers;
using System.Text.RegularExpressions;
using JOINT_G.CustomControls;

namespace JOINT_G.ViewModels.DetailsScreen
{
    /// <summary>
    /// 詳細画面のビューモデル。
    /// </summary>
    public class DetailsScreenWSPViewModel :
        ViewModelBase, IDetailScreenViewModel
    {
        #region Combobox Item
        public static Dictionary<int, string> Prop1Item => new()
        {
            { 0, "平列" },
            { 1, "千鳥" }
        };

        private static Dictionary<int, string> prop4Item = (new Func<Dictionary<int, string>>(() =>
         {
             try
             {

                 var result = DataRepository.GetFlgWspDat()._bolkk_list;

                 result = result.Trim();
                 Regex trimmer = new Regex(@"\s\s+");
                 result = trimmer.Replace(result, " ");

                 var data = new Dictionary<int, string>();
                 int j = 0;
                 foreach (var i in result.Split(" "))
                 {
                     data.Add(j, i);
                     j++;
                 }
                 return data;
             }
             catch (Exception ex)
             {
                 ex.HandleException();
                 return new Dictionary<int, string>();
             }
         })).Invoke();

        public static Dictionary<int, string> Prop4Item { get => prop4Item; }

        public static Dictionary<int, string> Prop12Item => new()
        {
            { 0, "入力" },
            { 1, "対称振り分け" }
        };

        public static Dictionary<int, string> Prop17Item => Helpers.DetailScreenHelper.GetStandards();

        public static Dictionary<int, string> Prop19Item => new()
        {
            { 1, "片側一枚" },
            { 2, "両側二枚" }
        };

        public static Dictionary<int, string> Prop21Item => new()
        {
            { 0, "なし" },
            { 1, "あり" }
        };

        public static Dictionary<string, string> Prop22Item => new()
        {
            { "B120", "" },
            { "B180", "" }
        };

        #endregion

        private int _selectedValueProp1;
        private int _selectedValueProp4;
        private int _selectedValueProp12;
        private int _selectedValueProp17;
        private int _selectedValueProp19;
        private int _selectedValueProp21;
        private string _selectedValueProp22;

        private bool _prop7Enabled;
        private bool _prop10Enabled;
        private bool _prop11Enabled;
        private bool _prop12Enabled;
        private bool _prop13Enabled;
        private bool _prop14Enabled;
        private bool _prop22Enabled;
        private bool _prop23Enabled;
        private bool _prop24Enabled;
        private bool _category3Enabled;
        private List<ErrorDetails> _errors;
        private bool _isFirstUpdateGspBkei = true;
        private bool _isCheckAll = false;
        private WdWindowPanel _drawArea;
        public DetailsScreenWSPViewModel(
            PropertyGridModelBase propertyGridModel, WdWindowPanel drawArea)
        {
            PropertyGridModel = propertyGridModel;
            _drawArea = drawArea;
            _errors = new List<ErrorDetails>();
            LoadDataToModel();
        }

        #region Selected Value Binding
        private string _SelectedLine = string.Empty;
        public string SelectedLine
        {
            get => _SelectedLine;
            set => SetProperty(ref _SelectedLine, value);
        }
        public int SelectedValueProp1
        {
            get => _selectedValueProp1;
            set
            {
                _selectedValueProp1 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp4
        {
            get => _selectedValueProp4;
            set
            {
                _selectedValueProp4 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp12
        {
            get => _selectedValueProp12;
            set
            {
                _selectedValueProp12 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp17
        {
            get => _selectedValueProp17;
            set
            {
                _selectedValueProp17 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp19
        {
            get => _selectedValueProp19;
            set
            {
                _selectedValueProp19 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp21
        {
            get => _selectedValueProp21;
            set
            {
                _selectedValueProp21 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public string SelectedValueOfPorp22
        {
            get => _selectedValueProp22;
            set
            {
                _selectedValueProp22 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }

        #endregion

        #region IsEnable Binding 

        public bool Prop7Enabled
        {
            get => _prop7Enabled;
            set
            {
                _prop7Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop10Enabled
        {
            get => _prop10Enabled;
            set
            {
                _prop10Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop11Enabled
        {
            get => _prop11Enabled;
            set
            {
                _prop11Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop12Enabled
        {
            get => _prop12Enabled;
            set
            {
                _prop12Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop13Enabled
        {
            get => _prop13Enabled;
            set
            {
                _prop13Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop14Enabled
        {
            get => _prop14Enabled;
            set
            {
                _prop14Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop22Enabled
        {
            get => _prop22Enabled;
            set
            {
                _prop22Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop23Enabled
        {
            get => _prop23Enabled;
            set
            {
                _prop23Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop24Enabled
        {
            get => _prop24Enabled;
            set
            {
                _prop24Enabled = value;
                UpdateProperty();
            }
        }

        public bool Category3Enabled
        {
            get => _category3Enabled;
            set
            {
                _category3Enabled = value;
                UpdateProperty();
            }
        }
        #endregion

        #region Public Field
        public string Title => DetailScreenHelper.GetTitle(DetailType.WSP);
        public PropertyGridModelBase PropertyGridModel { get; }
        public int PropNumber => 24;
        public string[] MemberInfors => DetailScreenHelper.GetMemberInfor(DetailType.WSP);
        public Dictionary<string, string> FormatStrings { get; } = new Dictionary<string, string>()
        {
            {"Prop1" , "%1d" },
            {"Prop2" , "%3d" },
            {"Prop3" , "%4.1f" },
            {"Prop4" ,"%2d" },
            {"Prop5" , "%2d" },
            {"Prop6" , "%3d" },
            {"Prop7" , "%3d" },
            {"Prop8" , "%6.1f" },
            {"Prop9" , "%6.1f" },
            {"Prop10" , "%6.1f" },
            {"Prop11" , "%6.1f"  },
            {"Prop12" ,"%1d"  },
            {"Prop13" , "%1s" },
            {"Prop14" ,"%6.1f" },
            {"Prop15" ,"%3d" },
            {"Prop16" , "%6.1f" },
            {"Prop17"  , "%2d" },
            {"Prop18" , "%6.1f" },
            {"Prop19" , "%1d" },
            {"Prop20" , "%s" },
            {"Prop21" , "%1d" },
            {"Prop22" , "%6s" },
            {"Prop23" ,"%6.1f" },
            {"Prop24" , "%2d" },
        };

        public List<ErrorDetails> Errors => _errors;


        private double _hsp;

        #endregion

        private void LoadDataToModel(FlgWspDat flgWspDat)
        {
            var result = flgWspDat;

            var model = (DetailsModelWSP)PropertyGridModel;
            _hsp = _hsp == 0 ? result._hsp : _hsp;
            SelectedValueProp1 = result._hiTyp; //         model.Prop1 = result._hiTyp;
            model.Prop2 = result._bkei.ToString();
            model.Prop3 = result._akei.ToString();
            SelectedValueProp4 = result._bkk; //        model.Prop4 = result._bkk;

            model.Prop5 = result._letu.ToString();
            model.Prop6 = result._honsu1.ToString();
            model.Prop7 = result._honsu2.ToString();
            model.Prop8 = result._hasi1.ToString();

            model.Prop9 = result._hasi2.ToString();

            model.Prop10 = result._pit.ToString();

            model.Prop11 = result._rpit.ToString();

            SelectedValueProp12 = result._herik;          // model.Prop14 = result._herik;
            model.Prop13 = result._herlb_label ?? "";
            model.Prop14 = result._heri.ToString();

            model.Prop15 = result._clr.ToString();

            model.Prop16 = result._spatu.ToString();
            SelectedValueProp17 = result._spkk; //model.Prop19 = result._spkk;
            model.Prop18 = result._futi.ToString();
            SelectedValueProp19 = result._futik; //  model.Prop21 = result._futik;

            model.Prop20 = result._splb_label ?? "";

            SelectedValueProp21 = result._choumu;  //   model.Prop23 = result._chomu;
            model.Prop22 = result._chotyp?.Trim() ?? "";
            model.Prop23 = result._chosnp.ToString();
            model.Prop24 = result._wkai.ToString();

            Prop7Enabled = Convert.ToBoolean(result._honsu2_act);
            Prop10Enabled = Convert.ToBoolean(result._pit_act);
            Prop11Enabled = Convert.ToBoolean(result._rpit_act);
            Prop12Enabled = Convert.ToBoolean(result._herik_act);
            Prop13Enabled = Convert.ToBoolean(result._herlb_act);
            Prop14Enabled = Convert.ToBoolean(result._heri_act);
            Prop22Enabled = Convert.ToBoolean(result._chotyp_act);
            Prop23Enabled = Convert.ToBoolean(result._chosnp_act);
            Prop24Enabled = Convert.ToBoolean(result._wkai_act);
            Category3Enabled = Convert.ToBoolean(result._wkai_act);


            UpdateProperty("PropertyGridModel");
        }
        private FlgWspDat UpdatePropertyGrid(bool onlyAssign = false)
        {
            FlgWspDat flgWspDat = new FlgWspDat();
            try
            {
                var model = (DetailsModelWSP)PropertyGridModel;

                flgWspDat._hiTyp = SelectedValueProp1;
                flgWspDat._bkei = Convert.ToInt32(model.Prop2);
                flgWspDat._akei = Convert.ToDouble(model.Prop3);
                flgWspDat._bkk = SelectedValueProp4;
                flgWspDat._letu = Convert.ToInt32(model.Prop5);
                flgWspDat._honsu1 = Convert.ToInt32(model.Prop6);
                flgWspDat._honsu2 = Convert.ToInt32(model.Prop7);
                flgWspDat._hasi1 = Convert.ToDouble(model.Prop8);
                flgWspDat._hasi2 = Convert.ToDouble(model.Prop9);

                flgWspDat._pit = Convert.ToDouble(model.Prop10);
                flgWspDat._rpit = Convert.ToDouble(model.Prop11);
                flgWspDat._herik = SelectedValueProp12;
                flgWspDat._herlb_label = model.Prop13;
                flgWspDat._heri = Convert.ToDouble(model.Prop14);
                flgWspDat._clr = Convert.ToInt32(model.Prop15);
                flgWspDat._spatu = Convert.ToDouble(model.Prop16);
                flgWspDat._spkk = SelectedValueProp17;
                flgWspDat._futi = Convert.ToDouble(model.Prop18);
                flgWspDat._futik = SelectedValueProp19;
                flgWspDat._splb_label = model.Prop20;
                flgWspDat._choumu = SelectedValueProp21;
                flgWspDat._chotyp = model.Prop22;
                flgWspDat._chosnp = Convert.ToDouble(model.Prop23);
                flgWspDat._wkai = Convert.ToInt16(model.Prop24);

                flgWspDat._honsu2_act = Convert.ToInt32(Prop7Enabled);
                flgWspDat._pit_act = Convert.ToInt32(Prop10Enabled);
                flgWspDat._rpit_act = Convert.ToInt32(Prop11Enabled);
                flgWspDat._herik_act = Convert.ToInt32(Prop12Enabled);
                flgWspDat._herlb_act = Convert.ToInt32(Prop13Enabled);
                flgWspDat._heri_act = Convert.ToInt32(Prop14Enabled);
                flgWspDat._chotyp_act = Convert.ToInt32(Prop22Enabled);
                flgWspDat._chosnp_act = Convert.ToInt32(Prop23Enabled);
                flgWspDat._wkai_act = Convert.ToInt32(Prop24Enabled);

                if (onlyAssign)
                    return flgWspDat;
                DataRepository.UpdateFlgWspDat(ref flgWspDat);
                LoadDataToModel(flgWspDat);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
            return flgWspDat;
        }
        private void LoadDataToModel()
        {
            try
            {
                LoadDataToModel(DataRepository.GetFlgWspDat());
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        public bool ValidateProperty(string propName)
        {
            try
            {
                _errors = new List<ErrorDetails>();
                if (propName == "ALL")
                {
                    _isCheckAll = true;
                    UpdatePropertyGrid();
                    UpdateDrawWin();
                    for (var i = 1; i <= PropNumber; i++)
                    {
                        CheckInput($"Prop{i}");
                        if (_errors.Any(x => x.ErrorStatus == -1))
                        {
                            _isCheckAll = false;
                            return false;
                        }
                    }
                    _isCheckAll = false;
                }
                else
                    CheckInput(propName);
                return _errors.Count == 0;
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return true;
            }
        }
        private void CheckInput(string propName)
        {
            string errorMess = "";
            int isValid = 0;
            if (!DetailScreenHelper.CanCheckProp(propName))
                return;

            switch (propName)
            {
                case "Prop1":
                case "Prop4":
                case "Prop14":
                case "Prop19":
                case "Prop21":
                case "Prop22":
                    if (DetailScreenHelper.HandelCombobox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        switch (propName)
                        {
                            case "Prop21":
                                errorMess = "蝶番取付有無の入力値が不正です。";
                                break;
                            case "Prop22":
                                errorMess = "蝶番タイプの入力値が不正です。";
                                break;
                        }
                        break;
                    }
                default:
                    if (DetailScreenHelper.ValidateTextBox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
            }


            var model = (DetailsModelWSP)PropertyGridModel;
            var jMS_WSP_HITYP = SelectedValueProp1;
            int jMS_WSP_BKEI;
            int.TryParse(model.Prop2, out jMS_WSP_BKEI);

            double jMS_WSP_AKEI;
            double.TryParse(model.Prop3, out jMS_WSP_AKEI);

            int jMS_WSP_LETU;
            int.TryParse(model.Prop5, out jMS_WSP_LETU);

            int jMS_GST_HONSU1;
            int.TryParse(model.Prop6, out jMS_GST_HONSU1);

            int jMS_GST_HONSU2;
            int.TryParse(model.Prop7, out jMS_GST_HONSU2);

            double jMS_GST_HASI1;
            double.TryParse(model.Prop8, out jMS_GST_HASI1);
            double jMS_GST_HASI2;
            double.TryParse(model.Prop9, out jMS_GST_HASI2);

            double jMS_GST_PIT;
            double.TryParse(model.Prop10, out jMS_GST_PIT);
            double jMS_GST_RPIT;
            double.TryParse(model.Prop11, out jMS_GST_RPIT);

            double jMS_GST_HERI;
            double.TryParse(model.Prop14, out jMS_GST_HERI);

            int jMS_GST_CLR;
            int.TryParse(model.Prop15, out jMS_GST_CLR);

            double jMS_WSP_FUTI;
            double.TryParse(model.Prop18, out jMS_WSP_FUTI);
            double jMS_WSP_CHOSNP;
            double.TryParse(model.Prop23, out jMS_WSP_CHOSNP);

            int jMS_WSP_WKAI;
            int.TryParse(model.Prop24, out jMS_WSP_WKAI);

            switch (propName)
            {
                case "Prop2": // JMS_TGP_BKEI
                    if (jMS_WSP_BKEI < 1) isValid = -1;
                    else
                    {
                        //if (mod == 0)
                        //{
                        //    wsp_data_get(Start_flg);
                        //}
                    }
                    break;
                case "Prop3": //JMS_TGP_AKEI
                    if (jMS_WSP_AKEI < 0.1) isValid = -1;
                    break;
                case "Prop5": // JMS_WSP_LETU

                    if (jMS_WSP_LETU < 1 || jMS_WSP_LETU > 15) isValid = -1;
                    if (jMS_WSP_HITYP == 1)
                    {
                        if (jMS_WSP_LETU < 2) isValid = -1;
                    }
                    //if (isValid != -1) pit_act();
                    break;
                case "Prop6": // JMS_GST_HONSU1
                              //pit_act();
                case "Prop7": // JMS_GST_HONSU2
                    int intValue;
                    if (propName == "Prop6")
                        intValue = jMS_GST_HONSU1;
                    else intValue = jMS_GST_HONSU2;

                    if (intValue < 1)
                    {
                        isValid = -1;
                        break;
                    }
                    if (jMS_WSP_LETU < 8)
                    {
                        if (intValue > 32)
                        {
                            isValid = -1;
                            break;
                        }
                    }
                    else if (jMS_WSP_LETU > 7)
                    {
                        if (intValue > 16)
                        {
                            isValid = -1;
                            break;
                        }
                    }
                    if (propName == "Prop6")
                    {
                        if (jMS_WSP_HITYP == 0)
                            model.Prop7 = intValue.ToString();
                    }
                    if (propName == "Prop7")
                    {
                        if (
                            Math.Abs(jMS_GST_HONSU1 - intValue) != 1
                            )
                        {
                            isValid = -1;
                            break;
                        }
                    }
                    break;
                case "Prop8": // JMS_GST_HASI1
                    if (jMS_GST_HASI1 < jMS_WSP_AKEI * 0.5)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop9":// JMS_GST_HASI2
                    if (jMS_GST_HASI2 < jMS_WSP_AKEI * 0.5)
                    {
                        isValid = -1;
                    }
                    break;
                //case "Prop10": //JMS_WSP_BRHSUMU
                //    if (jMS_WSP_BRHSUMU < 0 || jMS_WSP_BRHSUMU > 1) isValid = -1;
                //    break;
                case "Prop10": // JMS_GST_PIT , Valid : COM
                    if (jMS_GST_PIT < jMS_WSP_AKEI)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop11": // JMS_GST_RPIT
                    if (jMS_GST_RPIT < jMS_WSP_AKEI)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop12": //JMS_WSP_HERIK
                    break;
                case "Prop14": //JMS_GST_HERI
                    if (jMS_GST_HERI < 0.1)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop15": //JMS_GST_CLR
                    if (jMS_GST_CLR < 1)
                    {
                        if (jMS_GST_CLR == 0)
                        {
                            isValid = -10;
                        }
                        else
                        {
                            isValid = -1;
                        }
                    }
                    else if (jMS_GST_CLR < 5) isValid = -10;
                    //if (isValid > -1)
                    //    Console.WriteLine("web_hi_wid()");
                    //web_hi_wid();
                    break;
                case "Prop16":// JMS_WSP_SPATU
                    break;
                case "Prop17": // JMS_WSP_SPKK
                    break;
                case "Prop18": // JMS_WSP_FUTI
                    if (jMS_WSP_FUTI < jMS_WSP_AKEI * 0.5) isValid = -1;
                    break;
                case "Prop19": // JMS_WSP_FUTIK
                    break;
                case "Prop21": // JMS_WSP_CHOUMU
                    break;
                case "Prop23": // JMS_WSP_CHOSNP
                    if (jMS_WSP_CHOSNP < (float)0.0 || jMS_WSP_CHOSNP > (float)9999.9)
                    {
                        errorMess = "蝶番取付寸法の入力値が不正です。";
                        isValid = -1;
                        break;
                    }

                    if (jMS_WSP_CHOSNP > _hsp * (float)0.5)
                    {
                        errorMess = "蝶番取付寸法の入力値が不正です。";
                        isValid = -1;
                        break;
                    }

                    break;
                case "Prop22": // JMS_WSP_CHOTYP

                case "Prop24": // JMS_WSP_WKAI
                    //Gst_sp2.wb_wel[0] is JMS_GSP_WKAI
                    // Gst_sp2.wb_wel[1] is JMS_GSP_URBLT

                    if (jMS_WSP_WKAI < 0 || jMS_WSP_WKAI >= 90)
                    {
                        isValid = -1;
                        break;
                    }
                    break;
            }
            if (isValid < 0)
            {
                if (isValid == -10 || isValid == -2)
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = $"クリアが{jMS_GST_CLR}です";
                }
                else
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = "入力値が不正です。";
                }
                _errors.Add(new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = isValid, PropName = propName });
            }
            else
            {
                if (!_isCheckAll)
                {
                    if ("Prop2" == propName && _isFirstUpdateGspBkei == true)
                    {
                        FlgWspDat flgWspDat = UpdatePropertyGrid(true);

                        DataRepository.FirstUpdateWspBkei(ref flgWspDat);

                        LoadDataToModel(flgWspDat);

                        _isFirstUpdateGspBkei = false;
                    }
                    else
                        UpdatePropertyGrid();
                    UpdateDrawWin();
                }
                var validationResult = CheckInputCom(propName);

                if (validationResult != null)
                {
                    _errors.Add(validationResult);
                }
            }
        }
        private ErrorDetails CheckInputCom(string propName)
        {
            var validationResult = DataRepository.ValidateInpDat(DetailType.WSP);
            var length = validationResult.Item2.Length;

            ErrorDetails error = null;


            int controlIndex = GetControlIndexFromPropName(propName);

            if (controlIndex == 0)
                return error;

            for (var i = 0; i < length; i++)
            {
                var retCode = validationResult.Item2[i];
                var errorMess = validationResult.Item1[i];

                if (retCode < 0 && i + 1 == controlIndex)
                {
                    error = new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = retCode, PropName = propName };
                    break;
                }
            }
            return error;
        }
        private int GetControlIndexFromPropName(string propName)
        {
            int controlIndex = 0;
            switch (propName)
            {
                case "Prop16": // JMS_WSP_SPATU 
                    controlIndex = 1;
                    break;
                case "Prop17": // JMS_WSP_SPKK       
                    controlIndex = 2;
                    break;
                case "Prop18": // JMS_WSP_FUTI              
                    controlIndex = 3;
                    break;
            }
            return controlIndex;
        }

        public void UpdateDrawWin()
        {
            _drawArea.Manager.DrawWinFlgWsp(_drawArea.WinID);
        }

        public void SelectBolt()
        {
            _drawArea.Manager.SelBoltFlgWsp(_drawArea.WinID);
        }

        public void ClearSelectBolt()
        {
            _drawArea.Manager.ClrBoltFlgWsp(_drawArea.WinID);
        }
    }
}

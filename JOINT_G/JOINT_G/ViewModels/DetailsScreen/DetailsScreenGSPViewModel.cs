using JOINT_G.Models;
using JOINT_G.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using newfast;
using JOINT_G.Models.Interfaces;
using JOINT_G.Helpers;
using System.Text.RegularExpressions;
using JOINT_G.CustomControls;

namespace JOINT_G.ViewModels.DetailsScreen
{
    /// <summary>
    /// 詳細画面のビューモデル。
    /// </summary>
    public class DetailsScreenGSPViewModel :
        ViewModelBase, IDetailScreenViewModel
    {
        #region Combobox Item

        public static Dictionary<int, string> Prop1Item => new()
        {
            { 0, "平列" },
            { 1, "千鳥" },
        };

        private static Dictionary<int, string> prop4Item = (new Func<Dictionary<int, string>>(() =>
          {
              try
              {
                  var result = DataRepository.GetFlgGstSpDat()._bolkk_list ?? "";

                  result = result.Trim();
                  Regex trimmer = new Regex(@"\s\s+");
                  result = trimmer.Replace(result, " ");

                  var data = new Dictionary<int, string>();
                  int j = 0;
                  foreach (var i in result.Split(" "))
                  {
                      data.Add(j, i);
                      j++;
                  }
                  return data;
              }
              catch (Exception ex)
              {
                  ex.HandleException();
                  return new Dictionary<int, string>();
              }
          })).Invoke();
        public static Dictionary<int, string> Prop4Item { get => prop4Item; }
        public static Dictionary<int, string> Prop12Item => new()
        {
            { 0, "入力" },
            { 1, "対称振り分け" },
        };

        public static Dictionary<int, string> Prop17Item => Helpers.DetailScreenHelper.GetStandards();

        public static Dictionary<int, string> Prop19Item => new()
        {
            { 1, "片側一枚" },
            { 2, "両側二枚" },
        };


        public static Dictionary<int, string> Prop21Item => new()
        {
            { 0, "なし" },
            { 1, "あり" },
        };

        public static Dictionary<string, string> Prop22Item => new()
        {
            { "B120", "" },
            { "B180", "" }
        };

        public static Dictionary<int, string> Prop25Item => Prop17Item;

        public static Dictionary<int, string> Prop26Item => new()
        {
            { 0, "有（ＧＰ同厚）" },
            { 1, "無" },
            { 2, "有（板厚入力）" }
        };

        public static Dictionary<int, string> Prop29Item => new()
        {
            { 0, "有り" },
            { 1, "無し" },
        };

        #endregion

        private bool _prop7Enabled;
        private bool _prop10Enabled;
        private bool _prop11Enabled;
        private bool _prop12Enabled;
        private bool _prop13Enabled;
        private bool _prop14Enabled;
        private bool _prop22Enabled;
        private bool _prop23Enabled;
        private bool _prop27Enabled;
        private bool _prop28Enabled;
        private bool _prop29Enabled;
        private bool _prop30Enabled;

        private int _selectedValueProp1;
        private int _selectedValueProp4;
        private int _selectedValueProp12;
        private int _selectedValueProp17;
        private int _selectedValueProp19;
        private int _selectedValueProp21;
        private string _selectedValueProp22;
        private int _selectedValueProp25;
        private int _selectedValueProp26;
        private int _selectedValueProp29;
        private bool _category4Enabled;
        private List<ErrorDetails> _errors;
        private bool _isFirstUpdateGspBkei = true;
        private bool _isCheckAll = false;
        private WdWindowPanel _drawArea;
        public DetailsScreenGSPViewModel(PropertyGridModelBase propertyGridModel, WdWindowPanel drawArea)
        {
            PropertyGridModel = propertyGridModel;
            _drawArea = drawArea;
            _errors = new List<ErrorDetails>();
            LoadDataToModel();
        }

        #region Selected Value Binding
        private string _SelectedLine = string.Empty;
        public string SelectedLine
        {
            get => _SelectedLine;
            set => SetProperty(ref _SelectedLine, value);
        }
        public int SelectedValueProp1
        {
            get => _selectedValueProp1;
            set
            {
                _selectedValueProp1 = value;
                UpdateProperty();
            }
        }
        public int SelectedValueProp4
        {
            get => _selectedValueProp4;
            set
            {
                _selectedValueProp4 = value;
                UpdateProperty();
            }
        }
        public int SelectedValueProp12
        {
            get => _selectedValueProp12;
            set
            {
                _selectedValueProp12 = value;
                UpdateProperty();
            }
        }
        public int SelectedValueProp17
        {
            get => _selectedValueProp17;
            set
            {
                _selectedValueProp17 = value;
                UpdateProperty();
            }
        }
        public int SelectedValueProp19
        {
            get => _selectedValueProp19;
            set
            {
                _selectedValueProp19 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp21
        {
            get => _selectedValueProp21;
            set
            {
                _selectedValueProp21 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public string SelectedValueProp22
        {
            get => _selectedValueProp22;
            set
            {
                _selectedValueProp22 = ((string)value)?.Trim();
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp25
        {
            get => _selectedValueProp25;
            set
            {
                _selectedValueProp25 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp26
        {
            get => _selectedValueProp26;
            set
            {
                _selectedValueProp26 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        public int SelectedValueProp29
        {
            get => _selectedValueProp29;
            set
            {
                _selectedValueProp29 = value;
                if (DetailScreenHelper.CanUpdateCombobox())
                    UpdateProperty();
            }
        }
        #endregion

        #region IsEnableBinding
        public bool Category4Enabled
        {
            get => _category4Enabled;
            set
            {
                _category4Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop7Enabled
        {
            get => _prop7Enabled;
            set
            {
                _prop7Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop10Enabled
        {
            get => _prop10Enabled;
            set
            {
                _prop10Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop11Enabled
        {
            get => _prop11Enabled;
            set
            {
                _prop11Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop12Enabled
        {
            get => _prop12Enabled;
            set
            {
                _prop12Enabled = value;
                UpdateProperty();
            }
        }

        public bool Prop13Enabled
        {
            get => _prop13Enabled;
            set
            {
                _prop13Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop14Enabled
        {
            get => _prop14Enabled;
            set
            {
                _prop14Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop22Enabled
        {
            get => _prop22Enabled;
            set
            {
                _prop22Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop23Enabled
        {
            get => _prop23Enabled;
            set
            {
                _prop23Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop27Enabled
        {
            get => _prop27Enabled;
            set
            {
                _prop27Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop28Enabled
        {
            get => _prop28Enabled;
            set
            {
                _prop28Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop29Enabled
        {
            get => _prop29Enabled;
            set
            {
                _prop29Enabled = value;
                UpdateProperty();
            }
        }
        public bool Prop30Enabled
        {
            get => _prop30Enabled;
            set
            {
                _prop30Enabled = value;
                UpdateProperty();
            }
        }

        #endregion

        #region Public Field

        public string Title => DetailScreenHelper.GetTitle(DetailType.GST_SP);
        public PropertyGridModelBase PropertyGridModel { get; }
        public string[] IntTextBoxList { get; } = { "Prop2", "Prop5", "Prop6", "Prop7", "Prop15" };
        public int PropNumber => 30;
        public string[] MemberInfors => DetailScreenHelper.GetMemberInfor(DetailType.GST_SP);
        public Dictionary<string, string> FormatStrings => new Dictionary<string, string>()
        {
            {"Prop1" , "%1d" },
            {"Prop2" ,"%3d" },
            {"Prop3" , "%6.1f" },
            {"Prop4" , "%2d" },
            {"Prop5" , "%2d" },
            {"Prop6" , "%3d" },
            {"Prop7" , "%3d" },
            {"Prop8" , "%6.1f" },
            {"Prop9" , "%6.1f" },
            {"Prop10" , "%6.1f" },
            {"Prop11" , "%6.1f" },
            {"Prop12" , "%1d" },
            {"Prop13" , "%s" },
            {"Prop14" , "%6.1f" },
            {"Prop15" , "%3d" },
            {"Prop16" , "%6.1f" },
            {"Prop17" , "%2d" },
            {"Prop18" ,"%6.1f" },
            {"Prop19" , "%1d" },
            {"Prop20" , "%s" },
            {"Prop21" , "%1d" },
            {"Prop22" , "%6s" },
            {"Prop23" , "%6.1f" },
            {"Prop24" , "%6.1f" },
            {"Prop25" , "%2d" },
            {"Prop26" , "%1d" },
            {"Prop27" ,"%6.1f" },
            {"Prop28" , "%2d" },
            {"Prop29" , "%1d" },
            {"Prop30" , "%6.1f" },
        };

        public List<ErrorDetails> Errors => _errors;

        private double _hsp;
        #endregion

        private void LoadDataToModel(FlgGstSpDat flgGstSpDat)
        {
            var result = flgGstSpDat;
            var model = (DetailsModelGSP)PropertyGridModel;
            _hsp = _hsp == 0 ? result._hsp : _hsp;

            SelectedValueProp1 = result._hiTyp;      //model.Prop1 = result._hiTyp;
            model.Prop2 = result._bkei.ToString();
            model.Prop3 = result._akei.ToString();
            SelectedValueProp4 = result._bkk;  //model.Prop4 = result._bkk;

            model.Prop5 = result._retu.ToString();
            model.Prop6 = result._honsu1.ToString();
            model.Prop7 = result._honsu2.ToString();
            model.Prop8 = result._hasi1.ToString();

            model.Prop9 = result._hasi2.ToString();
            model.Prop10 = result._pit.ToString();
            model.Prop11 = result._rpit.ToString();
            SelectedValueProp12 = result._herik;  //model.Prop12 = result._herik;

            model.Prop13 = result._herlb_label ?? "";

            model.Prop14 = result._heri.ToString();
            model.Prop15 = result._clr.ToString();
            model.Prop16 = result._spatu.ToString();
            SelectedValueProp17 = result._spkk;   //model.Prop17 = result._spkk;

            model.Prop18 = result._futi.ToString();

            SelectedValueProp19 = result._msp; //model.Prop19 = result._msp;
            model.Prop20 = result._splb_label ?? "";



            SelectedValueProp21 = result._choumu; // model.Prop21 = result._choumu;
            model.Prop22 = result._chotyp?.Trim() ?? "";

            model.Prop23 = result._chosnp.ToString();

            model.Prop24 = result._gpatu.ToString();
            SelectedValueProp25 = result._gpkk;// model.Prop25 = Convert.ToInt32(result._gpkk);
            //SelectedValueProp26 = result._urarib;  // model.Prop26 = result._urarib;
            model.Prop27 = result._urariba.ToString();

            model.Prop28 = result._wkai.ToString();

            SelectedValueProp29 = result._urblt;  //model.Prop29 = Convert.ToInt32(result._urblt);
            model.Prop30 = result._uryou.ToString();


            Prop7Enabled = Convert.ToBoolean(result._honsu2_act);

            Prop10Enabled = Convert.ToBoolean(result._pit_act);
            Prop11Enabled = Convert.ToBoolean(result._rpit_act);
            Prop12Enabled = Convert.ToBoolean(result._herik_act);

            Prop13Enabled = Convert.ToBoolean(result._herlb_act);
            Prop14Enabled = Convert.ToBoolean(result._heri_act);

            Prop22Enabled = Convert.ToBoolean(result._chotyp_act);
            Prop23Enabled = Convert.ToBoolean(result._chosnp_act);

            //Prop27Enabled = Convert.ToBoolean(result._urariba_act);
            Prop28Enabled = Convert.ToBoolean(result._wkai_act);
            Prop29Enabled = Convert.ToBoolean(result._urblt_act);
            Prop30Enabled = Convert.ToBoolean(result._uryou_act);
            Category4Enabled = Convert.ToBoolean(result._wkai_act);
            UpdateProperty("PropertyGridModel");
        }
        private FlgGstSpDat UpdatePropertyGrid(bool onlyAssign = false)
        {
            FlgGstSpDat flgGstSpDat = new FlgGstSpDat();
            try
            {
                var model = (DetailsModelGSP)PropertyGridModel;

                flgGstSpDat._hiTyp = SelectedValueProp1;
                flgGstSpDat._bkei = Convert.ToInt32(model.Prop2);
                flgGstSpDat._akei = Convert.ToDouble(model.Prop3);
                flgGstSpDat._bkk = SelectedValueProp4;
                flgGstSpDat._retu = Convert.ToInt32(model.Prop5);
                flgGstSpDat._honsu1 = Convert.ToInt32(model.Prop6);
                flgGstSpDat._honsu2 = Convert.ToInt32(model.Prop7);
                flgGstSpDat._hasi1 = Convert.ToDouble(model.Prop8);
                flgGstSpDat._hasi2 = Convert.ToDouble(model.Prop9);
                flgGstSpDat._pit = Convert.ToDouble(model.Prop10);
                flgGstSpDat._rpit = Convert.ToDouble(model.Prop11);
                flgGstSpDat._herik = SelectedValueProp12;
                flgGstSpDat._herlb_label = model.Prop13;
                flgGstSpDat._heri = Convert.ToDouble(model.Prop14);
                flgGstSpDat._clr = Convert.ToInt32(model.Prop15);
                flgGstSpDat._spatu = Convert.ToDouble(model.Prop16);
                flgGstSpDat._spkk = SelectedValueProp17;
                flgGstSpDat._futi = Convert.ToDouble(model.Prop18);
                flgGstSpDat._msp = SelectedValueProp19;
                flgGstSpDat._splb_label = model.Prop20;
                flgGstSpDat._choumu = SelectedValueProp21;
                flgGstSpDat._chotyp = model.Prop22;
                flgGstSpDat._chosnp = Convert.ToDouble(model.Prop23);
                flgGstSpDat._gpatu = Convert.ToDouble(model.Prop24);
                flgGstSpDat._gpkk = (short)SelectedValueProp25;
                flgGstSpDat._urarib = SelectedValueProp26;
                flgGstSpDat._urariba = Convert.ToDouble(model.Prop27);
                flgGstSpDat._wkai = Convert.ToInt16(model.Prop28);
                flgGstSpDat._urblt = (short)SelectedValueProp29;
                flgGstSpDat._uryou = Convert.ToDouble(model.Prop30);

                flgGstSpDat._honsu2_act = Convert.ToInt32(Prop7Enabled);

                flgGstSpDat._pit_act = Convert.ToInt32(Prop10Enabled);
                flgGstSpDat._rpit_act = Convert.ToInt32(Prop11Enabled);
                flgGstSpDat._herik_act = Convert.ToInt32(Prop12Enabled);
                flgGstSpDat._herlb_act = Convert.ToInt32(Prop13Enabled);
                flgGstSpDat._heri_act = Convert.ToInt32(Prop14Enabled);

                flgGstSpDat._chotyp_act = Convert.ToInt32(Prop22Enabled);
                flgGstSpDat._chosnp_act = Convert.ToInt32(Prop23Enabled);

                flgGstSpDat._urariba_act = Convert.ToInt32(Prop27Enabled);
                flgGstSpDat._wkai_act = Convert.ToInt32(Prop28Enabled);
                flgGstSpDat._urblt_act = Convert.ToInt32(Prop29Enabled);
                flgGstSpDat._uryou_act = Convert.ToInt32(Prop30Enabled);

                if (onlyAssign)
                    return flgGstSpDat;
                DataRepository.UpdateFlgGstSpDat(ref flgGstSpDat);
                LoadDataToModel(flgGstSpDat);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
            return flgGstSpDat;
        }
        private void LoadDataToModel()
        {
            try
            {
                var model = (DetailsModelGSP)PropertyGridModel;
                var flgGstSpDat = DataRepository.GetFlgGstSpDat();

                SelectedValueProp26 = flgGstSpDat._urarib;
                model.Prop27 = flgGstSpDat._urariba.ToString();
                Prop27Enabled = Convert.ToBoolean(flgGstSpDat._urariba_act);
                LoadDataToModel(flgGstSpDat);
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
        }
        public bool ValidateProperty(string propName)
        {
            try
            {
                _errors = new List<ErrorDetails>();
                if (propName == "ALL")
                {
                    _isCheckAll = true;
                    UpdatePropertyGrid();
                    UpdateDrawWin();
                    for (var i = 1; i <= PropNumber; i++)
                    {
                        CheckInput($"Prop{i}");
                        if (_errors.Any(x => x.ErrorStatus == -1))
                        {
                            _isCheckAll = false;
                            return false;
                        }
                    }
                    _isCheckAll = false;
                }
                else
                    CheckInput(propName);
                return _errors.Count == 0;
            }
            catch (Exception ex)
            {
                ex.HandleException();
                return true;
            }
        }
        private void CheckInput(string propName)
        {
            string errorMess = "";
            int isValid = 0;
            if (!DetailScreenHelper.CanCheckProp(propName))
                return;
            switch (propName)
            {
                case "Prop1":
                case "Prop4":
                case "Prop12":
                case "Prop17":
                case "Prop19":
                case "Prop21":
                case "Prop22":
                case "Prop25":
                case "Prop26":
                case "Prop29":
                    if (DetailScreenHelper.HandelCombobox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
                default:
                    if (DetailScreenHelper.ValidateTextBox(propName))
                    {
                        break;
                    }
                    else
                    {
                        isValid = -1;
                        break;
                    }
            }

            var model = (DetailsModelGSP)PropertyGridModel;

            var jMS_GSP_HITYP = SelectedValueProp1;
            int jMS_GSP_BKEI;
            int.TryParse(model.Prop2, out jMS_GSP_BKEI);
            double jMS_GSP_AKEI;
            double.TryParse(model.Prop3, out jMS_GSP_AKEI);

            int jMS_GSP_RETU;
            int.TryParse(model.Prop5, out jMS_GSP_RETU);
            int jMS_GSP_HONSU1;
            int.TryParse(model.Prop6, out jMS_GSP_HONSU1);
            int jMS_GSP_HONSU2;
            int.TryParse(model.Prop7, out jMS_GSP_HONSU2);

            double jMS_GSP_HASI1;
            double.TryParse(model.Prop8, out jMS_GSP_HASI1);

            double jMS_GSP_HASI2;
            double.TryParse(model.Prop9, out jMS_GSP_HASI2);

            double jMS_GSP_PIT;
            double.TryParse(model.Prop10, out jMS_GSP_PIT);

            double jMS_GSP_RPIT;
            double.TryParse(model.Prop11, out jMS_GSP_RPIT);

            double jMS_GSP_HERI;
            double.TryParse(model.Prop14, out jMS_GSP_HERI);

            int jMS_GSP_CLR;
            int.TryParse(model.Prop15, out jMS_GSP_CLR);
            double jMS_GSP_FUTI;
            double.TryParse(model.Prop18, out jMS_GSP_FUTI);

            double jMS_WSP_CHOSNP;
            double.TryParse(model.Prop23, out jMS_WSP_CHOSNP);
            int jMS_GSP_WKAI;
            int.TryParse(model.Prop28, out jMS_GSP_WKAI);
            double jMS_GSP_URYOU;
            double.TryParse(model.Prop30, out jMS_GSP_URYOU);

            switch (propName)
            {
                case "Prop2": //JMS_GSP_BKEI
                    if (jMS_GSP_BKEI < 1) isValid = -1;
                    //else
                    //{
                    //    if (mod == 0)
                    //        wsp_data_get(Start_flg);
                    //}
                    break;
                case "Prop3": //JMS_GSP_AKEI
                    // Gst_sp2.dhall is Prop3
                    if (jMS_GSP_AKEI < 0.1) isValid = -1;
                    break;
                case "Prop5": //JMS_GSP_RETU

                    if (jMS_GSP_RETU < 1 || jMS_GSP_RETU > 15) isValid = -1;
                    if (jMS_GSP_HITYP == 1)
                    {
                        if (jMS_GSP_RETU < 2) isValid = -1;
                    }
                    //if (sts != -1) pit_act();
                    break;
                case "Prop6": //JMS_GSP_HONSU1
                case "Prop7": //JMS_GSP_HONSU2
                    // tt is JMS_GSP_RETU
                    int intValue;
                    if (propName == "Prop6") intValue = jMS_GSP_HONSU1;
                    else intValue = jMS_GSP_HONSU2;
                    if (intValue < 1)
                    {
                        isValid = -1;
                        break;
                    }
                    if (jMS_GSP_RETU < 8)
                    {
                        if (intValue > 32)
                        {
                            isValid = -1;
                            break;
                        }
                    }
                    else if (jMS_GSP_RETU > 7)
                    {
                        if (intValue > 16)
                        {
                            isValid = -1;
                            break;
                        }
                    }

                    if (propName == "Prop6") //JMS_GSP_HONSU1
                    {
                        // Gst_sp2.ihaire is JMS_GSP_HITYP 

                        //if (jMS_GSP_HITYP == 0)
                        //    //Gst_sp2.nbolt1 is JMS_GSP_HONSU1
                        //    //Gst_sp2.nbolt2 is JMS_GSP_HONSU2
                        //    model.Prop7 = jMS_GSP_HONSU1.ToString();
                    }
                    if (propName == "Prop7") //JMS_GSP_HONSU2
                    {
                        if (Math.Abs(jMS_GSP_HONSU1 - intValue) != 1)
                        {
                            isValid = -1;
                        }
                    }
                    break;
                case "Prop8":  //JMS_GSP_HASI1
                    //Gst_sp2.dhall is JMS_GSP_AKEI
                    if (jMS_GSP_HASI1 < jMS_GSP_AKEI * .5)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop9": //JMS_GSP_HASI2
                    //Gst_sp2.dhall is JMS_GSP_AKEI
                    if (jMS_GSP_HASI2 < jMS_GSP_AKEI * .5)
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop10": //JMS_GSP_PIT
                    // Gst_sp2.dhall is JMS_GSP_AKEI
                    if (jMS_GSP_PIT < jMS_GSP_AKEI) isValid = -1;
                    break;
                case "Prop11": //JMS_GSP_RPIT
                    if (jMS_GSP_RPIT < jMS_GSP_AKEI) isValid = -1;
                    break;
                case "Prop12": //JMS_GSP_HERIK
                    break;
                case "Prop13": //JMS_GSP_HERLB
                    break;
                case "Prop14": //JMS_GSP_HERI
                    if (jMS_GSP_HERI < 0.1) isValid = -1;
                    break;
                case "Prop15": //JMS_GSP_CLR
                    if (jMS_GSP_CLR < 1) isValid = -1;
                    else if (jMS_GSP_CLR < 5) isValid = -10;
                    //if (sts > -1)
                    //    web_hi_wid();
                    break;
                case "Prop16": //JMS_GSP_SPATU
                    break;
                case "Prop17": //JMS_GSP_SPKK
                case "Prop25": //JMS_GSP_GPKK
                    break;
                case "Prop26": //JMS_GSP_URARIB
                    break;
                case "Prop27": //JMS_GSP_URARIBA
                    break;
                case "Prop18": // JMS_GSP_FUTI
                    //Gst_sp2.dhall is JMS_GSP_AKEI
                    if (jMS_GSP_FUTI < (0.5 * jMS_GSP_AKEI))
                    {
                        isValid = -1;
                    }
                    break;
                case "Prop19": //JMS_GSP_MSP
                    break;
                case "Prop24": //JMS_GSP_GPATU
                    break;
                case "Prop21": //JMS_WSP_CHOUMU
                    break;
                case "Prop23": //JMS_WSP_CHOSNP
                    if (jMS_WSP_CHOSNP < (float)0.0 || jMS_WSP_CHOSNP > (float)9999.9)
                    {
                        errorMess = "蝶番取付寸法の入力値が不正です。";
                        isValid = -1;
                        break;
                    }

                    if (jMS_WSP_CHOSNP > _hsp * (float)0.5)
                    {
                        errorMess = "蝶番取付寸法の入力値が不正です。";
                        isValid = -1;
                        break;
                    }
                    break;
                case "Prop22": //JMS_WSP_CHOTYP
                case "Prop28": //JMS_GSP_WKAI
                    //Gst_sp2.wb_wel[0] is JMS_GSP_WKAI
                    // Gst_sp2.wb_wel[1] is JMS_GSP_URBLT

                    if (jMS_GSP_WKAI < 0 || jMS_GSP_WKAI >= 90)
                    {
                        isValid = -1;
                    }
                    else
                    {
                        if (jMS_GSP_WKAI == 0)
                        {
                            SelectedValueProp29 = 0;
                        }
                    }
                    break;
                case "Prop29":   //JMS_GSP_URBLT
                    break;
                case "Prop30":   //JMS_GSP_URYOU
                    if (jMS_GSP_URYOU < (float)0.1)
                    {
                        isValid = -1;
                    }
                    break;
            }
            if (isValid < 0)
            {
                if (isValid == -10 || isValid == -2)
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = $"クリアが{jMS_GSP_CLR}です";
                }
                else
                {
                    if (string.IsNullOrEmpty(errorMess))
                        errorMess = "入力値が不正です。";
                }

                _errors.Add(new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = isValid, PropName = propName });
            }
            else
            {
                if (!_isCheckAll || propName == "Prop26")
                {
                    if ("Prop2" == propName && _isFirstUpdateGspBkei == true)
                    {
                        FlgGstSpDat flgGstSpDat = UpdatePropertyGrid(true);

                        DataRepository.FirstUpdateGspBkei(ref flgGstSpDat);

                        LoadDataToModel(flgGstSpDat);

                        _isFirstUpdateGspBkei = false;
                    }
                    else if (propName == "Prop26")
                    {
                        FlgGstSpDat flgGstSpDat = new FlgGstSpDat();
                        flgGstSpDat._urarib = SelectedValueProp26;
                        flgGstSpDat._urariba_act = Convert.ToInt32(Prop27Enabled);
                        flgGstSpDat._urariba = Convert.ToDouble(model.Prop27);

                        DataRepository.UpdateUraribGstSp(ref flgGstSpDat);

                        SelectedValueProp26 = flgGstSpDat._urarib;
                        model.Prop27 = flgGstSpDat._urariba.ToString();
                        Prop27Enabled = Convert.ToBoolean(flgGstSpDat._urariba_act);

                    }
                    else
                        UpdatePropertyGrid();
                    UpdateDrawWin();

                }
                var validationResult = CheckInputCom(propName);

                if (validationResult != null)
                {
                    _errors.Add(validationResult);
                }
            }
        }
        private ErrorDetails CheckInputCom(string propName)
        {
            var validationResult = DataRepository.ValidateInpDat(DetailType.GST_SP);
            var length = validationResult.Item2.Length;

            ErrorDetails error = null;


            int controlIndex = GetControlIndexFromPropName(propName);

            if (controlIndex == 0)
                return error;

            for (var i = 0; i < length; i++)
            {
                var retCode = validationResult.Item2[i];
                var errorMess = validationResult.Item1[i];

                if (retCode < 0 && i + 1 == controlIndex)
                {
                    error = new ErrorDetails() { ErrorMessage = errorMess, ErrorStatus = retCode, PropName = propName };
                    break;
                }
            }
            return error;
        }
        private int GetControlIndexFromPropName(string propName)
        {
            int controlIndex = 0;
            switch (propName)
            {
                case "Prop16": // JMS_GSP_SPATU
                    controlIndex = 1;
                    break;
                case "Prop17": // JMS_GSP_SPKK    
                    controlIndex = 2;
                    break;
                case "Prop25": // JMS_GSP_GPKK        
                    controlIndex = 3;
                    break;
                case "Prop27": // JMS_GSP_URARIBA        
                    controlIndex = 4;
                    break;
                case "Prop18": //JMS_GSP_FUTI
                    controlIndex = 5;
                    break;
                case "Prop24": // JMS_GSP_GPATU
                    controlIndex = 6;
                    break;
            }
            return controlIndex;
        }

        public void UpdateDrawWin()
        {
            _drawArea.Manager.DrawWinFlgGstSp(_drawArea.WinID);
        }

        public void SelectBolt()
        {
            _drawArea.Manager.SelBoltFlgGstSp(_drawArea.WinID);
        }

        public void ClearSelectBolt()
        {
            _drawArea.Manager.ClrBoltFlgGstSp(_drawArea.WinID);
        }
    }
}

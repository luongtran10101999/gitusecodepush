using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using JOINT_G.Helpers;

namespace JOINT_G.ViewModels
{
    /// <summary>
    /// ビューモデルのベースクラス。
    /// </summary>
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void UpdateProperty([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected List<ValidationResult> ValidateProperty<T>(T value, [CallerMemberName] string propertyName = null, bool skip = false)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            if (skip)
            {
                return null;
            }
            try
            {
                bool valid = Validator.TryValidateProperty(value, new ValidationContext(this) { MemberName = propertyName }, results);
                if (!valid)
                    return results;
            }
            catch (Exception ex)
            {
                ex.HandleException();
            }
            return null;
        }
        protected bool SetProperty<T>(ref T field, T newValue, [CallerMemberName] string? propertyName = null, bool skip = false)
        {
            if (!(object.Equals(field, newValue)) || skip)
            {
                field = (newValue);
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
                return true;
            }
            return false;
        }

        protected virtual void Dispose() { }
    }
}
